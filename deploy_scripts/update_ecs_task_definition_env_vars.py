import json
import os
import subprocess
import sys
import re
from pathlib import Path


service = 'web'
env_ = sys.argv[1]
if env_ not in ['dev', 'prod', 'staging']:
    raise Exception('Invalid ENV')


# ------------------------------- DECLARATION ----------------------------------
# define env vars to update here:
# Be careful, this will propagate to all of task definitions so BE SURE
# that you do not update VARS that are unique for every task definition here
# like DJANGO_SETTINGS_MODULE
ENV_VARS_TO_UPDATE = {
    'REACT_APP_ZUNO_API_URL': 'https://api.zuno.io/api/v1'
}

# ------------------------------------ UPDATE ----------------------------------
# get list of task definition families
task_definitions = subprocess.Popen(
    'aws ecs list-task-definition-families --status ACTIVE',
    shell=True, stdout=subprocess.PIPE)
task_definitions = json.loads(task_definitions.stdout.read())

# ecs_dir = os.path.join(settings.BASE_DIR, 'ecs')
ecs_dir = os.path.join(Path(__file__).resolve().parent.parent, 'aws')
ecs_dir_env = os.path.join(ecs_dir, 'ecs')

# update each family
for task_def_family in task_definitions['families']:
    if not re.match(rf'{env_}-.*-{service}', task_def_family):
        continue
    print(f'Updating: {task_def_family}')
    fname = os.path.join(ecs_dir_env, f'{task_def_family}.json')
    task_def = subprocess.Popen(
        f'aws ecs describe-task-definition --task-definition {task_def_family}',
        shell=True, stdout=subprocess.PIPE)
    task_def = json.loads(
        task_def.stdout.read())['taskDefinition']
    env_vars = task_def['containerDefinitions'][0]['environment']
    env_vars = {v['name']: v for v in env_vars}
    for k, v in ENV_VARS_TO_UPDATE.items():
        if k == 'DJANGO_SETTINGS_MODULE':
            raise Exception(f'you are not allowed to update {k} this var here')
        env_vars[k] = {'name': k, 'value': v}
    # env_vars['DJANGO_SETTINGS_MODULE'] = {
    #     'name': 'DJANGO_SETTINGS_MODULE',
    #     'value': django_settings
    # }
    env_vars = list(env_vars.values())
    task_def['containerDefinitions'][0]['environment'] = env_vars
    task_def.pop('taskDefinitionArn', None)
    task_def.pop('revision', None)
    task_def.pop('status', None)
    task_def.pop('requiresAttributes', None)
    task_def.pop('compatibilities', None)
    task_def.pop('registeredAt', None)
    task_def.pop('registeredBy', None)

    with open(fname, 'w') as f:
        json.dump(task_def, f, indent=4)
    register_task_def = subprocess.Popen(
        f'aws ecs register-task-definition '
        f'--cli-input-json '
        f'file://{fname}',
        shell=True, stdout=subprocess.PIPE)
    register_task_def = json.loads(register_task_def.stdout.read())

    update_process = subprocess.Popen(
        f"aws ecs update-service "
        f"--cluster zuno-{service} "
        f"--service {env_}-zuno-{service} "
        f"--force-new-deployment --task-definition {task_def_family} "
        f"| less='+q';",
        shell=True, stdout=subprocess.PIPE)
    print(update_process.stdout.read())
