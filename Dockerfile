FROM public.ecr.aws/n2w3s6h5/node:16.3-alpine AS builder
ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

ARG REACT_APP_ZUNO_API_URL
ENV REACT_APP_ZUNO_API_URL $REACT_APP_ZUNO_API_URL


# create root directory for our project in the container
RUN mkdir /app

RUN printenv > /app/.env


RUN echo $REACT_APP_ZUNO_API_URL

# set working directory
WORKDIR /app

# install dependencies
COPY package.json ./
COPY yarn.lock ./
RUN yarn install --frozen-lockfile

# copy files
COPY . .

# compile
RUN yarn build

FROM public.ecr.aws/n2w3s6h5/nginx:1.19-alpine AS server

WORKDIR /app

# setup depyloyment locations
COPY ./etc/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/build /usr/share/nginx/html

WORKDIR /usr/share/nginx/html

EXPOSE 80
EXPOSE 3000

CMD ["nginx", "-g", "daemon off;"]
