#!/bin/bash
aws ecr get-login-password --region us-east-1 | sudo docker login --username AWS --password-stdin 573176876695.dkr.ecr.us-east-1.amazonaws.com/zuno-web
sudo docker build -t zuno-web:latest .
sudo docker tag  zuno-web:latest 573176876695.dkr.ecr.us-east-1.amazonaws.com/zuno-web

sudo docker push 573176876695.dkr.ecr.us-east-1.amazonaws.com/zuno-web
aws ecs update-service --cluster zuno-web --service dev-zuno-web --force-new-deployment | less='+q';
