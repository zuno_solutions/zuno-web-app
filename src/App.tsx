import React, { useEffect } from 'react';
import {
  Redirect, Route, Switch, useLocation,
} from 'react-router-dom';
import './App.scss';
import PrivateRoute from './auth/PrivateRoute';
import { OnboardingPage } from './pages/OnboardingPage';
import { HomePage } from './pages/HomePage';
import { Account } from './pages/Account';
import { VerifyEmail } from './pages/VerifyEmail';
// import Alert from './components/Alert/Alert';
import { VerifyResetPasswordToken } from './pages/VerifyResetPasswordToken';
import { SetYourPassword } from './pages/SetYourPassword';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-datetime/css/react-datetime.css';
import { Dashboard } from './pages/Dashboard';
import { Layout } from './components/Layout';
import { PersonnelManagement } from './pages/PersonnelManagement';

// import { accountService } from './_services/account.service';

function App() {
  const { pathname } = useLocation();

  useEffect(() => {
    (window as any).Chargebee?.init({
      site: process.env.REACT_APP_CHARGEBEE_SITE,
      publishableKey: process.env.REACT_APP_CHARGEBEE_KEY,
    });
  }, []);

  return (
    <>
      <Layout>
        {/* <Alert /> */}
        <Switch>
          <Redirect from="/:url*(/+)" to={pathname.slice(0, -1)} />
          <PrivateRoute exact path="/" component={HomePage} />
          <PrivateRoute exact path="/dashboard" component={Dashboard} />
          <PrivateRoute exact path="/manage-personnel" component={PersonnelManagement} />
          {/* <PrivateRoute path="/admin" roles={[Role.Admin]} component={Admin} /> */}
          <PrivateRoute path="/set-account" component={OnboardingPage} />
          <Route path="/account" component={Account} />
          <Route path="/verify-email" component={VerifyEmail} />
          <Route path="/accept-invite" component={VerifyEmail} />
          <Route path="/reset-password" component={VerifyResetPasswordToken} />
          <Route path="/set-password" component={SetYourPassword} />
          <Redirect from="*" to="/" />
        </Switch>
      </Layout>
    </>
  );
}

export default App;
