export const MedicalSpecialties = [
  'Cardiothoracic Surgery',
  'General Surgery',
  'Neurosurgery',
  'Oral and Maxillofacial Surgery',
  'Orthopedic Surgery',
  'Otolaryngology (ENT)',
  'Paediatric Surgery',
  'Plastic Surgery',
  'Trauma',
  'Urology',
];
