export interface InviteValues {
  firstName: string;
  lastName: string;
  email: string;
  check?: boolean;
  mainPhone?: string;
}
