export const VendorTimeBeforeSurgery = [
  '15 minutes',
  '20 minutes',
  '30 minutes',
  '45 minutes',
  '60 minutes',
  '90 minutes',
];
export const VendorGracePeriod = [
  '5 minutes',
  '10 minutes',
  '15 minutes',
  '20 minutes',
  '25 minutes',
  '30 minutes',
];
export const VendorStartTime = [
  '5 minutes',
  '10 minutes',
  '15 minutes',
  '20 minutes',
  '30 minutes',
];
export const TrayDeliveryTime = [
  '2 hours',
  '4 hours',
  '6 hours',
  '8 hours',
  '12 hours',
  '16 hours',
  '24 hours',
];
export const VendorTrayGracePeriod = [
  '30 minutes',
  '1 hours',
  '2 hours',
  '3 hours',
  '4 hours',
];
