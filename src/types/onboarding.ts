export enum ORDOnboardingTypes
{
  ORDConnection = 'ord-connection',
  ORDRoomsNumeration = 'ord-rooms-numeration',
  ORDRoomsNaming = 'ord-rooms-naming',
  ORDRoomsArrivalTimes = 'ord-arrival-times',
  ORDPersonnelUpload = 'ord-personnel-upload',
  ORDPersonnelUploadReview = 'ord-personnel-review',
  PersonnelInvite = 'ord-personnel-invite',
  PersonnelManagement = 'ord-personnel-management',
}
