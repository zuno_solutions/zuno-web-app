import React from 'react';
import { Col } from 'react-bootstrap';
import styles from './checkEmail.module.scss';
import emailIcon from '../../assets/images/email_icon.png';

interface CheckEmailProps {
  title: string;
  message: any;
}

const CheckEmail = ({ title, message }: CheckEmailProps) => (
  <Col className="d-flex justify-content-center">
    <div className="align-self-center text-center">
      <div className={styles.check_email_img__container}>
        <img src={emailIcon} alt="Email confirmation icon" />
      </div>
      <h1 className={styles.check_email__title}>{title}</h1>
      <p>{message}</p>
    </div>
  </Col>
);

export default CheckEmail;
