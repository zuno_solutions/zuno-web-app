import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import { Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { Button } from 'react-bootstrap';
import styles from './ForgotPassword.module.scss';
import CheckEmail from '../../CheckEmail';
import { alertService } from '../../../_services/alert.service';
import { accountService } from '../../../_services/account.service';
import InputFormikField from '../../../components/_forms/InputFormikField';

export const ForgotPassword = () => {
  const [successSubmit, setSuccessSubmit] = useState(false);

  interface Values {
    email: string;
  }
  const initialValues: Values = {
    email: '',
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().email('Email is invalid').required('Email is required'),
  });

  const onSubmit = (
    values: Values,
    { setStatus, setSubmitting }: FormikHelpers<Values>,
  ) => {
    setStatus();
    setSubmitting(true);
    accountService
      .forgotPassword(values)
      .then(() => {
        alertService.success(
          'Email submitted succesfully, please check your email for instructions',
          { keepAfterRouteChange: true },
        );
        setSuccessSubmit(true);
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  return (
    <>
      {successSubmit ? (
        <CheckEmail
          title="We sent you and email to reset your password"
          message="Please check your email for a confirmation link."
        />
      ) : (
        <div className={styles.form__container}>
          <h1 className={styles.form_title}>
            Please enter your email to reset your password.
          </h1>
          <p className="mb-4">
            Assistive text nisl vel lacinia aliquet, nisl neque blandit risus,
            cursus vulputate ligula odio id erat.
          </p>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
          >
            {({ errors, touched, isSubmitting }) => (
              <Form>
                <InputFormikField
                  name="email"
                  label="Email"
                  type="text"
                  errors={errors}
                  touched={touched}
                />
                <div className="form-group">
                  <div className="d-grid gap-2 mt-4">
                    <Button
                      type="submit"
                      disabled={isSubmitting}
                      variant="info"
                      size="lg"
                    >
                      {isSubmitting && (
                      <span className="spinner-border spinner-border-sm mr-1" />
                      )}
                      Submit
                    </Button>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
          <div className="mt-4">
            <span className={styles.signin_text}>
              Already have an account?
            </span>
            {' '}
            <Link to="login" className={`btn btn-link ${styles.signin_link}`}>
              Sign in
            </Link>
          </div>
        </div>
      )}
    </>
  );
};
