import React from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import styles from './Login.module.scss';
import { alertService } from '../../../_services/alert.service';
import { accountService } from '../../../_services/account.service';
import InputFormikField from '../../../components/_forms/InputFormikField';

interface LoginValues {
  email: string;
  password: string;
}

interface LoginProps {
  history: any;
}

export const Login = ({ history }: LoginProps) => {
  const initialValues: LoginValues = {
    email: '',
    password: '',
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().email('Email is invalid').required('Email is required'),
    password: Yup.string()
      .required('Password is required')
      .min(6, 'Password should be of minimum 8 characters length'),
  });

  const onSubmit = (
    { email, password }: LoginValues,
    { setSubmitting }: FormikHelpers<LoginValues>,
  ) => {
    alertService.clear();
    accountService
      .login(email, password)
      .then(() => {
        history.push('two-factor-authentication');
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  return (
    <div className={styles.form__container}>
      <h1 className={styles.form_title}>Please sign in.</h1>
      <p className="mb-4">
        Assistive text nisl vel lacinia aliquet, nisl neque blandit risus,
        cursus vulputate ligula odio id erat.
      </p>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({ errors, touched, isSubmitting }) => (
          <Form>
            <InputFormikField
              name="email"
              label="Email"
              type="text"
              errors={errors}
              touched={touched}
              variant="top_combo"
            />
            <InputFormikField
              name="password"
              label="Password"
              type="password"
              errors={errors}
              touched={touched}
              variant="bottom_combo"
            />
            <div className="form-row">
              <div className="form-group col">
                <div className="d-grid gap-2 mt-4">
                  <Button
                    type="submit"
                    disabled={isSubmitting}
                    variant="info"
                    size="lg"
                  >
                    {isSubmitting && (
                    <span className="spinner-border spinner-border-sm mr-1" />
                    )}
                    Submit
                  </Button>
                </div>
              </div>
              <div className="form-group col text-right">
                <Link to="forgot-password" className="">
                  Forgot Password?
                </Link>
              </div>
            </div>
          </Form>
        )}
      </Formik>
      <div>
        <span className={styles.signin_text}>You don’t have an account?</span>
        {' '}
        <Link to="register" className={`btn btn-link ${styles.signin_link}`}>
          Sign up
        </Link>
      </div>
    </div>
  );
};
