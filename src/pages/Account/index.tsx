import React, { useEffect } from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';

import { CheckEmailCode } from './CheckEmailCode';

import { Login } from './Login';
import { Register } from './Register';
import { VerifyEmail } from '../VerifyEmail';
import { ForgotPassword } from './ForgotPassword';
import CheckEmail from '../CheckEmail';

interface AccountProps extends RouteComponentProps {}

export const Account = ({ history, match }: AccountProps) => {
  const { path } = match;

  useEffect(() => {
    // redirect to home if already logged in
    if (JSON.parse(localStorage.getItem('user') || 'null')) {
      history.push('/');
    }
  }, []);

  return (
    <>
      <Switch>
        <Route path={`${path}/login`} component={Login} />
        <Route
          path={`${path}/two-factor-authentication`}
          component={CheckEmailCode}
        />
        <Route
          path={`${path}/check-email`}
          component={() => (
            <CheckEmail
              title="Thank you for Joining ZUNO!"
              message="Please check your email for a confirmation link."
            />
          )}
        />
        <Route path={`${path}/register`} component={Register} />
        <Route path={`${path}/verify-email`} component={VerifyEmail} />
        <Route path={`${path}/forgot-password`} component={ForgotPassword} />
      </Switch>
    </>
  );
};
