import React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { Button } from 'react-bootstrap';
import styles from './Register.module.scss';
import { alertService } from '../../../_services/alert.service';
import { accountService } from '../../../_services/account.service';
import InputFormikField from '../../../components/_forms/InputFormikField';

interface RegisterProps extends RouteComponentProps {}

export const Register = ({ history }: RegisterProps) => {
  interface Values {
    email: string;
  }
  const initialValues: Values = {
    email: '',
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().email('Email is invalid').required('Email is required'),
  });

  const onSubmit = (
    values: Values,
    { setStatus, setSubmitting }: FormikHelpers<Values>,
  ) => {
    setStatus();
    setSubmitting(true);
    accountService
      .register(values)
      .then((response) => {
        if (response.data.state === 1) {
          history.push('/set-password');
        } else {
          alertService.success(
            'Registration successful, please check your email for verification instructions',
            { keepAfterRouteChange: true },
          );
          history.push('check-email');
        }
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  return (
    <>
      <div className={styles.form__container}>
        <h1 className={styles.form_title}>Please sign up.</h1>
        <p className="mb-4">
          Assistive text nisl vel lacinia aliquet, nisl neque blandit risus,
          cursus vulputate ligula odio id erat.
        </p>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
        >
          {({ errors, touched, isSubmitting }) => (
            <Form>
              <InputFormikField
                name="email"
                label="Email"
                type="text"
                errors={errors}
                touched={touched}
              />
              <div className="form-group">
                <div className="d-grid gap-2 mt-4">
                  <Button
                    type="submit"
                    disabled={isSubmitting}
                    variant="info"
                    size="lg"
                  >
                    {isSubmitting && (
                      <span className="spinner-border spinner-border-sm mr-1" />
                    )}
                    Submit
                  </Button>
                </div>
              </div>
            </Form>
          )}
        </Formik>
        <div className="mt-4">
          <span className={styles.signin_text}>
            Already have an account?
          </span>
          {' '}
          <Link to="login" className={`btn btn-link ${styles.signin_link}`}>
            Sign in
          </Link>
        </div>
      </div>
    </>
  );
};
