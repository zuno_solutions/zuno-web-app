import React from 'react';
import { Button, Col } from 'react-bootstrap';
import styles from './checkEmailCode.module.scss';
import emailIcon from '../../../assets/images/email_icon.png';
import PinCodeInput from '../../../components/PinCodeInput';
import { accountService } from '../../../_services/account.service';
import { alertService } from '../../../_services/alert.service';

export const CheckEmailCode = () => {
  const email = accountService.emailValue;

  const handleResendOTP = () => {
    accountService
      .resendOTP()
      .then(() => {
        alertService.success('6 digit code sent');
      })
      .catch((error) => {
        alertService.error(error);
      });
  };

  return (
    <Col className="d-flex justify-content-center">
      <div className="align-self-center text-center">
        <div className={styles.check_email_img__container}>
          <img src={emailIcon} alt="Email confirmation icon" />
        </div>
        <h1 className={styles.check_email__title}>Check your email.</h1>
        <p>
          We sent a 6-digit code to
          {' '}
          {email}
          {' '}
          <br />
          {' '}
          Please enter it below. Can’t
          find it? Check your spam folder.
        </p>
        <PinCodeInput />
        <Button variant="link" onClick={handleResendOTP}>
          <strong className={styles.check_email__link}>
            Click here to send a new code
          </strong>
        </Button>
        {' '}
        <p className={styles.check_email__text_link}>
          Noticed a typo?
          {' '}
          <a href="/SignIn">
            <strong className={styles.check_email__link}>
              Fix your email address
            </strong>
          </a>
          {' '}
        </p>
      </div>
    </Col>
  );
};
