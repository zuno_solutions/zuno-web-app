/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import { Layout } from '../../components/Layout';
import { SetAccount } from '../../components/SetAccount';
import { PaymentToAccounting } from '../../components/PaymentToAccounting';
import { OnboardingORD } from '../../components/_onboardingORD';
import PrivateRoute from '../../auth/PrivateRoute';
import { Role } from '../../_helpers/role';
import { VerifyEpicToken } from '../VerifyEpicToken';
import { ITToDirector } from '../../components/ITToDirector';
import { accountService } from '../../_services/account.service';

interface OnboardingPageProps {
  match: any;
}

export const OnboardingSteps = {
  HospitalData: 'hospital-data',
  PersonalData: 'personal-data',
  PaymentData: 'payment-data',
  ITData: 'it-data',
  ORDData: 'ord-data',
};

export const OnboardingPage = ({ match }: OnboardingPageProps) => {
  const [onboardingState, setOnboardingState] = useState(
    OnboardingSteps.HospitalData,
  );
  const userState = accountService.userValue?.state || 0;
  const hospitalState = accountService.hospitalValue?.state || 0;

  const getOnboardingState = () => {
    if (!hospitalState) {
      setOnboardingState(OnboardingSteps.HospitalData);
    } else if (hospitalState === 1 && userState < 3) {
      setOnboardingState(OnboardingSteps.PersonalData);
    } else if (hospitalState === 1 && userState > 2) {
      setOnboardingState(OnboardingSteps.PaymentData);
    } else if (hospitalState === 2 && userState > 2) {
      setOnboardingState(OnboardingSteps.ITData);
    } else if (hospitalState > 2 && userState > 2) {
      setOnboardingState(OnboardingSteps.ORDData);
    } else {
      setOnboardingState(OnboardingSteps.ITData);
    }
  };

  useEffect(() => {
    getOnboardingState();
  }, [userState, hospitalState]);

  const { path } = match;
  const steps = Object.values(OnboardingSteps);
  const step = steps.indexOf(onboardingState) + 1;
  const totalSteps = steps.length;

  const updateOnboardingStateFromForm = () => {
    setOnboardingState(steps[step]);
  };

  return (
    <Row className="justify-content-center">
      <Col className="px-0 align-self-center text-center">
        <Switch>
          <Route
            exact
            path={`${path}/`}
            component={() => (
              <SetAccount
                roles={[Role.VP]}
                onboardingStatus={onboardingState}
                setOnboardingState={updateOnboardingStateFromForm}
              />
            )}
          />
          <PrivateRoute
            path={`${path}/payment`}
            roles={[Role.ACCOUNTANT]}
            component={() => (
              <PaymentToAccounting
                setOnboardingState={updateOnboardingStateFromForm}
              />
            )}
          />
          <PrivateRoute
            path={`${path}/it`}
            roles={[Role.IT_DIRECTOR]}
            component={() => <ITToDirector />}
          />
          <PrivateRoute path={`${path}/ord`} roles={[Role.ORD, Role.VP]} component={() => <OnboardingORD />} />
          <Route path={`${path}/verify-token`} component={VerifyEpicToken} />
        </Switch>
      </Col>
    </Row>
  );
};
