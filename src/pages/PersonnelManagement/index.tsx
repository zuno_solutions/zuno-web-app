/* eslint-disable react/require-default-props */
import React from 'react';
import {
  Button, Col, Container, Row,
} from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import medicalStaffIcon from '../../assets/images/medical_staff_icon.png';
import { OnboardingTitle } from '../../components/OnboardingTitle/OnboardingTitle';
import { PersonnelManagementTable } from '../../components/PersonnelManagementTable';

interface ORDPersonnelManagementProps {
  updateOnboardingStateFromForm?: () => void;
}

export const PersonnelManagement = ({
  updateOnboardingStateFromForm,
}: ORDPersonnelManagementProps) => {
  const history = useHistory();
  return (
    <Container fluid className="text-center">
      <Row>
        <OnboardingTitle
          image={medicalStaffIcon}
          assistiveText="Upload the staff list and we will synchronize it with your ZUNO and EPIC account."
          title="Personnel Management"
        />
      </Row>
      <Row className="mx-5">
        <PersonnelManagementTable />
      </Row>
      {updateOnboardingStateFromForm && (
      <Row className="justify-content-center mt-2">
        <Col md={5} className="d-grid gap-2">
          <Button
            variant="info"
            size="lg"
            onClick={() => history.push('/dashboard')}
          >
            Continue
          </Button>
        </Col>
      </Row>
      )}
    </Container>
  );
};
