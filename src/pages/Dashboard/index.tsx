/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useRef, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import styles from './Dashboard.module.scss';
import { SwimmingLanes } from '../../components/SwimmingLanes/SwimmingLanes';
import { NotificationsSidebar } from '../../components/NotificationsSidebar';
import generateFakeData from '../../_services/generate-fake-data';
import { RoomsConsole } from '../../components/RoomsConsole/RoomsConsole';
import { TodayConsole } from '../../components/TodayConsole/TodayConsole';
import { PatientConsole } from '../../components/PatientConsole/PatientConsole';
import { ProcedureConsole } from '../../components/ProcedureConsole/ProcedureConsole';
import { VendorConsole } from '../../components/VendorConsole/VendorConsole';

export interface IOperationRoom {
  id: string,
  title: any,
  rightTitle: any,
  stackItems?: boolean,
  height?: number,
  color: string
}

export interface ISurgeryItem {
  id: string;
  group: any;
  title: string;
  surgeon: string;
  status: string;
  start_time: number;
  end_time: number;
  calendarDetails: string;
  canMove: boolean;
  canResize: string | boolean;
  className: string;
  personnel: string;
  anesthesiologistStatus: string;
  anesthesiologist: string;
  patient: {name: string, emergencyContact: string, age: number}
  surgeonStatus: string
  late: number
  trays: number
  duration: string
  vendor: string
  vendorStatus: string
  manufacturer: string
  traysStatus: string
}

export const Dashboard = () => {
  const [selectedRoom, setSelectedRoom] = useState<number | string>();
  const [selectedItem, setSelectedItem] = useState<ISurgeryItem>();
  const [selectedPersonnel, setSelectedPersonnel] = useState<string>();
  const [operationRooms, setOperationRooms] = useState<
  IOperationRoom[]
  >([]);
  const [surgeries, setSurgeries] = useState<
  ISurgeryItem[]
  >([]);
  const [sideBarStatus, setSideBarStatus] = useState<string>('Hospital Notifications');

  const [sideBarCollapsed, setSideCollapsed] = useState<boolean>(true);

  useEffect(() => {
    const { groups, items } = generateFakeData(20);
    setOperationRooms(groups);
    setSurgeries(items);
  }, []);

  const isInitialMount = useRef(true);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
    } else {
      // Your useEffect code here to be run on update
      const newArr = [...surgeries]; // copying the old datas array
      const selectedSurgeries = newArr.map((element) => {
        if (element.group === Number(selectedRoom) || (element.personnel === selectedPersonnel)) {
          return { ...element, selectedRoom: true };
        } return { ...element, selectedRoom: false };
      });
      setSurgeries(selectedSurgeries);
    }
  }, [selectedRoom, selectedPersonnel]);

  const handleRoomSelection = (room: string | number) => {
    setSelectedRoom(Number(room));
    setSelectedPersonnel(undefined);
  };

  const handleItemSelection = (item: ISurgeryItem) => {
    setSelectedRoom(undefined);
    setSelectedPersonnel(undefined);
    setSelectedItem(item);
  };

  const handlePersonnelSelection = (personnel: string) => {
    setSelectedPersonnel(personnel);
    setSelectedRoom(undefined);
  };

  const handleCollapseSideBar = () => {
    setSideCollapsed(!sideBarCollapsed);
  };

  return (
    <Container fluid className={`mt-0 pb-3 mx-0 ${styles.container}`}>
      <Row>
        <Col className={sideBarCollapsed ? styles.timeline_collapsed : styles.timeline_open}>
          <Row className={`mt-2 ${styles.swimming_lanes}`}>
            <SwimmingLanes
              setSurgeries={setSurgeries}
              operationRooms={operationRooms}
              surgeries={surgeries}
              selectedRoom={selectedRoom}
              selectedPersonnel={selectedPersonnel}
              handleItemSelection={handleItemSelection}
            />

          </Row>
          <Row className={`mt-3 ${styles.panel}`}>
            {selectedItem === undefined
            && (
            <>
              <Col sm={4}>
                <RoomsConsole operationRooms={operationRooms} surgeries={surgeries} handleRoomSelection={handleRoomSelection} selectedRoom={selectedRoom} />
              </Col>
              <Col sm={8}><TodayConsole selectedPersonnel={selectedPersonnel} operationRooms={operationRooms} surgeries={surgeries} handlePersonnelSelection={handlePersonnelSelection} /></Col>
            </>
            )}
            {selectedItem !== undefined
            && (
            <>
              <Col sm={4}>
                <PatientConsole item={selectedItem} />
              </Col>
              <Col sm={4}>
                <ProcedureConsole item={selectedItem} />
              </Col>
              <Col sm={4}>
                <VendorConsole item={selectedItem} />
              </Col>

            </>
            )}
          </Row>
        </Col>
        <Col className={`${sideBarCollapsed ? styles.sidebar_collapsed : styles.sidebar_open}`}>
          <NotificationsSidebar
            title={sideBarStatus}
            handleCollapseSideBar={handleCollapseSideBar}
            sideBarCollapsed={sideBarCollapsed}
          />
        </Col>
      </Row>
    </Container>
  );
};
