import React from 'react';
import { SetPassword } from '../../components/_forms/SetPassword';
import styles from './SetYourPassword.module.scss';

export const SetYourPassword = () => (
  <div className={styles.form__container}><SetPassword /></div>
);
