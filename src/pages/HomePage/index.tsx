import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import { accountService } from '../../_services/account.service';
import './homePage.module.scss';

export const HomePage = () => {
  // const hospital = JSON.parse(localStorage.getItem("hospital") || "{}");
  const hospital = localStorage.getItem('hospital') !== null
    ? JSON.parse(localStorage.getItem('hospital')!)
    : null;
  const { roomsValue } = accountService || null;

  if (
    !hospital.state
    || hospital.state < 4
    || (roomsValue && roomsValue[0]?.roomsCount === 0)
  ) {
    // user has not completed onboarding, so redirect to
    return <Redirect to={{ pathname: '/set-account' }} />;
  }
  return (
    <Row className="justify-content-center mt-2">
      <Col md={5}>
        <h1>Welcome to Zuno</h1>
      </Col>
    </Row>
  );
};
