import queryString from 'query-string';
import React, { useEffect, useState } from 'react';
import { SetPassword } from '../../components/_forms/SetPassword';
import { accountService } from '../../_services/account.service';
import { alertService } from '../../_services/alert.service';
import styles from './VerifyEmail.module.scss';

interface VerifyEmailProps extends RouteComponentProps {}

export const VerifyEmail = ({ history, location }: VerifyEmailProps) => {
  const EmailStatus = {
    Verifying: 'Verifying',
    Failed: 'Failed',
    Verified: 'Verified',
  };

  const [emailStatus, setEmailStatus] = useState(EmailStatus.Verifying);

  const handleTokenValidation = async (token: string | string[] | null) => {
    if (location.pathname === '/verify-email') {
      return accountService.verifyEmail(token);
    }
    return accountService.acceptInvite(token);
  };

  useEffect(() => {
    const { token } = queryString.parse(location.search);

    // remove token from url to prevent http referer leakage
    history.replace(location.pathname);
    handleTokenValidation(token)
      .then(() => {
        alertService.success('Verification ucessfull', {
          keepAfterRouteChange: true,
        });
        setEmailStatus(EmailStatus.Verified);
      })
      .catch((error) => {
        setEmailStatus(EmailStatus.Failed);
        alertService.error(error);
      });
  }, []);

  function getBody() {
    switch (emailStatus) {
      case EmailStatus.Verifying:
        return <div>Verifying...</div>;
      case EmailStatus.Verified:
        return <SetPassword />;
      case EmailStatus.Failed:
        return (
          <div>
            Verification failed
          </div>
        );
      default: throw new Error('Invalid Email Status');
    }
  }

  return (
    <div className={styles.form__container}>{getBody()}</div>
  );
};
