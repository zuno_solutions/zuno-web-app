/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import queryString from 'query-string';
import { Col } from 'react-bootstrap';
import styles from './VerifyEpicToken.module.scss';
import { IUser, accountService } from '../../_services/account.service';
import { EpicConnection } from '../../components/EpicConnection';
import { OnboardingTitle } from '../../components/OnboardingTitle/OnboardingTitle';
import hospitalIcon from '../../assets/images/hospital_icon.png';
import { Modal } from '../../components/_modals/Modal';
import successLogo from '../../assets/images/check_circle_outlined.png';

interface VerifyEpicTokenProps extends RouteComponentProps {}

export const VerifyEpicToken = ({
  history,
  location,
}: VerifyEpicTokenProps) => {
  const TokenStatus = {
    Verifying: 'Verifying',
    Failed: 'Failed',
    Verified: 'Verified',
  };

  const [tokenStatus, setTokenStatus] = useState(TokenStatus.Verifying);
  const user: IUser = JSON.parse(localStorage.getItem('user') || 'null');

  const [showModalSuccess, setShowModalSuccess] = useState<boolean>(false);
  const onCloseModalSuccess = () => {
    setShowModalSuccess(false);
    if (user.role === 1) {
      history.push('/set-account');
    } else {
      history.push('/dashboard');
    }
  };
  const onOpenModalSuccess = () => {
    setShowModalSuccess(true);
  };

  const handleTokenValidation = async (token: string | string[] | null) => {
    await accountService.verifyEpicToken(token);
  };

  const logOutAndConfirm = () => {
    accountService.logout();
  };

  useEffect(() => {
    const { code } = queryString.parse(location.search);

    // remove token from url to prevent http referer leakage
    history.replace(location.pathname);

    handleTokenValidation(code)
      .then(() => {
        setTokenStatus(TokenStatus.Verified);
      })
      .catch(() => {
        setTokenStatus(TokenStatus.Failed);
      });
  }, []);

  function getBody() {
    switch (tokenStatus) {
      case TokenStatus.Verifying:
        return <div>Verifying...</div>;
      case TokenStatus.Verified:
        return (
          <Col>
            <OnboardingTitle
              image={hospitalIcon}
              assistiveText="Assistive text indicating indicating the purpose of this screen: after accepting the invite billing contact will proceed."
              title="IT"
            />
            <p>
              Assistive text indicating indicating the purpose of this screen:
              ORD is comfortable that the data has been pulled through.
            </p>
            <EpicConnection
              confirmButtonLabel="Continue"
              cancelButtonLabel="Save and complete later"
              confirmButtonClick={onOpenModalSuccess}
              cancelButtonClick={logOutAndConfirm}
            />
          </Col>
        );
      case TokenStatus.Failed:
        return <div>Verification failed, please try again.</div>;
      default:
        throw new Error('Invalid Token Status');
    }
  }

  const getHeader = (image: string | undefined) => (
    <div className={styles.modal_image_header}>
      <img src={image} alt="log out logo" />
    </div>
  );

  return (
    <>
      <Modal
        show={showModalSuccess}
        onHide={onCloseModalSuccess}
        title={getHeader(successLogo)}
        closeButtonText="Ok"
        variantClose="info"
      >
        <>
          <h1>Success!</h1>
          <p>
            We have notified the ORD VPName and VPTitle VPTTitle that the
            account is ready to go. Zuno will send you an email if there is any
            further action needed. Thank you!
          </p>
        </>
      </Modal>
      {getBody()}
    </>
  );
};
