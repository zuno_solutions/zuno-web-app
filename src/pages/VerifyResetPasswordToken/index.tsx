import React, { useEffect, useState } from 'react';
import queryString from 'query-string';
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons';
import { Form, Formik, FormikHelpers } from 'formik';
import { RouteComponentProps } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { accountService } from '../../_services/account.service';
import styles from './VerifyResetPasswordToken.module.scss';
import { alertService } from '../../_services/alert.service';
import validationSchema from './validationSchema';
import InputFormikField from '../../components/_forms/InputFormikField';

interface Values {
  confirmPassword: string;
  password: string;
}

interface VerifyResetPasswordTokenProps extends RouteComponentProps {}

export const VerifyResetPasswordToken = ({
  history,
  location,
}: VerifyResetPasswordTokenProps) => {
  const [passwordShown, setPasswordShown] = useState<any>({
    password: false,
    confirmPassword: false,
  });
  const [verificationToken, setVerificationToken] = useState<any>('');

  useEffect(() => {
    const { token } = queryString.parse(location.search);
    setVerificationToken(token);
    // remove token from url to prevent http referer leakage
    history.replace(location.pathname);
  }, []);

  const togglePasswordVisiblity = (state: string) => {
    setPasswordShown((prevState: any) => (passwordShown[state]
      ? { ...prevState, [state]: false }
      : { ...prevState, [state]: true }));
  };

  const initialValues = {
    password: '',
    confirmPassword: '',
  };
  const onSubmit = (
    values: Values,
    { setStatus, setSubmitting }: FormikHelpers<Values>,
  ) => {
    const { password } = values;
    setStatus();
    setSubmitting(true);
    accountService
      .resetPassword({ token: verificationToken, password })
      .then(() => {
        alertService.success(
          'Your password has been successfully changed, please login',
          {
            keepAfterRouteChange: true,
          },
        );
        history.push('login');
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  return (
    <div className={styles.form__container}>
      <h1 className={styles.form_title}>Set your password.</h1>
      <p>
        Requirements: min 6 characters, at least 1 capital, 1 Number and 1
        special character.
      </p>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({ errors, touched, isSubmitting }) => (
          <Form>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="Enter your password"
                name="password"
                type={passwordShown.password ? 'text' : 'password'}
                icon={passwordShown.password ? faEyeSlash : faEye}
                onClickIcon={() => togglePasswordVisiblity('password')}
              />
            </div>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="Reenter password"
                name="confirmPassword"
                type={passwordShown.confirmPassword ? 'text' : 'password'}
                icon={passwordShown.confirmPassword ? faEyeSlash : faEye}
                onClickIcon={() => togglePasswordVisiblity('confirmPassword')}
              />
            </div>
            <div className={`d-grid gap-2 ${styles.submit__button}`}>
              <Button
                type="submit"
                disabled={isSubmitting}
                variant="info"
                size="lg"
              >
                Continue
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
