import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { Role } from '../_helpers/role';
import { IUser } from '../_services/account.service';

const PrivateRoute: React.FC<any> = ({
  component: Component,
  roles,
  ...rest
}: {
  component: React.ComponentType<RouteProps>;
  roles: Role[];
}) => (
  <Route
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...rest}
    render={(props: RouteProps) => {
      const user: IUser = JSON.parse(localStorage.getItem('user') || 'null');
      if (!user) {
        // not logged in so redirect to login page with the return url
        return (
          <Redirect
            to={{ pathname: '/account/login', state: { from: props.location } }}
          />
        );
      }

      // check if route is restricted by role
      if (roles && roles.indexOf(user.role) === -1) {
        // role not authorized so redirect to home page
        return <Redirect to={{ pathname: '/' }} />;
      }

      // authorized so return component
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <Component {...props} />;
    }}
  />
);

export default PrivateRoute;
