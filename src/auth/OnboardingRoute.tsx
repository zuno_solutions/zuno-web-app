import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router';
import { Role } from '../_helpers/role';
import { IUser } from '../_services/account.service';

const OnboardingRoute: React.FC<any> = ({
  component: Component,
  roles,
  ...rest
}: {
  component: React.ComponentType<RouteProps>;
  roles: Role[];
}) => (
  <Route
    {...rest}
    render={(props) => {
      const user: IUser = JSON.parse(localStorage.getItem('user') || 'null');
      if (!user) {
        // not logged in so redirect to login page with the return url
        return (
          <Redirect
            to={{ pathname: '/account/login', state: { from: props.location } }}
          />
        );
      }

      // check if route is restricted by role
      if (roles && roles.indexOf(user.role) === -1) {
        // role not authorized so redirect to home page
        return <Redirect to={{ pathname: '/' }} />;
      }

      // authorized so return component
      return <Component {...props} />;
    }}
  />
);

export default OnboardingRoute;
