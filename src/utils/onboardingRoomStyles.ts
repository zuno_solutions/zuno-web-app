export const onboardingRoomStyles: {
  variant: 'top' | 'bottom';
  position: 'right' | 'left';
}[] = [
  { variant: 'top', position: 'right' },
  { variant: 'top', position: 'left' },
  { variant: 'top', position: 'right' },
  { variant: 'top', position: 'left' },
  { variant: 'bottom', position: 'right' },
  { variant: 'bottom', position: 'left' },
  { variant: 'bottom', position: 'right' },
  { variant: 'bottom', position: 'left' },
];
