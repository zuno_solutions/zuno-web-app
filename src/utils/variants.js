export const bootstrapButtonVariants = [
  'primary',
  'secondary',
  'success',
  'warning',
  'danger',
  'info',
  'light',
  'dark',
  'outline-primary',
  'outline-secondary',
  'outline-success',
  'outline-warning',
  'outline-danger',
  'outline-info',
  'outline-light',
  'outline-dark',
];

export const customButtonVariants = [
  'unstyled-primary',
  'unstyled-secondary',
  'unstyled-success',
  'unstyled-warning',
  'unstyled-danger',
  'unstyled-info',
  'unstyled-light',
  'unstyled-dark',
  'no-padding-primary',
  'no-padding-secondary',
  'no-padding-success',
  'no-padding-warning',
  'no-padding-danger',
  'no-padding-info',
  'no-padding-light',
  'no-padding-dark',
];

export const buttonVariants = [
  ...bootstrapButtonVariants,
  ...customButtonVariants,
];

export const linkVariants = [
  'primary',
  'secondary',
  'success',
  'warning',
  'danger',
  'info',
  'light',
  'dark',
  'muted',
  'white',
  'unstyled-light',
  'unstyled-dark',
];

export const dropdownVariants = [
  ...bootstrapButtonVariants,
  ...customButtonVariants,
];
