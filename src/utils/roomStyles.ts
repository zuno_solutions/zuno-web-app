export const roomConsoleStyles: {
  color: 'string';
}[] = [
  { color: '#68DDED' },
  { color: '#15C0ED' },
  { color: '#139CFF' },
  { color: '#2296D7' },
  { color: '#4FA2EE' },
  { color: '#4E67C0' },
  { color: '#5876BA' },
  { color: '#6B7FAE' },
];
