import './custom.scss';
import React from 'react';
import { render } from 'react-dom';
import { Router } from 'react-router-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { history } from './_helpers/history';
import { accountService } from './_services/account.service';

function startApp() {
  render(
    <Router history={history}>
      <App />
    </Router>,
    document.getElementById('root'),
  );
}

accountService.getUserData().then(() => accountService.getRoomsData()).finally(startApp).catch(startApp);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
