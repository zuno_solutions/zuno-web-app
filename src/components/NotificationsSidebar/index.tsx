/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import 'react-pro-sidebar/dist/css/styles.css';
import { faEye } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCompressAlt } from '@fortawesome/free-solid-svg-icons';
import { Col, Row } from 'react-bootstrap';
import { SyringeIcon } from '../../_icons/syringe';
import styles from './NotificationsSidebar.module.scss';
import generateFakeData from '../../_services/generate-fake-notification';
import { NotificationItem } from '../NotificationItem';

interface NotificationsSidebarProps {
  title: string;
  handleCollapseSideBar: () => void;
  sideBarCollapsed: boolean;
}

export const NotificationsSidebar = ({ title, sideBarCollapsed, handleCollapseSideBar }: NotificationsSidebarProps) => {
  const { groups, items } = generateFakeData(7);

  return (
    <div className={`p-3 ${styles.container}`}>
      <Row
        className={`justify-content-between ${styles.header}`}
      >
        {!sideBarCollapsed && (
        <Col className=" my-auto ">
          <p>
            {title}
          </p>
        </Col>
        )}
        <Col xs="auto">
          <button
            type="button"
            onClick={handleCollapseSideBar}
            className={styles.collapse_button}
          >
            <FontAwesomeIcon icon={faCompressAlt} />
          </button>
        </Col>
      </Row>
      <div
        className={`${styles.notification}`}
      >
        {items.map((item) => (
          <NotificationItem key={item.id} item={item} sideBarCollapsed={sideBarCollapsed} />
        ))}
      </div>
    </div>
  );
};
