/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import SelectFormikField from '../_forms/SelectFormikField';
import { positionNewPersonnelInvite } from '../../types/positionNewPersonnelInvite';
import validationSchema from './validationSchema';
import { InviteValues } from '../_forms/InviteForm';
import styles from './ViewSurgeon.module.scss';
import { TelephoneInputIntl } from '../_forms/TelephoneInputIntl';
import SelectFormikFieldOptions from '../_forms/SelectFormikFieldOptions';
import { personnelService } from '../../_services/personnel.service';
import DisabledInputFormikField from '../_forms/DisabledInputFormikField';

interface ViewSurgeonValues {
  position: string;
  firstName: string;
  lastName: string;
  email: string;
  mainPhoneNumber: string;
  companyName: string;
  companyAddress: string;
  companyMainPhoneNumber: string;
}

interface ViewSurgeonProps {
  row: any;
}

export const ViewSurgeon = ({ row }: ViewSurgeonProps) => {
  const [position, setPosition] = useState<string>('6');
  const [firstName, setFirstName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [mainPhoneNumber, setMainPhoneNumber] = useState<string>('');
  const [companyName, setCompanyName] = useState<string>('');
  const [companyAddress, setCompanyAddress] = useState<string>('');
  const [companyMainPhoneNumber, setCompanyMainPhoneNumber] = useState<string>('');

  const initialValues = {
    position,
    firstName,
    lastName,
    email,
    mainPhoneNumber,
    companyName,
    companyAddress,
    companyMainPhoneNumber,
  };

  const onSubmit = (
    values: ViewSurgeonValues,
    { setStatus, setSubmitting }: FormikHelpers<ViewSurgeonValues>,
  ) => console.log(values);

  useEffect(() => {
    const getPersonnelData = async () => personnelService.getRawPersonnelById(row.id);
    getPersonnelData().then((data) => {
      setFirstName(data.rawPersonnel.first_name);
      setLastName(data.rawPersonnel.last_name);
      setEmail(data.rawPersonnel.email);
      setMainPhoneNumber(data.rawPersonnel.phone);
      // setCompanyName(data)
      // setCompanyAddress(data)
      // setCompanyMainPhoneNumber(data)
    });
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {({
        errors, touched, isSubmitting, setFieldValue,
      }) => (
        <Form>
          <Row className="mt-2">
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col>
                  <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    Personnel info
                  </p>
                </Col>
              </Row>
              <Row>
                <Col className="px-1 pb-5">
                  <div className={styles.input__container}>
                    <SelectFormikFieldOptions
                      name="position"
                      label="Position"
                      type="text"
                      options={positionNewPersonnelInvite}
                      // handleChange={handleChange}
                      setFieldValue={setFieldValue}
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="First Name"
                      name="firstName"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Last Name"
                      name="lastName"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Email"
                      name="email"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <label
                      htmlFor="mainPhone"
                      className="label_intl_phone_element"
                    >
                      Main Phone Number
                    </label>
                    <TelephoneInputIntl
                      name="mainPhoneNumber"
                      id="mainPhoneNumber"
                      setFieldValue={setFieldValue}
                      disabled
                      defaultValue={mainPhoneNumber}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col>
                  <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    Company / Distributor
                  </p>
                </Col>
              </Row>
              <Row>
                <Col className="px-1 pb-5">
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Name"
                      name="companyName"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Address"
                      name="companyAddress"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <label
                      htmlFor="mainPhone"
                      className="label_intl_phone_element"
                    >
                      Main Phone Number
                    </label>
                    <TelephoneInputIntl
                      setFieldValue={setFieldValue}
                      name="companyMainPhoneNumber"
                      id="companyMainPhoneNumber"
                      disabled
                    />
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      )}
    </Formik>
  );
};
