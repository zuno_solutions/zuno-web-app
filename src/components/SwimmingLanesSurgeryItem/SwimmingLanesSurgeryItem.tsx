/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import moment, { Moment } from 'moment';
import styles from './SwimmingLanesSurgeryItem.module.scss';

export const SwimmingLanesSurgeryItem = ({
  item,
  itemContext,
  getItemProps,
  getResizeProps,
  selectedRoom,
  selectedPersonnel,
  handleItemSelection,
}: {
  item: any;
  itemContext: any;
  getItemProps: any;
  getResizeProps: any;
  selectedRoom: number | string | undefined
  selectedPersonnel: string | undefined,
  handleItemSelection: (item: any) => void;
}) => {
  const { left: leftResizeProps, right: rightResizeProps } = getResizeProps();
  const backgroundColor = item.status === 'on time'
    ? '#5876BA' : (item.status === 'rescheduled' ? '#15C0ED'
      : '#FF3B30');

  const borderColor = itemContext.resizing || itemContext.dragging ? 'red' : backgroundColor;
  const opacity = (selectedRoom === undefined && selectedPersonnel === undefined) || item.selectedRoom ? 1 : 0.5;

  const itemProps: {style: {height: string | number, top: string | number}} = getItemProps({
    style: {
      background: backgroundColor,
      opacity,
      color: item.color,
      borderLeft: `${itemContext.selected ? 3 : 1}px solid ${borderColor}`,
      borderRight: `${itemContext.selected ? 3 : 1}px solid ${borderColor}`,
      borderRadius: 4,
      height: '400px',
    },
    onMouseDown: () => {
      handleItemSelection(item);
    },
  });
  itemProps.style.height = '106px';
  itemProps.style.top = itemContext.dimensions.top + (itemContext.dimensions.top / 100) * 14;

  return (
    <div
      {...itemProps}
    >
      {itemContext.useResizeHandle ? <div {...leftResizeProps} /> : null}

      <div
        style={{
          height: '100px',
          overflow: 'hidden',
          paddingLeft: 3,
          color: 'white',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap',
        }}
      >
        <p className={styles.surgeon}>
          {item.surgeon}
        </p>
        <p className={styles.surgery}>
          {item.title}
        </p>
        <p>
          {item.calendarDetails}
        </p>
      </div>

      {itemContext.useResizeHandle ? <div {...rightResizeProps} /> : null}
    </div>
  );
};
