import React, { Fragment, useState } from 'react';
import { Modal } from '../Modal';
import styles from './ModalWelcomeBack.module.scss';
import signOutLogo from '../../../assets/images/sign_out_logo.png';

interface ModalWelcomeBackProps {
  onHide: () => void;
  closeButtonText?: string;
  saveButtonText?: string;
  title?: string | JSX.Element;
  onSave: () => void;
  show: boolean;
}

export const ModalWelcomeBack = ({
  onHide,
  closeButtonText = 'Cancel',
  saveButtonText = 'Sign Out',
  title = 'Leave?',
  onSave,
  show,
  ...rest
}: ModalWelcomeBackProps) => {
  const logOutAndConfirm = () => {
    onSave();
  };

  const getHeader = (image: string | undefined) => (
    <div className={styles.modal_image_header}>
      <img src={image} alt="log out logo" />
    </div>
  );

  return (
    <Modal
      show={show}
      title={getHeader(signOutLogo)}
      // title={success ? <img src={signOutLogo} alt="Zuno app logo"/> : <img src={leaveLogo} alt="Zuno app logo"/>}
      onHide={onHide}
      saveButtonText="Resume"
      onSave={logOutAndConfirm}
      {...rest}
    >
      <>
        <h1>Welcome Back</h1>
        <p>
          We saved your progress so that you can now resume your ZUNO
          registration.
        </p>
      </>
    </Modal>
  );
};
