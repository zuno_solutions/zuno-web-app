import React from 'react';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Modal } from '../Modal';
import styles from './DeletePersonnelModal.module.scss';

interface DeletePersonnelModalProps {
  onHide: () => void;
  onSave: () => void;
  show: boolean;
}

export const DeletePersonnelModal = ({
  onHide,
  onSave,
  show,
}: DeletePersonnelModalProps) => {
  const header = (
    <div className={styles.modal_image_header}>
      <FontAwesomeIcon
        size="2x"
        className={styles.icon}
        icon={faTrashAlt}
        color="#FF3B30"
      />
    </div>
  );

  return (
    <Modal
      show={show}
      onHide={onHide}
      title={header}
      saveButtonText="Confirm"
      closeButtonText="Cancel"
      onSave={onSave}
    >
      <>
        <h1>Delete?</h1>
        <p>You are about to delete 1 personnel from your personnel list.</p>
      </>
    </Modal>
  );
};
