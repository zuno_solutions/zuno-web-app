import React from 'react';
import { Modal } from '../Modal';
import styles from './SuccessModal.module.scss';
import successLogo from '../../../assets/images/check_circle_outlined.png';

interface SuccessModalProps {
  show: boolean;
  onHide: () => void;
}

export const SuccessModal = ({
  show, onHide,
}: SuccessModalProps) => {
  const header = (
    <div className={styles.modal_image_header}>
      <img src={successLogo} alt="success logo" />
    </div>
  );

  return (
    <Modal
      onHide={onHide}
      show={show}
      title={header}
      closeButtonText="Close"
    >
      <>
        <h1>Success!</h1>
        <p>Success message after sending or schedule invite to all selected personnel</p>
      </>
    </Modal>
  );
};
