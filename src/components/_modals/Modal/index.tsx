import React from 'react';
import { Modal as ModalBootstrap } from 'react-bootstrap';
import classNames from 'classnames';
import Button from '../../Button';
import styles from './Modal.module.scss';

interface ModalProps {
  onHide: () => void;
  show: boolean;
  title: string | JSX.Element;
  closeButtonText?: string;
  saveButtonText?: string;
  onSave?: () => void;
  children: JSX.Element | JSX.Element[];
  withHeaderFooterBorder?: boolean;
  footer?: string | JSX.Element;
  variantClose?: string;
  variantSave?: string;
  dialogClassName?: string;
}

const defaultProps = {
  closeButtonText: null,
  saveButtonText: 'Sign Out',
  onSave: null,
  withHeaderFooterBorder: false,
  footer: null,
  variantClose: 'link',
  variantSave: 'info',
  dialogClassName: null,
};

const Modal = ({
  show,
  title,
  onHide,
  onSave,
  children,
  withHeaderFooterBorder,
  footer,
  closeButtonText,
  saveButtonText = 'Save changes',
  variantClose = 'link',
  variantSave = 'info',
  ...rest
}: ModalProps) => {
  const headerClass = classNames(styles.header, {
    [styles.header__without_border]: !withHeaderFooterBorder,
  });

  const footerClass = classNames(styles.footer, {
    [styles.footer__without_border]: !withHeaderFooterBorder,
  });

  return (
    <ModalBootstrap
      onHide={onHide}
      show={show}
      centered
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
    >
      <ModalBootstrap.Header className={headerClass}>
        <ModalBootstrap.Title className={styles.title}>
          {title}
        </ModalBootstrap.Title>
      </ModalBootstrap.Header>
      <ModalBootstrap.Body className={styles.body}>
        {children}
      </ModalBootstrap.Body>
      <ModalBootstrap.Footer className={footerClass}>
        {!footer && (
          <>
            {onSave ? (
              <Button
                className="w-100"
                size="lg"
                onClick={onSave}
                variant={variantSave}
              >
                {saveButtonText}
              </Button>
            ) : null}
            {closeButtonText ? (
              <Button
                size="lg"
                className="w-100"
                onClick={onHide}
                variant={variantClose}
              >
                {closeButtonText}
              </Button>
            ) : null}
          </>
        )}
        {footer}
      </ModalBootstrap.Footer>
    </ModalBootstrap>
  );
};

Modal.defaultProps = defaultProps;
export { Modal };
