import React, { createContext, useEffect, useState } from 'react';

export enum OnboardingTypes {
  HospitalData = 'hospital-data',
  PersonalData = 'personal-data',
  PaymentData = 'payment-data',
  ORDData = 'ord-data',
  ITData = 'it-data',
}

export interface IUserProfileContext {
  state: number;
}

export const userProfileContextDefaultValue: IUserProfileContext = {
  state: 0,
};

export const UserProfileContext = createContext<IUserProfileContext>(
  userProfileContextDefaultValue,
);

interface UserProfileContextProviderProps {
  children: JSX.Element | JSX.Element[];
}

export const UserProfileContextProvider = ({
  children,
}: UserProfileContextProviderProps) => {
  const [userProfile, setUserProfile] = useState(
    userProfileContextDefaultValue,
  );

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user') || 'null')
      || userProfileContextDefaultValue;
    setUserProfile(user);
    // const hospitalState = JSON.parse(
    //   localStorage.getItem('hospital') || '{}',
    // )?.state;
    // getUserProfile().then((res) => {
    //   setUserProfile(res);
    // });
  }, []);

  return (
    <UserProfileContext.Provider value={userProfile}>
      {children}
    </UserProfileContext.Provider>
  );
};
