import React, { createContext, useEffect, useState } from 'react';

export enum OnboardingTypes {
  HospitalData = 'hospital-data',
  PersonalData = 'personal-data',
  PaymentData = 'payment-data',
  ORDData = 'ord-data',
  ITData = 'it-data',
}

export interface IHospitalData {
  floorsNumber: string[];
  operationRooms: IOperationRooms[];
}

export enum MedicalSpeciaty {
  Anesthesiology = 'anesthesiology',
  Cardiology = 'cardiology',
}

export interface IOperationRooms {
  name: string;
  specialty: MedicalSpeciaty;
  floor: string;
}

export interface IHospitalDataContext {
  state: number;
}

export const hospitalDataContextDefaultValue: IHospitalDataContext = {
  state: 0,
};

export const HospitalDataContext = createContext<IHospitalDataContext>(
  hospitalDataContextDefaultValue,
);

interface HospitalDataContextProviderProps {
  children: JSX.Element | JSX.Element[];
}

export const HospitalDataContextProvider = ({
  children,
}: HospitalDataContextProviderProps) => {
  const [hospitalData, setHospitalData] = useState(
    hospitalDataContextDefaultValue,
  );

  useEffect(() => {
    const hospital = JSON.parse(localStorage.getItem('hospital') || '{}') || hospitalDataContextDefaultValue;
    setHospitalData(hospital);

    // getHospitalData().then((res) => {
    //   setHospitalData(res);
    // });
  }, []);

  return (
    <HospitalDataContext.Provider value={hospitalData}>
      {children}
    </HospitalDataContext.Provider>
  );
};
