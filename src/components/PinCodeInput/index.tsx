import React, { useState } from 'react';
import PinInput from 'react-pin-input';
import { withRouter } from 'react-router-dom';
import styles from './PinCodeInput.module.scss';
import { alertService } from '../../_services/alert.service';
import { accountService } from '../../_services/account.service';

const PinCodeInput: React.FC<{ location: any; history: any }> = ({
  location,
  history,
}) => {
  const [pinCodeValidated, setPinCodeValidated] = useState(false);

  const handleSubmit = (value: string) => {
    alertService.clear();
    accountService
      .verifyOTP(value)
      .then(() => {
        const { from } = location.state || { from: { pathname: '/' } };
        history.push(from);
      })
      .catch((error) => {
        alertService.error(error);
      });
    setPinCodeValidated(true);
  };

  return (
    <div className={pinCodeValidated ? styles.pin_input__container : undefined}>
      <PinInput
        length={6}
        focus
        type="numeric"
        inputMode="number"
        onComplete={handleSubmit}
      />
    </div>
  );
};

export default withRouter(PinCodeInput);
