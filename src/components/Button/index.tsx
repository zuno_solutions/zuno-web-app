import React from 'react';
import { Button as BootstrapButton } from 'react-bootstrap';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { buttonVariants } from '../../utils/variants';

import styles from './Button.module.scss';

interface ButtonProps {
  onClick: () => void;
  children: JSX.Element | JSX.Element[] | string;
  size: 'sm' | 'lg' | undefined; // button: size
  href?: string; // used if button is a link
  type?: 'default' | 'icon'; // default: normal button, icon: only shows icon
  variant?: typeof buttonVariants[number]; // button: variant
  iconName?: any; // Name of icon (ex. 'arrow' from 'fas fa-arrow')
  iconPosition?: 'left' | 'right'; // Postition of Icon
  iconStyle?: string;
  className?: string;
  externalLink?: boolean;
  forwardedRef?: any; // forward ref if required
  // Accepts all basic react button props (onClick, etc)
  // Accepts all Bootstrap Button props
}

const defaultProps = {
  href: null,
  type: 'default',
  variant: 'primary',
  iconName: null,
  iconPosition: 'left',
  iconStyle: null,
  className: null,
  externalLink: false,
  forwardedRef: null,
};

const Button = ({
  children,
  href,
  type = 'default',
  variant = 'primary',
  size,
  iconName,
  iconPosition = 'left',
  // iconStyle,
  className,
  forwardedRef,
  // externalLink,
  ...rest
}: ButtonProps) => {
  const buttonClass = classNames(styles[variant], className, {
    [styles.icon]: type === 'icon',
  });

  return (
    <BootstrapButton
      as={href ? 'a' : undefined}
      variant={variant}
      size={size}
      ref={forwardedRef}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...rest}
      className={buttonClass}
    >
      {iconName && iconPosition === 'left' && (
        <FontAwesomeIcon icon={iconName} />
      )}
      {children}
      {iconName && iconPosition === 'right' && (
        <FontAwesomeIcon icon={iconName} />
      )}
    </BootstrapButton>
  );
};

Button.defaultProps = defaultProps;
export default Button;
