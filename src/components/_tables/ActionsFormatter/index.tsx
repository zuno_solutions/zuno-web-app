/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faEye, faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import styles from './ActionsFormatter.module.scss';
import { DeletePersonnelModal } from '../../_modals/DeletePersonnelModal';
import { personnelService } from '../../../_services/personnel.service';
import { ViewPersonnelModal } from '../../ViewPersonnelModal';
import { EditPersonnelModal } from '../../EditPersonnelModal';

interface ActionsFormatterProps {
  row: any;
  currentPage: number;
  setPersonnel: (data: any) => void;
  edit: boolean;
  api?: string;
}

const defaultProps = {
  api: null,
};

const ActionsFormatter = ({
  row,
  currentPage,
  setPersonnel,
  edit,
  api,
}: ActionsFormatterProps) => {
  const [showDeletePersonnelModal, setShowDeletePersonnelModal] = useState(false);
  const onCloseDeletePersonnelModal = () => setShowDeletePersonnelModal(false);
  const onOpenDeletePersonnelModal = () => {
    setShowDeletePersonnelModal(true);
  };

  const [showViewPersonnelModal, setShowViewPersonnelModal] = useState(false);
  const onCloseViewPersonnelModal = () => setShowViewPersonnelModal(false);
  const onOpenViewPersonnelModal = () => {
    setShowViewPersonnelModal(true);
  };

  const [showEditPersonnelModal, setShowEditPersonnelModal] = useState(false);
  const onCloseEditPersonnelModal = () => setShowEditPersonnelModal(false);
  const onOpenEditPersonnelModal = () => {
    setShowEditPersonnelModal(true);
  };

  const deletePersonnel = async () => {
    personnelService.deletePersonnel(row.id).then(() => {
      personnelService.getRawPersonnel({ offset: currentPage }).then((data) => {
        setPersonnel(data.data);
        onCloseDeletePersonnelModal();
      });
    });
  };

  const updatePersonnel = async () => {
    personnelService.getRawPersonnel({ offset: currentPage }).then((data) => {
      setPersonnel(data.data);
    });
  };

  const deleteImportedPersonnel = async () => personnelService.deleteImportedPersonnel(row.id).then((response) => {
    personnelService.getPersonnel({ offset: currentPage }).then((data) => {
      setPersonnel(data.data);
    });
    onCloseDeletePersonnelModal();
  });

  return (
    <>
      {edit && (
        <EditPersonnelModal
          row={row}
          show={showEditPersonnelModal}
          onHide={onCloseEditPersonnelModal}
          updatePersonnel={updatePersonnel}
        />
      )}
      {!edit && (
        <ViewPersonnelModal
          row={row}
          show={showViewPersonnelModal}
          onHide={onCloseViewPersonnelModal}
        />
      )}
      <DeletePersonnelModal
        onHide={onCloseDeletePersonnelModal}
        onSave={api ? deleteImportedPersonnel : deletePersonnel}
        show={showDeletePersonnelModal}
      />
      {edit && (
        <FontAwesomeIcon
          size="lg"
          className={styles.icon}
          icon={faEdit}
          onClick={onOpenEditPersonnelModal}
        />
      )}
      {!edit && (
        <FontAwesomeIcon
          size="lg"
          className={styles.icon}
          icon={faEye}
          onClick={onOpenViewPersonnelModal}
        />
      )}
      <FontAwesomeIcon
        size="lg"
        className={styles.icon}
        icon={faTrashAlt}
        onClick={onOpenDeletePersonnelModal}
      />
    </>
  );
};

ActionsFormatter.defaultProps = defaultProps;
export default ActionsFormatter;
