import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import styles from './PaymentInfoPage.module.scss';
import 'react-intl-tel-input/dist/main.css';
import paymentIcon from '../../assets/images/payment_icon.png';
import { Pricing } from '../Pricing';
import { invitesService } from '../../_services/invites.service';
import InviteForm from '../_forms/InviteForm';
import { InviteValues } from '../../types/forms';
import { OnboardingTitle } from '../OnboardingTitle/OnboardingTitle';

interface PaymentInfoPageProps {
  setOnboardingState: () => void;
}

export const PaymentInfoPage = ({
  setOnboardingState,
}: PaymentInfoPageProps) => {
  const handleFormSubmit = (values: InviteValues): Promise<any> => invitesService.inviteAccountant(values);

  return (
    <Container>
      <Row className="justify-content-center">
        <Col md={9} lg={5} className="my-2">
          <Row>
            <OnboardingTitle image={paymentIcon} title="Payment" />
          </Row>
          <Pricing />
          <p className={`${styles.desition_flow} mt-3`}>
            Billing Office Contact
          </p>
        </Col>
      </Row>
      <InviteForm
        mainPhone
        check
        submitButtonText="Send to Billing contact"
        setOnboardingState={setOnboardingState}
        handleFormSubmit={handleFormSubmit}
      />
    </Container>
  );
};
