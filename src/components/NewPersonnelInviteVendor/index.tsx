/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import SelectFormikField from '../_forms/SelectFormikField';
import { positionNewPersonnelInvite } from '../../types/positionNewPersonnelInvite';
import validationSchema from './validationSchema';
import { InviteValues } from '../_forms/InviteForm';
import styles from './NewPersonnelInviteVendor.module.scss';
import InputFormikField from '../_forms/InputFormikField';
import { TelephoneInputIntl } from '../_forms/TelephoneInputIntl';
import SelectFormikFieldOptions from '../_forms/SelectFormikFieldOptions';
import {
  IManufacturer,
  ISurgeon,
  personnelService,
} from '../../_services/personnel.service';
import EditRelations from '../EditRelations';
import { manufacturerService } from '../../_services/manufacturer.service';
import { alertService } from '../../_services/alert.service';

interface NewPersonnelInviteVendorValues {
  position: string;
  firstName: string;
  lastName: string;
  email: string;
  mainPhoneNumber: string;
  companyName: string;
  companyAddress: string;
  companyMainPhoneNumber: string;
}

interface NewPersonnelInviteVendorProps {
  handleChange: (personnelPosition: string) => void;
  handleSuccededSubmit: () => void;
  succededSubmit: boolean;
}

export const NewPersonnelInviteVendor = ({
  handleChange, handleSuccededSubmit, succededSubmit,
}: NewPersonnelInviteVendorProps) => {
  const [manufacturers, setManufacturers] = useState<IManufacturer[]>([]);
  const [surgeons, setSurgeons] = useState<ISurgeon[]>([]);

  const handleManufacturerSelection = (manufacturer: any) => {
    const newState = [...manufacturers]; // copying the old datas array
    // replace e.target.value with whatever you want to change it to
    const index = newState.findIndex(
      (manufacturerObj) => manufacturerObj.id === manufacturer.id,
    );
    if (index !== -1) {
      return;
    }
    newState.push(manufacturer);

    setManufacturers(newState);
  };

  const deselectManufacturer = (manufacturer: any) => {
    const newState = [...manufacturers]; // copying the old datas array
    // replace e.target.value with whatever you want to change it to
    const index = newState.findIndex(
      (manufacturerObj) => manufacturerObj.id === manufacturer.id,
    );
    if (index === -1) {
      return;
    }
    newState.splice(index, 1);
    setManufacturers(newState);
  };

  const handleSurgeonSelection = (surgeon: any) => {
    const newState = [...surgeons]; // copying the old datas array
    // replace e.target.value with whatever you want to change it to
    const index = newState.findIndex(
      (surgeonObj) => surgeonObj.id === surgeon.id,
    );
    if (index !== -1) {
      return;
    }
    newState.push(surgeon);

    setSurgeons(newState);
  };

  const deselectSurgeon = (surgeon: any) => {
    const newState = [...surgeons]; // copying the old datas array
    // replace e.target.value with whatever you want to change it to
    const index = newState.findIndex(
      (surgeonObj) => surgeonObj.id === surgeon.id,
    );
    if (index === -1) {
      return;
    }
    newState.splice(index, 1);
    setSurgeons(newState);
  };

  const initialValues = {
    position: '5',
    firstName: '',
    lastName: '',
    email: '',
    mainPhoneNumber: '',
    companyName: '',
    companyAddress: '',
    companyMainPhoneNumber: '',
  };
  const onSubmit = (
    values: NewPersonnelInviteVendorValues,
    { setStatus, setSubmitting }: FormikHelpers<NewPersonnelInviteVendorValues>,
  ) => {
    setStatus();
    setSubmitting(true);
    const manufacturersIds = manufacturers.map(
      (manufacturerObj) => manufacturerObj.id,
    );
    const surgeonsIds = surgeons.map((surgeonObj) => surgeonObj.id);
    personnelService.addPersonnel(values, manufacturersIds, surgeonsIds)
      .then(() => {
        alertService.success('Your invite has been  sent', {
          keepAfterRouteChange: true,
        });
        handleSuccededSubmit();
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  const getManufacturerSuggestions = async (value: string): Promise<any[]> => manufacturerService.getSuggestedManufacturers({
    valueSearched: value,
  }).then((response) => response.data);

  const getSurgeonSuggestions = async (value: string): Promise<any[]> => manufacturerService.getSuggestedSurgeons({
    valueSearched: value,
  }).then((response) => response.data);

  return (
    <>
      {succededSubmit
          && (
          <>
            <h1>Success!</h1>
            <div>
              Your invite has been sent
            </div>
          </>
          )}
      {!succededSubmit && (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({
          errors, touched, isSubmitting, setFieldValue,
        }) => (
          <Form>
            <Row className="mt-5">
              <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
                <Row>
                  <Col>
                    <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                      Personnel info
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col className="px-1 pb-5">
                    <div className={styles.input__container}>
                      <SelectFormikFieldOptions
                        name="position"
                        label="Position"
                        type="text"
                        options={positionNewPersonnelInvite}
                        handleChange={handleChange}
                        setFieldValue={setFieldValue}
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="First Name"
                        name="firstName"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Last Name"
                        name="lastName"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Email"
                        name="email"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <label
                        htmlFor="mainPhone"
                        className="label_intl_phone_element"
                      >
                        Main Phone Number
                      </label>
                      <TelephoneInputIntl
                        name="mainPhoneNumber"
                        id="mainPhoneNumber"
                        setFieldValue={setFieldValue}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
                <Row>
                  <Col>
                    <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                      Company / Distributor
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col className="px-1 pb-5">
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Name"
                        name="companyName"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Address"
                        name="companyAddress"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <label
                        htmlFor="mainPhone"
                        className="label_intl_phone_element"
                      >
                        Main Phone Number
                      </label>
                      <TelephoneInputIntl
                        setFieldValue={setFieldValue}
                        disabled={isSubmitting}
                        name="companyMainPhoneNumber"
                        id="companyMainPhoneNumber"
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col sm={6}>
                <EditRelations
                  deselectRelation={deselectManufacturer}
                  relationsTitle="manufacturer"
                  relations={manufacturers}
                  handleRelationSelection={handleManufacturerSelection}
                  getRelationSuggestions={getManufacturerSuggestions}
                />
              </Col>
              <Col sm={6}>
                <EditRelations
                  deselectRelation={deselectSurgeon}
                  relationsTitle="surgeon"
                  relations={surgeons}
                  handleRelationSelection={handleSurgeonSelection}
                  getRelationSuggestions={getSurgeonSuggestions}
                />
              </Col>
            </Row>
            <Row className="d-flex justify-content-center mt-3">
              <Col xs={9} className="d-flex justify-content-around">
                <Button
                  className={styles.button}
                  type="submit"
                // disabled={isSubmitting}
                  variant="info"
                  size="lg"
                >
                  Invite
                </Button>
                <Button
                  className={styles.button}
                  type="submit"
                  variant="outline-info"
                  size="lg"
                >
                  Save and Invite Later
                </Button>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
      )}
    </>
  );
};
