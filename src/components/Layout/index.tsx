import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import {
  useLocation,
} from 'react-router-dom';
import { Footer } from '../Footer';
import { OnBoardingHeader } from '../Header';

import styles from './layout.module.scss';

type Props = {
  children: JSX.Element | JSX.Element[];
  // eslint-disable-next-line react/require-default-props
  step?: number;
  // eslint-disable-next-line react/require-default-props
  totalSteps?: number;
};

export const Layout: React.FC<Props> = ({ children, step, totalSteps = 5 }) => {
  const { pathname } = useLocation();
  const sideImageRoutes = ['/accept-invite', '/verify-email', '/reset-password', '/set-password'];

  const noFooter = ['/dashboard'];

  const fullScreenRoutes = ['/account/two-factor-authentication', '/account/check-email'];
  const showSideImage = (pathname.startsWith('/account') || sideImageRoutes.indexOf(pathname) !== -1) && fullScreenRoutes.indexOf(pathname) === -1;
  const showFooter = (noFooter.indexOf(pathname) === -1);

  return (
    <Container fluid>
      <Row className={`${styles.layout__container}`}>
        <Col className="d-flex justify-content-between flex-column px-0">
          <OnBoardingHeader />
          {children}
          {
showFooter
          && <Footer step={step} totalSteps={totalSteps} />
          }
        </Col>
        {showSideImage && <Col className={styles.img__container} />}
      </Row>
    </Container>
  );
};
