import React from 'react';
import { Modal } from '../_modals/Modal';
import styles from './ViewPersonnelModal.module.scss';
import medicalStaff from '../../assets/images/medical_staff_icon.png';
import { ViewSurgeon } from '../ViewSurgeon';
import { ViewVendor } from '../ViewVendor';

interface ViewPersonnelModalProps {
  onHide: () => void;
  show: boolean;
  row: any
}

export const ViewPersonnelModal = ({
  onHide,
  show,
  row,
}: ViewPersonnelModalProps) => {
  const getHeader = (image: string | undefined) => (
    <>
      <div className={`mb-4 ${styles.modal_image_header_padding}`}>
        <img src={image} alt="log out logo" />
      </div>
      <div>
        <h3>View  Personnel</h3>
        <div className="d-flex justify-content-center">
          <h5 className={`${styles.subtitle_title}`}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi posuere eu magna.
          </h5>
        </div>
      </div>
    </>
  );

  return (
    <>
      <Modal
        show={show}
        onHide={onHide}
        title={getHeader(medicalStaff)}
        dialogClassName={styles.modal_width}
        closeButtonText="< Back"
      >
        <>
          {row.role === 4
          && <ViewSurgeon row={row} />}
          {row.role === 5
          && <ViewVendor row={row} />}
          {row.role === 6
          && <ViewSurgeon row={row} />}
        </>
      </Modal>
    </>
  );
};
