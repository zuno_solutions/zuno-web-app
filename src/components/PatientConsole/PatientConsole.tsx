/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import moment from 'moment';
import React from 'react';
import { Col } from 'react-bootstrap';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';
import styles from './PatientConsole.module.scss';

interface PatientConsoleProps {
  item: ISurgeryItem,
}

export const PatientConsole = ({
  item,
}: PatientConsoleProps) => (
  <Col className={`d-flex justify-content-between flex-column pt-4 pb-5 ${styles.container}`}>
    <h5>Patient</h5>
    <h2>{item.patient.name}</h2>
    <p>
      Age:
      {' '}
      {item.patient.age}
    </p>
    <p>
      Gender:
      {' '}
      {item.patient.gender}
    </p>
    <h5>Emergency Contact</h5>
    <h4>{item.patient.emergencyContact}</h4>
    <p>
      Relationship with the patient: Son
    </p>
  </Col>
);
