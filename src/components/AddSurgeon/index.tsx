/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import validationSchema from './validationSchema';
import styles from './AddSurgeon.module.scss';
import InputFormikField from '../_forms/InputFormikField';
import { TelephoneInputIntl } from '../_forms/TelephoneInputIntl';
import { personnelService } from '../../_services/personnel.service';

interface AddSurgeonValues {
  position: string;
  firstName: string;
  lastName: string;
  email: string;
  mainPhoneNumber: string;
}

interface AddSurgeonProps {
  closeForm: () => void;
  handleRelationSelection: (variable: any) => void;
}

export const AddSurgeon = ({
  closeForm,
  handleRelationSelection,
}: AddSurgeonProps) => {
  const initialValues = {
    position: '6',
    firstName: '',
    lastName: '',
    email: '',
    mainPhoneNumber: '',
  };
  const onSubmit = (
    values: AddSurgeonValues,
  ) => {
    personnelService.addPersonnel(values)
      .then((response) => personnelService.getRawPersonnelById(response.data.rawPersonnelId)).then((data: {personnel: any}) => {
        handleRelationSelection(data.rawPersonnel);
        closeForm();
      })
      .catch((error) => {
      });
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        // validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({
          errors, touched, isSubmitting, setFieldValue, values,
        }) => (
          <Form>
            <Row className="">
              <Col
                className={`${styles.column_container} bg-white mx-3 rounded`}
              >
                <Row>
                  <Col>
                    <p className={`mb-2 ${styles.payment__subtitle}`}>
                      Personnel info
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col className="px-1 pb-3">
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="First Name"
                        name="firstName"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Last Name"
                        name="lastName"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Email"
                        name="email"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <label
                        htmlFor="mainPhone"
                        className="label_intl_phone_element"
                      >
                        Main Phone Number
                      </label>
                      <TelephoneInputIntl
                        name="mainPhoneNumber"
                        id="mainPhoneNumber"
                        setFieldValue={setFieldValue}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="d-flex justify-content-center">
              <Col xs={9} className="d-flex justify-content-around">
                <Button
                  disabled={isSubmitting}
                  variant="info"
                  onClick={() => onSubmit(values)}
                >
                  Invite
                </Button>
                <Button
                  disabled={isSubmitting}
                  variant="outline-info"
                  onClick={closeForm}
                >
                  Cancel
                </Button>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </>
  );
};
