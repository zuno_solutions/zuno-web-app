/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AutoSuggest from 'react-autosuggest';
import { faPlusCircle, faSearch } from '@fortawesome/free-solid-svg-icons';
import theme from './theme.module.scss';
import styles from './RelationsSuggestionBar.module.scss';
import { manufacturerService } from '../../_services/manufacturer.service';
import { IManufacturer, personnelService } from '../../_services/personnel.service';
import { NewPersonnelInviteSurgeon } from '../NewPersonnelInviteSurgeon';
import { AddSurgeon } from '../AddSurgeon';

interface RelationsSuggestionBarProps {
  handleRelationSelection: (manufacturer: IManufacturer) => void;
  getRelationSuggestions: (value: string) => Promise<any[]>;
  role: string;
}

export const RelationsSuggestionBar = ({
  handleRelationSelection,
  getRelationSuggestions,
  role,
}: RelationsSuggestionBarProps) => {
  const [value, setValue] = useState('');
  const [showSurgeonForm, setShowSurgeonForm] = useState(false);
  const [suggestions, setSuggestions] = useState<string[]>([]);

  const getSuggestions = async ({ value }: { value: string }) => {
    setValue(value);
    const suggestions = await getRelationSuggestions(value);
    setSuggestions(suggestions);
  };

  const onSuggestionSelected = (
    event: any,
    { suggestion }: { suggestion: any },
  ) => {
    handleRelationSelection(suggestion);
    setValue('');
  };

  const addRelation = () => {
    if (role === 'manufacturer') {
      personnelService.addManufacturer(value).then((response) => {
        handleRelationSelection(response.data);
        setValue('');
      });
    } else {
      setShowSurgeonForm(true);
      setValue('');
    }
  };

  const closeForm = () => {
    setShowSurgeonForm(false);
  };

  return (
    <>
      <div className={`${styles.searchBarContainer} my-4`}>
        <AutoSuggest
          theme={theme}
          suggestions={suggestions}
          onSuggestionsClearRequested={() => setSuggestions([])}
          onSuggestionsFetchRequested={getSuggestions}
          onSuggestionSelected={onSuggestionSelected}
          getSuggestionValue={(suggestion: any) => suggestion.manufacturer || suggestion.first_name}
          renderSuggestion={(suggestion: any) => (
            <span>
              {suggestion.manufacturer
                || `${suggestion.first_name} ${suggestion.last_name}`}
            </span>
          )}
          inputProps={{
            placeholder: 'Search',
            value,
            onChange: (_, { newValue, method }) => {
              setValue(newValue);
            },
          }}
          highlightFirstSuggestion
          multiSection={false}
        />
        <div className={styles.actions_container}>
          <button
            type="button"
            // onClick={searchIconClicked}
            className={styles.searchIcon}
          >
            <FontAwesomeIcon icon={faSearch} size="lg" />
          </button>
        </div>
        {suggestions.length === 0 && value.length > 0 && (
          <div className={styles.actions_container}>
            <button
              type="button"
              onClick={addRelation}
              className={styles.addIcon}
            >
              <FontAwesomeIcon icon={faPlusCircle} size="lg" />
            </button>
          </div>
        )}
      </div>
      <div>
        {suggestions.length === 0 && value.length > 0 && !showSurgeonForm && (
          <span className={styles.notFound}>
            No
            {' '}
            {role}
            s found, Save new
            {' '}
            {role}
            ?
          </span>
        )}
      </div>
      {showSurgeonForm && (
      <div>
        <AddSurgeon closeForm={closeForm} handleRelationSelection={handleRelationSelection} />
      </div>
      )}
    </>
  );
};
