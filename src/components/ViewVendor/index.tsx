/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import SelectFormikField from '../_forms/SelectFormikField';
import { positionNewPersonnelInvite } from '../../types/positionNewPersonnelInvite';
import validationSchema from './validationSchema';
import styles from './ViewVendor.module.scss';
import { TelephoneInputIntl } from '../_forms/TelephoneInputIntl';
import SelectFormikFieldOptions from '../_forms/SelectFormikFieldOptions';
import {
  IManufacturer,
  ISurgeon,
  personnelService,
} from '../../_services/personnel.service';
import DisabledInputFormikField from '../_forms/DisabledInputFormikField';
import EditRelations from '../EditRelations';

interface ViewVendorValues {
  position: string;
  firstName: string;
  lastName: string;
  email: string;
  mainPhoneNumber: string;
  companyName: string;
  companyAddress: string;
  companyMainPhoneNumber: string;
}

interface ViewVendorProps {
  row: any;
}

export const ViewVendor = ({ row }: ViewVendorProps) => {
  const [position, setPosition] = useState<string>('5');
  const [firstName, setFirstName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [mainPhoneNumber, setMainPhoneNumber] = useState<string>('');
  const [companyName, setCompanyName] = useState<string>('');
  const [companyAddress, setCompanyAddress] = useState<string>('');
  const [companyMainPhoneNumber, setCompanyMainPhoneNumber] = useState<string>('');
  const [manufacturers, setManufacturers] = useState<IManufacturer[]>([]);
  const [surgeons, setSurgeons] = useState<ISurgeon[]>([]);

  const initialValues = {
    position,
    firstName,
    lastName,
    email,
    mainPhoneNumber,
    companyName,
    companyAddress,
    companyMainPhoneNumber,
  };
  const onSubmit = (
    values: ViewVendorValues,
    { setStatus, setSubmitting }: FormikHelpers<ViewVendorValues>,
  ) => {
    console.log(values);
  };

  useEffect(() => {
    const getPersonnelData = async () => personnelService.getRawPersonnelById(row.id);
    getPersonnelData().then((data) => {
      setFirstName(data.rawPersonnel.first_name);
      setLastName(data.rawPersonnel.last_name);
      setEmail(data.rawPersonnel.email);
      setMainPhoneNumber(data.rawPersonnel.phone);
      // setManufacturers(data.manufacturers);
      // setSurgeons(data.surgeons);
      // setCompanyName(data)
      // setCompanyAddress(data)
      // setCompanyMainPhoneNumber(data)
    });
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {({
        errors, touched, isSubmitting, setFieldValue,
      }) => (
        <Form>
          <Row className="mt-2">
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col>
                  <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    Personnel info
                  </p>
                </Col>
              </Row>
              <Row>
                <Col className="px-1 pb-5">
                  <div className={styles.input__container}>
                    <SelectFormikFieldOptions
                      name="position"
                      label="Position"
                      type="text"
                      options={positionNewPersonnelInvite}
                      // handleChange={handleChange}
                      setFieldValue={setFieldValue}
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="First Name"
                      name="firstName"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Last Name"
                      name="lastName"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Email"
                      name="email"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <label
                      htmlFor="mainPhone"
                      className="label_intl_phone_element"
                    >
                      Main Phone Number
                    </label>
                    <TelephoneInputIntl
                      name="mainPhoneNumber"
                      id="mainPhoneNumber"
                      setFieldValue={setFieldValue}
                      defaultValue={mainPhoneNumber}
                      disabled
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col>
                  <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    Company / Distributor
                  </p>
                </Col>
              </Row>
              <Row>
                <Col className="px-1 pb-5">
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Name"
                      name="companyName"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Address"
                      name="companyAddress"
                      type="text"
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <label
                      htmlFor="mainPhone"
                      className="label_intl_phone_element"
                    >
                      Main Phone Number
                    </label>
                    <TelephoneInputIntl
                      setFieldValue={setFieldValue}
                      name="companyMainPhoneNumber"
                      id="companyMainPhoneNumber"
                      disabled
                    />
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <EditRelations
                relationsTitle="manufacturer"
                relations={manufacturers}
              />
            </Col>
            <Col sm={6}>
              <EditRelations relationsTitle="surgeon" relations={surgeons} />
            </Col>
          </Row>
        </Form>
      )}
    </Formik>
  );
};
