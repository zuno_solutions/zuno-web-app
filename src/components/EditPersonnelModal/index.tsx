import React from 'react';
import { Modal } from '../_modals/Modal';
import styles from './EditPersonnelModal.module.scss';
import medicalStaff from '../../assets/images/medical_staff_icon.png';
import { roleText } from '../../_helpers/role';
import { EditSurgeon } from '../EditSurgeon';
import { EditVendor } from '../EditVendor';
import { EditAnesthesiologist } from '../EditAnesthesiologist';

interface EditPersonnelModalProps {
  onHide: () => void;
  updatePersonnel: () => void;
  show: boolean;
  row: {role: keyof typeof roleText};
}

export const EditPersonnelModal = ({
  onHide,
  show,
  row,
  updatePersonnel,
}: EditPersonnelModalProps) => {
  const roleTitle = roleText[row.role];

  const getHeader = (image: string | undefined) => (
    <>
      <div className={`mb-4 ${styles.modal_image_header_padding}`}>
        <img src={image} alt="log out logo" />
      </div>
      <div>
        <h2>
          Edit
          {' '}
          {roleTitle}
        </h2>
      </div>
    </>
  );

  return (
    <>
      <Modal
        show={show}
        onHide={onHide}
        title={getHeader(medicalStaff)}
        dialogClassName={styles.modal_width}
      >
        <>
          {row.role === 7
          && <EditAnesthesiologist row={row} onHide={onHide} updatePersonnel={updatePersonnel} />}
          {row.role === 5
          && <EditVendor row={row} onHide={onHide} updatePersonnel={updatePersonnel} />}
          {row.role === 6
          && <EditSurgeon row={row} onHide={onHide} updatePersonnel={updatePersonnel} />}
        </>
      </Modal>
    </>
  );
};
