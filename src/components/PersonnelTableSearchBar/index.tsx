/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
// @ts-ignore
import SearchField from 'react-search-field';
import styles from './PersonnelTableSearchBar.module.scss';

interface PersonnelTableSearchBarProps {
  handleTableSearch: (value: string) => void;
}

export const PersonnelTableSearchBar = ({ handleTableSearch }:PersonnelTableSearchBarProps) => (
  <SearchField
    classNames={styles.search_bar}
    placeholder="Search..."
    onChange={handleTableSearch}
    searchText=""
  />
);
