/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';
import styles from './TodayDateRange.module.scss';
import clockIcon from '../../assets/images/clock_badge_icon.png';

interface TodayDateRangeProps {
  surgeries: ISurgeryItem[];
}

export const TodayDateRange = ({ surgeries }: TodayDateRangeProps) => {
  const dates = surgeries.map((surgery) => surgery.start_time);
  const orderedSurgeries = dates.sort((a, b) => a - b);

  return (
    <>
      <Row className={`${styles.item} p-2`}>
        <Col className="d-flex justify-content-between">
          <img
            src={clockIcon}
            alt="clock icon"
          />
          <div>
            <p className={`${styles.title}`}>
              First Surgery Start
            </p>
            <p className={`${styles.time}`}>
              {moment(orderedSurgeries[0]).format('LT')}
            </p>
          </div>
          <div>
            <p className={`${styles.title}`}>
              Last Surgery Ends
            </p>
            <p className={`${styles.time}`}>
              {moment(orderedSurgeries[orderedSurgeries.length - 1]).format('LT')}
            </p>
          </div>
        </Col>
      </Row>

    </>
  );
};
