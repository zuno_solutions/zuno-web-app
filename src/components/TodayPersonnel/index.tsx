/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';
import styles from './TodayPersonnel.module.scss';
import scalpelIcon from '../../assets/images/scalpel_icon.png';
import syringeIcon from '../../assets/images/syringe_blue_icon.png';
import vendorsIcon from '../../assets/images/vendors_icon.png';

interface TodayPersonnelProps {
  surgeries: ISurgeryItem[];
  handlePersonnelSelection: (personnel: string) => void
  selectedPersonnel: string | undefined
}

export const TodayPersonnel = ({ surgeries, selectedPersonnel, handlePersonnelSelection }: TodayPersonnelProps) => {
  const personnel = [
    { personnel: 'Surgeon', count: 6, icon: scalpelIcon }, { personnel: 'Anesthesiologist', count: 9, icon: syringeIcon },
    { personnel: 'Vendor', count: 12, icon: vendorsIcon }];
  return (
    <>
      <div>
        <p className={`${styles.subtitle} p-2`}>
          Personnel
        </p>
      </div>
      {personnel.map((person) => {
        console.log(selectedPersonnel, person.personnel);
        const backgroundColor = selectedPersonnel === person.personnel ? '#15C0ED' : 'transparent';

        return (
          <button
            key={person.personnel}
            // style={{ backgroundColor, color }}
            type="button"
            className={`px-3 ${styles.item} ${selectedPersonnel === person.personnel ? styles.selected : styles.deselected}`}
            onClick={() => handlePersonnelSelection(person.personnel)}
          >
            <Row>
              <Col sm={2}>
                <img
                  src={person.icon}
                  alt="scalpel icon"
                />
              </Col>
              <Col>
                <p className={`${styles.title}`}>
                  {person.personnel}
                </p>
              </Col>
              <Col sm={2}>
                <p className={`${styles.number}`}>
                  {person.count}
                </p>
              </Col>
            </Row>
          </button>
        );
      })}
    </>
  );
};
