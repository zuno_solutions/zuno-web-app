/* eslint-disable react/require-default-props */
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import styles from './OnboardingTitle.module.scss';

interface OnboardingTitleProps {
  image: string;
  title: string;
  assistiveText?: string;
}

export const OnboardingTitle = ({
  image,
  assistiveText,
  title,
}: OnboardingTitleProps) => (
  <Row className="justify-content-center mb-4">
    <Col className="col-md-5 ">
      <div className={styles.hospital_info_img__container}>
        <img src={image} alt={`${title} icon`} />
      </div>
      <h1 className={styles.basic__title}>{title}</h1>
      {assistiveText && <p>{assistiveText}</p>}
    </Col>
  </Row>
);
