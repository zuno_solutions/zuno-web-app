/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import Timeline, {
  CustomHeader,
  CustomMarker,
  DateHeader,
  SidebarHeader,
  TimelineHeaders,
  TodayMarker,
} from 'react-calendar-timeline';
import 'react-calendar-timeline/lib/Timeline.css';
import moment from 'moment';
import style from './SwimmingLanes.module.scss';
import { SwimmingLanesSurgeryItem } from '../SwimmingLanesSurgeryItem/SwimmingLanesSurgeryItem';
import { SwimmingLanesSurgeryLane } from '../SwimmingLanesSurgeryLane/SwimmingLanesSurgeryLane';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';

interface SwimmingLanesProps {
  operationRooms: IOperationRoom[],
  surgeries: ISurgeryItem[],
  setSurgeries: (value: ISurgeryItem[]) => void
  selectedRoom: number | string | undefined;
  selectedPersonnel: string | undefined;
  handleItemSelection: (item: any) => void;
}

const keys = {
  groupIdKey: 'id',
  groupTitleKey: 'title',
  groupRightTitleKey: 'rightTitle',
  itemIdKey: 'id',
  itemTitleKey: 'title',
  itemDivTitleKey: 'title',
  itemGroupKey: 'group',
  itemTimeStartKey: 'start_time',
  itemTimeEndKey: 'end_time',
  groupLabelKey: 'title',
};

export const SwimmingLanes = ({
  operationRooms, surgeries, setSurgeries, selectedRoom, selectedPersonnel, handleItemSelection,
}: SwimmingLanesProps) => {
  const [defaultTimeStart, setDefaultTimeStart] = useState(
    moment().startOf('day').toDate(),
  );
  const [defaultTimeEnd, setDefaultTimeEnd] = useState(
    moment().startOf('day').add(1, 'day').toDate(),
  );

  const itemRenderer = ({
    item,
    itemContext,
    getItemProps,
    getResizeProps,
  }: {
    item: any;
    itemContext: any;
    getItemProps: any;
    getResizeProps: any;
  }) => (
    <SwimmingLanesSurgeryItem
      item={item}
      itemContext={itemContext}
      getItemProps={getItemProps}
      getResizeProps={getResizeProps}
      selectedRoom={selectedRoom}
      selectedPersonnel={selectedPersonnel}
      handleItemSelection={handleItemSelection}
    />
  );

  const groupRenderer = ({ group }: { group: any }) => (
    <SwimmingLanesSurgeryLane group={group} selectedRoom={selectedRoom} />
  );

  const handleItemMove = (
    itemId: string,
    dragTime: any,
    newGroupOrder: any,
  ) => {
    const group = operationRooms[newGroupOrder];
    const newArr = [...surgeries];
    setSurgeries(
      newArr.map((surgery) => (surgery.id === itemId
        ? {
          ...surgery,
          start_time: dragTime,
          end_time: dragTime + (surgery.end_time - surgery.start_time),
          group: group.id,
        }
        : surgery)),
    );
    console.log('Moved', itemId, dragTime, newGroupOrder);
  };

  const handleItemResize = (itemId: string, time: any, edge: any) => {
    const newArr = [...surgeries];

    setSurgeries(
      newArr.map((surgery) => (surgery.id === itemId
        ? {
          ...surgery,
          start_time: edge === 'left' ? time : surgery.start_time,
          end_time: edge === 'left' ? surgery.end_time : time,
        }
        : surgery)),
    );

    console.log('Resized', itemId, time, edge);
  };

  return (
    <Timeline
      groups={operationRooms}
      items={surgeries}
      defaultTimeStart={moment().add(-7, 'hour')}
      defaultTimeEnd={moment().add(7, 'hour')}
      keys={keys}
      canMove
      canResize="both"
      itemTouchSendsClick={false}
      // stackItems
      itemHeightRatio={1}
      sidebarWidth={22}
      onItemMove={handleItemMove}
      onItemResize={handleItemResize}
      itemRenderer={itemRenderer}
      groupRenderer={groupRenderer}
      maxZoom={129600000}
      minZoom={10800000}
    >
      <TodayMarker>
        {({ styles, date }) => {
        // date is value of current date. Use this to render special styles for the marker
        // or any other custom logic based on date:
        // e.g.
          const customStyles = {
            ...styles,
            backgroundColor: 'White',
            width: '2px',
            zIndex: 800,
          };
          return <div style={customStyles} />;
        }}
      </TodayMarker>
      <TodayMarker>
        {({ styles, date }) => {
        // date is value of current date. Use this to render special styles for the marker
        // or any other custom logic based on date:
        // e.g.
          const customStyles = {
            ...styles,
            backgroundColor: '#34C759',
            width: '25px',
            heigth: '10px',
            zIndex: 900,
            bottom: '99.5%',
            marginLeft: '-10px',
            // marginTop: '-10px',
            fontSize: '8px',
            color: '#FFFFFF',
            borderRadius: '4px',
            textAlign: 'center',
            verticalAlign: 'center',
          };
          return (
            <div style={customStyles}>
              <p>
                Now
              </p>
            </div>
          );
        }}
      </TodayMarker>
      <TimelineHeaders className={style.sticky}>
        <CustomHeader height={50} headerData={{ someData: 'data' }} unit="day">
          {({
            headerContext: { intervals },
            getRootProps,
            getIntervalProps,
            showPeriod,
            data,
          }) => (
            <div {...getRootProps()}>
              {intervals.map((interval) => {
                const intervalStyle = {
                  backgroundColor: 'White',
                  position: 'sticky',
                };
                return (
                  <div
                    className={style.date_container}
                    {...getIntervalProps({
                      interval,
                      style: intervalStyle,
                    })}
                  >
                    <p
                      className={style.sticky_date}
                      style={{
                        position: 'fixed',
                        left: 80,
                        background: 'White',
                        fontSize: '17px',
                        paddingTop: '15px',
                      }}
                    >
                      <b>Hospital Name</b>
                      {' '}
                      {interval.startTime.format('dddd, D MMMM YYYY')}
                    </p>
                  </div>
                );
              })}
            </div>
          )}
        </CustomHeader>
        <CustomHeader
          height={30}
          headerData={{ someData: 'data' }}
          unit="hour"
        >
          {({
            headerContext: { intervals },
            getRootProps,
            getIntervalProps,
            showPeriod,
            data,
          }) => (
            <div {...getRootProps()}>
              {intervals.map((interval) => {
                const intervalStyle = {
                  lineHeight: '30px',
                  cursor: 'pointer',
                  backgroundColor: 'White',
                };
                return (
                  <div
                    className={style.timeline}
                    {...getIntervalProps({
                      interval,
                      style: intervalStyle,
                    })}
                  >
                    <div style={{
                      textAlign: 'center',
                      marginLeft: '-100%',
                      fontSize: '10px',
                    }}
                    >
                      {interval.labelWidth > 45
                        ? `${interval.startTime.format('hA')}`
                        : `${interval.startTime.format('h')}`}
                    </div>
                  </div>
                );
              })}
            </div>
          )}
        </CustomHeader>
        <CustomHeader height={15} headerData={{ someData: 'data' }} unit="hour">
          {({
            headerContext: { intervals },
            getRootProps,
            getIntervalProps,
            showPeriod,
            data,
          }) => (
            <div {...getRootProps()}>
              {intervals.map((interval) => {
                const intervalStyle = {
                  lineHeight: '30px',
                  textAlign: 'center',
                  borderLeft: '1px solid black',
                  cursor: 'pointer',
                  backgroundColor: 'White',
                  opacity: '0.5',
                  color: 'white',
                };
                return (
                  <div
                    className={style.hour}
                    {...getIntervalProps({
                      interval,
                      style: intervalStyle,
                    })}
                  >
                    <div className={style.first_half_hour}>1</div>
                    <div className={style.second_half_hour}>2</div>
                  </div>
                );
              })}
            </div>
          )}
        </CustomHeader>
      </TimelineHeaders>
    </Timeline>
  );
};
