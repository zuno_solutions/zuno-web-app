import React from 'react';
import cx from 'classnames';
import styles from './Stepper.module.scss';

interface StepperProps {
  step?: number;
  totalSteps?: number;
}

const defaultProps = {
  step: 1,
  totalSteps: 5,
};

const Stepper: React.FC<StepperProps> = ({
  step = 1,
  totalSteps = 5,
}) => (
  <div
    className="d-flex align-items-center justify-content-between w-50 align-middle"
  >
    {[...Array(totalSteps).keys()].map((index) => {
      const classNameActive = step === index + 1 ? styles.step_active : null;
      const classNameAhead = step < index + 1 ? styles.step_ahead : null;
      const classNameStep = !(step === index + 1 || step < index + 1) ? styles.step : null;
      return (
        <div
          key={index}
          className={cx(classNameActive, classNameAhead, classNameStep, 'rounded-circle')}
        />
      );
    })}
  </div>
);

Stepper.defaultProps = defaultProps;
export default Stepper;
