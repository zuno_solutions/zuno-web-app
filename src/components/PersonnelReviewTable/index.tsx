/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-plusplus */
import React, { useEffect, useState } from 'react';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
// @ts-ignore
import overlayFactory from 'react-bootstrap-table2-overlay';
import BootstrapTable from 'react-bootstrap-table-next';
// @ts-ignore
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from 'react-bootstrap-table2-paginator';
import { Col, Row } from 'react-bootstrap';
import styles from './PersonnelReviewTable.module.scss';
import {
  IQuery,
  perPage,
  personnelService,
} from '../../_services/personnel.service';
import { PersonnelTableSearchBar } from '../PersonnelTableSearchBar';
import { roleText } from '../../_helpers/role';
import ActionsFormatter from '../_tables/ActionsFormatter';
import { alertService } from '../../_services/alert.service';

export const PersonnelReviewTable = () => {
  const [personnel, setPersonnel] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [total, settotal] = useState(1);
  const [loading, setLoading] = useState(false);

  const rowClasses = (row: any, rowIndex: number) => {
    if (rowIndex % 2 === 0) {
      return 'row-style pair-row-style';
    }
    return 'row-style';
  };

  const options = {
    custom: true,
    page: currentPage,
    sizePerPage: perPage,
    totalSize: total,
    alwaysShowAllBtns: true,
  };
  const getPersonnelData = async (query?: IQuery) => personnelService.getPersonnel({ offset: currentPage, ...query });

  const reloadData = (data: any) => setPersonnel(data);

  useEffect(() => {
    getPersonnelData().then((data) => {
      setPersonnel(data.data);
      settotal(data.pagination.total);
    });
  }, []);

  const actionsFormatter = (cell: any, row: any) => (
    <ActionsFormatter
      edit={false}
      row={row}
      currentPage={currentPage}
      setPersonnel={reloadData}
      api="import"
    />
  );

  const personnelReviewTableColumns = [
    {
      dataField: 'role',
      text: 'Position',
      sort: true,
      formatter: (cell: keyof typeof roleText) => roleText[cell],
      editor: {
        type: Type.SELECT,
        options: [
          {
            value: 5,
            label: 'Vendor',
          },
          {
            value: 6,
            label: 'Surgeon',
          },
          {
            value: 7,
            label: 'Anesthesiologist',
          },
        ],
        style: { width: '90px' },
      },
    },
    {
      dataField: 'last_name',
      text: 'Last Name',
      sort: true,
    },
    {
      dataField: 'first_name',
      text: 'First Name',
      sort: true,
    },
    {
      dataField: 'phone',
      text: 'Phone Number',
      sort: true,
    },
    {
      dataField: 'email',
      text: 'Email',
      sort: true,
    },
    // {
    //   dataField: 'manufacturers',
    //   text: 'Manufacturer',
    //   sort: true,
    //   formatter: (cell: { manufacturer: string }[]) => {
    //     for (let i = 0; i < cell.length; i++) {
    //       return <p>{cell[i].manufacturer}</p>;
    //     }
    //     return '';
    //   },
    // },
    {
      dataField: 'actions',
      text: 'Actions',
      isDummyField: true,
      editable: false,
      csvExport: false,
      formatter: actionsFormatter,
    },
  ];

  const handleTableChange = async (
    type: any,
    {
      page,
      sortField,
      sortOrder,
    }: { page: number; sortField: string; sortOrder: string },
  ) => {
    setLoading(true);
    // eslint-disable-next-line camelcase
    const sort_by = sortOrder === 'asc' ? sortField : `-${sortField}`;
    await personnelService
      .getPersonnel({ offset: page, sort_by })
      .then((data) => {
        setPersonnel(data.data);
        settotal(data.pagination.total);
        setCurrentPage(page);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        alertService.error(error);
      });
  };

  const handleTableSearch = (value: string) => {
    getPersonnelData({ search: value }).then((data) => {
      setPersonnel(data.data);
      settotal(data.pagination.total);
    });
  };

  const afterSaveCell = async (
    oldValue: any,
    newValue: any,
    row: any,
    column: any,
  ) => {
    const { dataField } = column;
    await personnelService.editPersonnel(row, dataField, newValue);
  };

  return (
    <>
      <PaginationProvider pagination={paginationFactory(options)}>
        {({
          paginationProps,
          paginationTableProps,
        }: {
          paginationProps: any;
          paginationTableProps: any;
        }) => (
          <>
            <Row
              className={`d-flex justify-content-end ${styles.padding_zero}`}
            >
              <Col className="my-4 col-md-3">
                <PersonnelTableSearchBar
                  handleTableSearch={handleTableSearch}
                />
              </Col>
            </Row>
            <BootstrapTable
              noDataIndication="Table is Empty"
              bootstrap4
              loading={loading}
              keyField="id"
              data={personnel}
              columns={personnelReviewTableColumns}
              cellEdit={cellEditFactory({
                mode: 'click',
                blurToSave: true,
                afterSaveCell,
              })}
              rowClasses={rowClasses}
              headerClasses="header-table"
              {...paginationTableProps}
              remote={{ pagination: true, filter: true, sort: true }}
              onTableChange={handleTableChange}
              hover
              overlay={overlayFactory({
                spinner: true,
                styles: {
                  overlay: (base: any) => ({
                    ...base,
                    background: 'rgba(242, 242, 247, 1)',
                  }),
                },
              })}
              wrapperClasses="table-responsive"
            />
            <Row
              className={`d-flex justify-content-center ${styles.padding_zero}`}
            >
              <Col className="my-4 col-md-5 d-flex justify-content-center">
                <PaginationListStandalone {...paginationProps} />
              </Col>
            </Row>
          </>
        )}
      </PaginationProvider>
    </>
  );
};
