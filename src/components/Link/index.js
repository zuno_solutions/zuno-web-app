// import React from 'react';
// import get from 'lodash/get';

// // const propTypes = {
// //   children: PropTypes.node,
// //   className: PropTypes.string,
// //   variant: PropTypes.oneOf(linkVariants),
// //   href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
// //   externalLink: PropTypes.bool,
// //   linkContent: PropTypes.node, // Pass content that doesn't get wrapped in 'a'
// //   locale: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
// //   fullReload: PropTypes.bool, // Does full page reload
// //   // Accepts Next Link props
// // };

// export const Link = ({
//   children,
//   href,
//   externalLink = false,
//   handleOnClick,
//   ...rest
// }) => (
//   <a
//     target={externalLink ? '_blank' : undefined}
//     onClick={handleOnClick}
//   >
//     {children}
//   </a>
// );
