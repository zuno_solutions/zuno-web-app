import React, { useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { accountService } from '../../_services/account.service';
import { ChooseEMR } from '../_forms/ChooseEMR';

export const ITData = () => {
  const [emr, setEmr] = useState<string>('');
  const logOutAndConfirm = () => {
    accountService.logout();
  };

  return (
    <>
      <ChooseEMR emr={emr} setEmr={setEmr} />
      <Row className="justify-content-center mt-5">
        <Col className="col-md-5">
          <div className="d-grid gap-2">
            <Button target="_blank" variant="info" size="lg" href={`https://fhir.epic.com/interconnect-fhir-oauth/oauth2/authorize?response_type=code&redirect_uri=${process.env.REACT_APP_EPIC_REDIRECT_URI}&client_id=${process.env.REACT_APP_EPIC_CLIENT_ID}&state=code&scope=fhiruser`}>
              <span className="align-middle">Continue</span>
            </Button>
            <Button
              className="mt-2"
              variant="link"
              size="lg"
              onClick={logOutAndConfirm}
            >
              Save and complete later
            </Button>
          </div>
        </Col>
      </Row>
    </>
  );
};
