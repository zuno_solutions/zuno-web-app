/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import SelectFormikField from '../_forms/SelectFormikField';
import { positionNewPersonnelInvite } from '../../types/positionNewPersonnelInvite';
import validationSchema from './validationSchema';
import { InviteValues } from '../_forms/InviteForm';
import styles from './NewPersonnelInviteAnesthesiologist.module.scss';
import InputFormikField from '../_forms/InputFormikField';
import { TelephoneInputIntl } from '../_forms/TelephoneInputIntl';
import SelectFormikFieldOptions from '../_forms/SelectFormikFieldOptions';
import { personnelService } from '../../_services/personnel.service';
import { alertService } from '../../_services/alert.service';

interface NewPersonnelInviteAnesthesiologistValues {
  position: string;
  firstName: string;
  lastName: string;
  email: string;
  mainPhoneNumber: string;
  companyName: string;
  companyAddress: string;
  companyMainPhoneNumber: string;
}

interface NewPersonnelInviteAnesthesiologistProps {
  handleChange: (personnelPosition: string) => void;
  handleSuccededSubmit: () => void;
  succededSubmit: boolean;
}

export const NewPersonnelInviteAnesthesiologist = ({
  handleChange, handleSuccededSubmit, succededSubmit,
}: NewPersonnelInviteAnesthesiologistProps) => {
  const initialValues = {
    position: '7',
    firstName: '',
    lastName: '',
    email: '',
    mainPhoneNumber: '',
    companyName: '',
    companyAddress: '',
    companyMainPhoneNumber: '',
  };
  const onSubmit = (
    values: NewPersonnelInviteAnesthesiologistValues,
    {
      setStatus,
      setSubmitting,
    }: FormikHelpers<NewPersonnelInviteAnesthesiologistValues>,
  ) => {
    setStatus();
    setSubmitting(true);
    personnelService.addPersonnel(values)
      .then(() => {
        alertService.success('Your invite has been  sent', {
          keepAfterRouteChange: true,
        });
        handleSuccededSubmit();
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  return (
    <>
      {succededSubmit
          && (
          <>
            <h1>Success!</h1>
            <div>
              Your invite has been sent
            </div>
          </>
          )}
      {!succededSubmit && (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({
          errors, touched, isSubmitting, setFieldValue,
        }) => (
          <Form>
            <Row className="mt-5">
              <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
                <Row>
                  <Col>
                    <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                      Personnel info
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col className="px-1 pb-5">
                    <div className={styles.input__container}>
                      <SelectFormikFieldOptions
                        name="position"
                        label="Position"
                        type="text"
                        options={positionNewPersonnelInvite}
                        handleChange={handleChange}
                        setFieldValue={setFieldValue}
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="First Name"
                        name="firstName"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Last Name"
                        name="lastName"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Email"
                        name="email"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <label
                        htmlFor="mainPhone"
                        className="label_intl_phone_element"
                      >
                        Main Phone Number
                      </label>
                      <TelephoneInputIntl
                        name="mainPhoneNumber"
                        id="mainPhoneNumber"
                        setFieldValue={setFieldValue}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
                <Row>
                  <Col>
                    <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                      Company / Distributor
                    </p>
                  </Col>
                </Row>
                <Row>
                  <Col className="px-1 pb-5">
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Name"
                        name="companyName"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <InputFormikField
                        errors={errors}
                        touched={touched}
                        label="Address"
                        name="companyAddress"
                        type="text"
                      />
                    </div>
                    <div className={styles.input__container}>
                      <label
                        htmlFor="mainPhone"
                        className="label_intl_phone_element"
                      >
                        Main Phone Number
                      </label>
                      <TelephoneInputIntl
                        setFieldValue={setFieldValue}
                        name="companyMainPhoneNumber"
                        id="companyMainPhoneNumber"
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="d-flex justify-content-center mt-3">
              <Col xs={9} className="d-flex justify-content-around">
                <Button
                  className={styles.button}
                  type="submit"
                  disabled={isSubmitting}
                  variant="info"
                  size="lg"
                >
                  Invite
                </Button>
                <Button
                  className={styles.button}
                  type="submit"
                  disabled={isSubmitting}
                  variant="outline-info"
                  size="lg"
                >
                  Save and Invite Later
                </Button>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
      )}
    </>
  );
};
