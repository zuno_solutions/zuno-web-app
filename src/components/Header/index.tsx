import React, { useState } from 'react';
import Nav from 'react-bootstrap/Nav';
import { Container, Navbar } from 'react-bootstrap';
import styles from './onBoardingHeader.module.scss';
import zunoLogo from '../../assets/images/zuno_logo.png';
import { IUser, accountService } from '../../_services/account.service';
import leaveLogo from '../../assets/images/leave_logo.png';
import { Modal } from '../_modals/Modal';

export const OnBoardingHeader: React.FC<{}> = () => {
  const [showLeaveModal, setShowLeaveModal] = useState(false);
  const onCloseLeaveModal = () => setShowLeaveModal(false);
  const onOpenLeaveModal = () => {
    setShowLeaveModal(true);
  };

  const user: IUser = JSON.parse(localStorage.getItem('user') || 'null');

  const getModalHeader = (image: string | undefined) => (
    <div className={styles.modal_image_header}>
      <img src={image} alt="log out logo" />
    </div>
  );

  const logOutAndConfirm = () => {
    accountService.logout();
    onCloseLeaveModal();
  };

  return (
    <>
      <Modal
        show={showLeaveModal}
        onHide={onCloseLeaveModal}
        title={getModalHeader(leaveLogo)}
        saveButtonText="Sign Out"
        closeButtonText="Cancel"
        onSave={logOutAndConfirm}
      >
        <>
          <h1>Leave?</h1>
          <p>
            We save your progress so you can resume it the next time you log in
            to your account.
          </p>
        </>
      </Modal>
      <Navbar expand="lg">
        <Container fluid>
          <Nav>
            <Nav.Item>
              <Navbar.Brand href="/" className={`pl-2 ${styles.logo__container}`}>
                <img src={zunoLogo} alt="Zuno app logo" />
              </Navbar.Brand>
            </Nav.Item>
            {user && (
              <>
                <Nav.Item>
                  <Nav.Link href="/dashboard" className={styles.item}>
                    Dashboard
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link href="/manage-personnel" className={styles.item}>
                    Personnel
                  </Nav.Link>
                </Nav.Item>
              </>
            )}
          </Nav>
          {user && (
          <Nav.Item>
            <Nav.Link onClick={onOpenLeaveModal}>Sign out</Nav.Link>
          </Nav.Item>
          )}
        </Container>
      </Navbar>
    </>
  );
};
