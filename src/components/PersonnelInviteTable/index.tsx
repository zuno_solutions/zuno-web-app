/* eslint-disable no-restricted-globals */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable import/order */
import React, { useEffect, useState } from 'react';
import styles from './PersonnelInviteTable.module.scss';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import { perPage, personnelService } from '../../_services/personnel.service';
// @ts-ignore
import cellEditFactory from 'react-bootstrap-table2-editor';
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from 'react-bootstrap-table2-paginator';
import { PersonnelInviteTableColumns } from './columns';
import { Button, Col, Row } from 'react-bootstrap';
import { PersonnelTableSearchBar } from '../PersonnelTableSearchBar';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { NewPersonnelInviteModal } from '../NewPersonnelInviteModal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface PersonnelInviteTableProps {
  selectPersonnel: (id: number) => void;
  updateOnboardingStateFromForm: () => void;
}

export const PersonnelInviteTable = ({
  selectPersonnel, updateOnboardingStateFromForm,
}: PersonnelInviteTableProps) => {
  const [personnel, setPersonnel] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [total, settotal] = useState(1);

  const [showNewPersonnelInviteModal, setShowNewPersonnelInviteModal] = useState(false);
  const onCloseNewPersonnelInviteModal = () => setShowNewPersonnelInviteModal(false);
  const onOpenNewPersonnelInviteModal = () => {
    setShowNewPersonnelInviteModal(true);
  };

  const rowClasses = (row: any, rowIndex: number) => {
    if (rowIndex % 2 === 0) {
      return 'row-style pair-row-style';
    }
    return 'row-style';
  };

  const options = {
    custom: true,
    page: currentPage,
    sizePerPage: perPage,
    totalSize: total,
    alwaysShowAllBtns: true,
  };

  useEffect(() => {
    const getPersonnelData = async () => personnelService.getRawPersonnel({ offset: currentPage });
    getPersonnelData().then((data) => {
      setPersonnel(data.data);
      settotal(data.pagination.total);
    });
  }, [showNewPersonnelInviteModal]);

  const handleTableChange = async (type: any, { page }: { page: number }) => {
    await personnelService.getPersonnel({ offset: page }).then((data) => {
      setPersonnel(data.data);
      settotal(data.pagination.total);
      setCurrentPage(page);
    });
  };

  const handleTableSearch = (value: string) => {
    personnelService.searchPersonnel({ query: value }).then((data) => {
      setPersonnel(data.data);
      settotal(data.pagination.total);
    });
  };

  const handleOnSelect = (row: any, isSelect: any, rowIndex: any, e: any) => {
    selectPersonnel(row.id);
  };

  const handleOnSelectAll = (isSelect: any, rows: any, e: any) => {
    const ids = rows.map((row: any) => row.id);
    selectPersonnel(ids);
  };

  const selectRow = {
    mode: 'checkbox',
    clickToSelect: true,
    onSelect: handleOnSelect,
    onSelectAll: handleOnSelectAll,
  };

  return (
    <>
      {showNewPersonnelInviteModal && (
      <NewPersonnelInviteModal
        show={showNewPersonnelInviteModal}
        onHide={onCloseNewPersonnelInviteModal}
      />
      )}
      <PaginationProvider pagination={paginationFactory(options)}>
        {({
          paginationProps,
          paginationTableProps,
        }: {
          paginationProps: any;
          paginationTableProps: any;
        }) => (
          <>
            <Row
              className={`d-flex justify-content-between ${styles.padding_zero}`}
            >
              <Col className={`d-flex align-items-center ${styles.skip}`}>
                <Button
                  type="button"
                  variant="link"
                  onClick={updateOnboardingStateFromForm}
                  size="sm"
                >
                  Skip to Next Step
                </Button>
              </Col>
              <Col className="my-4 col-md-5 d-flex justify-content-end">
                <PersonnelTableSearchBar
                  handleTableSearch={handleTableSearch}
                />
                <Button
                  type="button"
                  className="rounded-circle mx-3"
                  variant="info"
                  onClick={onOpenNewPersonnelInviteModal}
                >
                  <FontAwesomeIcon
                    className={styles.icon}
                    icon={faPlus}
                  />
                </Button>
              </Col>
            </Row>
            <BootstrapTable
              keyField="id"
              data={personnel}
              columns={PersonnelInviteTableColumns}
              rowClasses={rowClasses}
              headerClasses="header-table"
              {...paginationTableProps}
              remote={{ pagination: true, filter: false, sort: false }}
              onTableChange={handleTableChange}
              selectRow={selectRow}
            />
            <Row
              className={`d-flex justify-content-center ${styles.padding_zero}`}
            >
              <Col className="my-4 col-md-5 d-flex justify-content-center">
                <PaginationListStandalone {...paginationProps} />
              </Col>
            </Row>
          </>
        )}
      </PaginationProvider>
    </>
  );
};
