/* eslint-disable no-plusplus */
// @ts-ignore
import { roleText } from '../../_helpers/role';

export const PersonnelInviteTableColumns = [
  {
    dataField: 'role',
    text: 'Position',
    formatter: (cell: keyof typeof roleText) => roleText[cell],
  },
  {
    dataField: 'last_name',
    text: 'Last Name',
  },
  {
    dataField: 'first_name',
    text: 'First Name',
  },
  {
    dataField: 'phone',
    text: 'Phone Number',
  },
  {
    dataField: 'email',
    text: 'Email',
  },
  // {
  //   dataField: 'manufacturers',
  //   text: 'Manufacturer',
  //   formatter: (cell: { manufacturer: string }[]) => {
  //     for (let i = 0; i < cell.length; i++) {
  //       return <p>{cell[i].manufacturer}</p>;
  //     }
  //     return '';
  //   },
  // },
];
