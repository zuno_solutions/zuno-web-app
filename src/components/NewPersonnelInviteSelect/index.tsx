/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import SelectFormikField from '../_forms/SelectFormikField';
import { positionNewPersonnelInvite } from '../../types/positionNewPersonnelInvite';
import validationSchema from './validationSchema';
import { InviteValues } from '../_forms/InviteForm';
import styles from './NewPersonnelInviteSelect.module.scss';
import InputFormikField from '../_forms/InputFormikField';
import { TelephoneInputIntl } from '../_forms/TelephoneInputIntl';
import SelectFormikFieldOptions from '../_forms/SelectFormikFieldOptions';

interface NewPersonnelInviteSelectValues {
  position: string;
  firstName: string;
  lastName: string;
  email: string;
  mainPhoneNumber: string;
  companyName: string;
  companyAddress: string;
  companyMainPhoneNumber: string;
}

interface NewPersonnelInviteSelectProps {
  handleChange: (personnelPosition: string) => void;
}

export const NewPersonnelInviteSelect = ({
  handleChange,
}: NewPersonnelInviteSelectProps) => {
  const initialValues = {
    position: '',
    firstName: '',
    lastName: '',
    email: '',
    mainPhoneNumber: '',
    companyName: '',
    companyAddress: '',
    companyMainPhoneNumber: '',
  };
  const onSubmit = (
    values: NewPersonnelInviteSelectValues,
    { setStatus, setSubmitting }: FormikHelpers<NewPersonnelInviteSelectValues>,
  ) => console.log(values);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      {({
        errors, touched, isSubmitting, setFieldValue,
      }) => (
        <Form>
          <Row className="mt-5">
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col>
                  <h4 className="my-4 desition_flow_right">Personnel info</h4>
                </Col>
              </Row>
              <Row>
                <Col md={6} className="px-1 pb-5">
                  <SelectFormikFieldOptions
                    name="position"
                    label="Position"
                    type="text"
                    options={positionNewPersonnelInvite}
                    handleChange={handleChange}
                    setFieldValue={setFieldValue}
                    placeholder="Select position"
                  />
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="d-flex justify-content-center mt-3">
            <Col xs={9} className="d-flex justify-content-around">
              <Button
                className={styles.button}
                type="submit"
                disabled={isSubmitting}
                variant="info"
                size="lg"
              >
                Invite
              </Button>
              <Button
                className={styles.button}
                type="submit"
                disabled={isSubmitting}
                variant="outline-info"
                size="lg"
              >
                Save and Invite Later
              </Button>
            </Col>
          </Row>
        </Form>
      )}
    </Formik>
  );
};
