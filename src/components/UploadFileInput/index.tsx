/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { IInputProps } from 'react-dropzone-uploader';

import uploadExcelIcon from '../../assets/images/upload_excel.png';
import styles from './UploadFileInput.module.scss';

export const UploadFileInput = ({
  accept,
  onFiles,
  files,
  getFilesFromEvent,
}: IInputProps) => {
  const text = files.length > 0 ? 'Add more files' : 'Choose files';

  if (files.length === 0) {
    return (
      <>
        <img src={uploadExcelIcon} alt="upload excel file" />
        <p className={styles.input__title}>Drag and drop</p>
        <p className={styles.input__subtitle}>
          or
          {' '}
          <label className={styles.input__label}>
            Browse
            <input
              style={{ display: 'none' }}
              type="file"
              accept={accept}
              multiple
              onChange={(e) => {
                getFilesFromEvent(e).then((chosenFiles: any) => {
                  onFiles(chosenFiles);
                });
              }}
            />
          </label>
          {' '}
          your
          {' '}
          <b>csv</b>
          ,
          {' '}
          <b>xls</b>
          {' '}
          or
          {' '}
          <b>xlsx</b>
          {' '}
          files.
        </p>
      </>
    );
  }
  return null;
};
