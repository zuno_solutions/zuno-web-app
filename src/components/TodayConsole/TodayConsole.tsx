/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';
import { TodayDateRange } from '../TodayDateRange';
import { TodayPersonnel } from '../TodayPersonnel';
import { TodaySurgeriesStatus } from '../TodaySurgeriesStatus/TodayConsole';
import styles from './TodayConsole.module.scss';

interface TodayConsoleProps {
  operationRooms: IOperationRoom[];
  surgeries: ISurgeryItem[];
  handlePersonnelSelection: (personnel: string) => void
  selectedPersonnel: string | undefined;
}

export const TodayConsole = ({
  operationRooms, selectedPersonnel, surgeries, handlePersonnelSelection,
}: TodayConsoleProps) => {
  const todaySurgeries = surgeries.filter((surgery) => moment(surgery.start_time).isSame(new Date(), 'day'));

  return (
    <Row className={`px-2 py-4 ${styles.container}`}>
      <h5>Today</h5>
      <Col className="d-flex justify-content-between flex-column">
        <TodaySurgeriesStatus surgeries={todaySurgeries} />
        <TodayDateRange surgeries={todaySurgeries} />
      </Col>
      <Col className="d-flex justify-content-between flex-column">
        <TodayPersonnel selectedPersonnel={selectedPersonnel} handlePersonnelSelection={handlePersonnelSelection} surgeries={todaySurgeries} />
      </Col>
    </Row>
  );
};
