/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { capitalize } from 'lodash';
import styles from './EditRelations.module.scss';
import { IManufacturer, ISurgeon } from '../../_services/personnel.service';
import { RelationsSuggestionBar } from '../RelationsSuggestionBar';

interface EditRelationsProps {
  relations: any[];
  handleRelationSelection?: (relation: IManufacturer | ISurgeon) => void;
  deselectRelation?: (relation: IManufacturer | ISurgeon) => void;
  getRelationSuggestions?: (value: string) => Promise<any[]>;
  relationsTitle: string;
}

const defaultProps = {
  handleRelationSelection: null,
  deselectRelation: null,
  getRelationSuggestions: null,
};

const EditRelations = ({
  handleRelationSelection,
  relations,
  deselectRelation,
  getRelationSuggestions,
  relationsTitle,
}: EditRelationsProps) => (
  <>
    <h5 className={styles.title}>
      {capitalize(relationsTitle)}
      s
    </h5>
    {handleRelationSelection && getRelationSuggestions && (
    <RelationsSuggestionBar
      handleRelationSelection={handleRelationSelection}
      getRelationSuggestions={getRelationSuggestions}
      role={relationsTitle}
    />
    )}
    <p className={styles.subtitle}>
      Selected
      {' '}
      {relationsTitle}
      s
      {' '}
      Associated.
    </p>
    {relations.map((element) => (
      <div className={`${styles.pill} px-3 py-2 my-2`}>
        <span>
          {element.manufacturer
              || `${element.first_name} ${element.last_name}`}
        </span>
        {deselectRelation && (
        <FontAwesomeIcon
          size="lg"
          className={styles.icon}
          icon={faTrashAlt}
          color="#D1D1D6"
          onClick={() => deselectRelation(element)}
        />
        )}
      </div>
    ))}
  </>
);

EditRelations.defaultProps = defaultProps;
export default EditRelations;
