import React, { useEffect, useState } from 'react';
import styles from './RoomConfig.module.scss';

interface RoomConfigProps {
  roomId: number;
  floor: number;
  variant: 'top' | 'bottom';
  isSelected: boolean;
  setNumberFromCurrentFloor: (roomId: number) => void;
  setConfirmedFromCurrentFloor: () => void;
  position: 'right' | 'left';
  confirmed: boolean;
}

export const RoomConfig = ({
  roomId,
  floor,
  variant,
  isSelected,
  setNumberFromCurrentFloor,
  position,
  confirmed,
  setConfirmedFromCurrentFloor,
}: RoomConfigProps) => {
  const [selected, setSelected] = useState<boolean>(false);

  useEffect(() => {
    setSelected(isSelected);
  }, [isSelected]);

  const handleHover = () => {
    if (!confirmed) {
      setNumberFromCurrentFloor(roomId);
      setSelected(true);
    }
  };

  const handleClick = () => {
    setConfirmedFromCurrentFloor();
    setNumberFromCurrentFloor(roomId);
    setSelected(true);
  };

  const roomName = (roomId).toString().padStart(2, '0');
  const floorName = (floor).toString().padStart(2, '0');

  return (
    <button
      type="button"
      value={roomId}
      key={roomId}
      className={` py-4 px-3 ${styles.room__container} ${
        selected
          ? styles[`room__selected_${position}_true`]
          : styles[`room__selected_${position}_false`]
      } ${styles[`room__container_${variant}`]}`}
      onMouseOver={handleHover}
      onFocus={handleHover}
      onClick={handleClick}
      onKeyDown={handleClick}
    >
      <p>
        floor
        {' '}
        {floorName}
      </p>
      <h4>{roomName}</h4>
      <h5>room</h5>
    </button>
  );
};
