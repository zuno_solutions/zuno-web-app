import React from 'react';
import { Col, Row } from 'react-bootstrap';
import styles from './FormColumn.module.scss';

interface FormColumnProps {
  icon: any;
  title: string;
}

export const FormColumn = ({ icon, title }: FormColumnProps) => (
  <Col className={`${styles.column_container} shadow bg-white mx-3 rounded`}>
    <Row className={`justify-content-between ${styles.column_title}`}>
      <Col>
        <h4>{title}</h4>
      </Col>
      <Col className="d-flex justify-content-end">
        <img src={icon} alt="ambulance logo" />
      </Col>
    </Row>
  </Col>
);
