import React from 'react';
import styles from './RoomButton.module.scss';

interface RoomButtonProps {
  roomId: number;
  setDisplayedRoomsFromCurrentFloor: (roomNumber: number) => void;
}

export const RoomButton = ({
  roomId,
  setDisplayedRoomsFromCurrentFloor,
}: RoomButtonProps) => {
  const handleClick = () => {
    setDisplayedRoomsFromCurrentFloor(roomId + 7);
  };

  return (
    <div
      key={roomId}
      className={` py-4 px-3 ${styles.room__container}`}
      onClick={handleClick}
      onKeyDown={handleClick}
      role="button"
      tabIndex={0}
    >
      <h4>
        {roomId.toString().padStart(2, '0')}
        +
      </h4>
      <h5>room</h5>
      {' '}
    </div>
  );
};
