import React from 'react';
import { Col, Row } from 'react-bootstrap';
import styles from './RoomSetting.module.scss';
import InputFormikField from '../../_forms/InputFormikField';
import SelectFormikField from '../../_forms/SelectFormikField';
import { MedicalSpecialties } from '../../../types/medicalSpecialties';

interface RoomSettingProps {
  roomId: number;
  floor: number;
  initialRoomsNumber: number;
  variant: 'top' | 'bottom';
  position: 'right' | 'left';
}

export const RoomSetting = ({
  roomId,
  floor,
  initialRoomsNumber,
  variant,
  position,
}: RoomSettingProps) => {
  const roomName = roomId.toString().padStart(2, '0');
  const floorName = floor.toString().padStart(2, '0');
  return (
    <Col
      className={`
      py-4 px-3 
      ${styles.room__container}
      ${styles[`room__container_${variant}`]}
      ${styles[`room__container_${position}`]}
      `}
    >
      <Row className="pb-2">
        <Col>
          <p className="text-nowrap">
            Floor
            {' '}
            {floorName}
          </p>
        </Col>
        <Col>
          <p className={`text-nowrap ${styles.room_number}`}>
            Room
            {' '}
            {roomName}
          </p>
        </Col>
      </Row>
      <Row className="mt-4">
        <InputFormikField
          key={`${roomId}_${floor + initialRoomsNumber}_input`}
          name={`rooms.${roomId - 1 + initialRoomsNumber}.name`}
          label="Room Name"
          type="text"
        />
        <SelectFormikField
          key={`${roomId}_${floor + initialRoomsNumber}_select`}
          name={`rooms.${roomId - 1 + initialRoomsNumber}.specialty`}
          label="Specialty"
          type="text"
          options={MedicalSpecialties}
          placeholder="Select specialty"
        />
      </Row>
    </Col>
  );
};
