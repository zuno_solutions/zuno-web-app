import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { onboardingRoomStyles } from '../../../utils/onboardingRoomStyles';
import { RoomSetting } from '../RoomSetting';
import styles from './FloorFormConfig.module.scss';

interface FloorFormConfigProps {
  floor: number;
  roomsNumber: number;
  initialRoomsNumber: number;
}

export const FloorFormConfig = ({
  floor,
  roomsNumber,
  initialRoomsNumber,
}: FloorFormConfigProps) => {
  const roomsNumberArray = Array.from(
    { length: roomsNumber || 0 },
    (_, i) => i + 1,
  );

  let n = -1;

  // useEffect(() => {
  //   if (hospitalData) {
  //     setSelectedOperationRooms(hospitalData.operationRooms.filter(room => room.floor === floor).length);
  //   }
  // }, [hospitalData, floor]);

  return (
    <div className={`mb-5 ${styles.floor__container}`}>
      <Row className="justify-content-center">
        <Col lg={11} xxl={9}>
          <Row className="my-5">
            <p className={styles.desition_flow}>
              Floor
              {' '}
              {floor}
            </p>
          </Row>
          <Row md={4}>
            {roomsNumberArray.map((index, value) => {
              // eslint-disable-next-line no-unused-expressions
              n > 6 ? (n = 0) : (n += 1);
              return (
                <RoomSetting
                  // eslint-disable-next-line react/no-array-index-key
                  key={floor + 100 + value}
                  initialRoomsNumber={initialRoomsNumber}
                  floor={floor}
                  roomId={index}
                  variant={onboardingRoomStyles[n].variant}
                  position={onboardingRoomStyles[n].position}
                />
              );
            })}
          </Row>
        </Col>
      </Row>
    </div>
  );
};
