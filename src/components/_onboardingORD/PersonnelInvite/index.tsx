/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import {
  Button, Col, Container, Row,
} from 'react-bootstrap';
import Datetime from 'react-datetime';
import styles from './PersonnelInvite.module.scss';
import medicalStaffIcon from '../../../assets/images/medical_staff_icon.png';
import { OnboardingTitle } from '../../OnboardingTitle/OnboardingTitle';
import { PersonnelInviteTable } from '../../PersonnelInviteTable';
import { personnelService } from '../../../_services/personnel.service';
import { SuccessModal } from '../../_modals/SuccessModal';

interface ORDPersonnelInviteProps {
  updateOnboardingStateFromForm: () => void;
}

export const PersonnelInvite = ({
  updateOnboardingStateFromForm,
}: ORDPersonnelInviteProps) => {
  const [date, setDate] = useState<number>(new Date().getTime());
  const [personnelIds, setPersonnelIds] = useState<number[]>([]);
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const onCloseSuccessModal = () => setShowSuccessModal(false);
  const onOpenSuccessModal = () => {
    setShowSuccessModal(true);
  };

  const sendInvites = () => {
    personnelService.invitePersonnel(personnelIds, date).then(() => onOpenSuccessModal());
  };

  const selectPersonnel = (id: number | number[]) => {
    if (Array.isArray(id)) {
      if (personnelIds.length < id.length) {
        setPersonnelIds(id);
      } else {
        setPersonnelIds([]);
      }
    } else {
      const newState = [...personnelIds]; // copying the old datas array
      // replace e.target.value with whatever you want to change it to
      const index = newState.indexOf(id);
      if (index !== -1) {
        newState.splice(index, 1);
      } else {
        newState.push(id);
      }
      setPersonnelIds(newState);
    }
  };
  const selectDate = (moment: string | { unix: () => number }) => {
    if (typeof moment === 'object') {
      setDate(moment.unix());
    }
  };

  const inputProps = {
    className: styles.date_input,
  };

  return (
    <Container fluid>
      <SuccessModal
        show={showSuccessModal}
        onHide={onCloseSuccessModal}
      />
      <Row>
        <OnboardingTitle
          image={medicalStaffIcon}
          assistiveText="Zuno is designed for ORCs, Surgeons, Anesthesiologist, Vendors and tray delivery.
          Please invite them to Zunō from the list below."
          title="Invite Personnel"
        />
      </Row>
      <Row className="mx-5">
        <PersonnelInviteTable selectPersonnel={selectPersonnel} updateOnboardingStateFromForm={updateOnboardingStateFromForm} />
      </Row>
      <Row className="justify-content-center mt-2">
        <Col md={4} lg={3} className="d-grid gap-2">
          <div className={`${styles.input__container}`}>
            <label htmlFor="mainPhone" className={styles.label_date_input}>
              Set Invite Date
            </label>
            <Datetime onChange={selectDate} inputProps={inputProps} initialValue={new Date()} />
          </div>
        </Col>
        <Col md={4} lg={3} className="d-grid gap-2 mb-1">
          <Button variant="info" size="lg" onClick={sendInvites}>
            Invite Now
          </Button>
        </Col>
      </Row>
      <Row className="justify-content-center mt-4">
        <Col md={4} className="d-grid gap-2">
          <Button
            variant="info"
            size="lg"
            onClick={updateOnboardingStateFromForm}
          >
            Continue
          </Button>
        </Col>
      </Row>
    </Container>
  );
};
