import React from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import styles from './ORDConnection.module.scss';
import epicConnectionLogo from '../../../assets/images/epic_connection_logo.png';
import orIcon from '../../../assets/images/OR_icon.png';
import { OnboardingTitle } from '../../OnboardingTitle/OnboardingTitle';
import { IUser, accountService } from '../../../_services/account.service';

// eslint-disable-next-line camelcase
const user: IUser = JSON.parse(localStorage.getItem('user') || 'null');

interface ORDConnectionProps {
  updateOnboardingStateFromForm: () => void;
}

export const ORDConnection = ({
  updateOnboardingStateFromForm,
}: ORDConnectionProps) => {
  const logOut = () => {
    accountService.logout();
  };
  return (
    <Col>
      <OnboardingTitle
        image={orIcon}
        assistiveText="Assistive text indicating indicating the purpose of this screen: ORD is comfortable that the data has been pulled through."
        // eslint-disable-next-line camelcase
        title={`Welcome ORD ${user?.first_name} ${user?.last_name}`}
      />
      <Row className="justify-content-center">
        <Col className="my-2 col-md-4">
          <Row>
            <div className="rounded-content border-gradient-zuno">
              <img
                className="my-5 pt-3"
                src={epicConnectionLogo}
                alt="payment account icon"
              />
              <p className={`mb-5 ${styles.ord_connection__text}`}>
                CONNECTION ESTABLISHED
              </p>
            </div>
            <p className={`my-4 ${styles.ord_connection__status}`}>
              Running sync, continue your configuration in the meantime.
            </p>
          </Row>
          <Row>
            <Button
              onClick={updateOnboardingStateFromForm}
              variant="info"
              size="lg"
            >
              Next
            </Button>
          </Row>
          <Row>
            <Button className="mt-2" variant="link" size="lg" onClick={logOut}>
              Save and complete later
            </Button>
          </Row>
        </Col>
      </Row>
    </Col>
  );
};
