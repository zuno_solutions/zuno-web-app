/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
// import styles from './FloorConfig.module.scss';
import { RoomConfig } from '../RoomConfig';
import { RoomButton } from '../RoomButton';
import styles from './FloorConfig.module.scss';
import { accountService } from '../../../_services/account.service';
import { onboardingRoomStyles } from '../../../utils/onboardingRoomStyles';

interface FloorConfigProps {
  floor: number;
  setNumberFromCurrentFloor: (roomId: number) => void;
  setConfirmedFromCurrentFloor: () => void;
  selectedOperationRooms: number;
  confirmed: boolean;
  displayedRooms: number;
  setDisplayedRoomsFromCurrentFloor: (roomNumber: number) => void;
}

export const FloorConfig = ({
  floor,
  setNumberFromCurrentFloor,
  setConfirmedFromCurrentFloor,
  selectedOperationRooms,
  confirmed,
  displayedRooms,
  setDisplayedRoomsFromCurrentFloor,
}: FloorConfigProps) => {
  let n = -1;

  return (
    <div className={`mb-5 ${styles.floor__container}`}>
      <Row className="justify-content-center">
        <Col lg={11} xl={9}>
          <Row className="my-5">
            <p className={styles.desition_flow}>
              How Many Rooms in Floor
              {' '}
              {floor}
            </p>
          </Row>
          <Row className="my-3">
            {[...Array(displayedRooms).keys()].map((value) => {
              // eslint-disable-next-line no-unused-expressions
              n > 6 ? (n = 0) : (n += 1);
              return (
                <RoomConfig
                  key={value + 1}
                  roomId={value + 1}
                  floor={floor}
                  variant={onboardingRoomStyles[n].variant}
                  position={onboardingRoomStyles[n].position}
                  isSelected={selectedOperationRooms > value}
                  setNumberFromCurrentFloor={setNumberFromCurrentFloor}
                  confirmed={confirmed}
                  setConfirmedFromCurrentFloor={setConfirmedFromCurrentFloor}
                />
              );
            })}
          </Row>
          <Row
            className={`justify-content-center mt-5 ${styles.more_rooms__button}`}
          >
            <RoomButton
              roomId={displayedRooms + 1}
              setDisplayedRoomsFromCurrentFloor={
                setDisplayedRoomsFromCurrentFloor
              }
            />
          </Row>
        </Col>
      </Row>
    </div>
  );
};
