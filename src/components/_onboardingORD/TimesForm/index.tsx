/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import {
  Button, Col, Container, Row,
} from 'react-bootstrap';
import styles from './TimesForm.module.scss';
import { alertService } from '../../../_services/alert.service';
import SelectFormikField from '../../_forms/SelectFormikField';
import {
  TrayDeliveryTime,
  VendorGracePeriod,
  VendorStartTime,
  VendorTimeBeforeSurgery,
  VendorTrayGracePeriod,
} from '../../../types/thresholdsArrivalTimes';
import ambulanceIcon from '../../../assets/images/ambulance_icon.png';
import doctorIcon from '../../../assets/images/doctor_icon.png';
import syringeIcon from '../../../assets/images/syringe_icon.png';
import arrivalTimesLogo from '../../../assets/images/arrival_times_logo.png';
import { OnboardingTitle } from '../../OnboardingTitle/OnboardingTitle';
import { onboardingService } from '../../../_services/onboarding.service';

export interface ArrivalTimesValues {
  vendorBeforeSurgery: string;
  vendorGracePeriod: string;
  vendorStartTime: string;
  trayDeliveryTime: string;
  vendorTrayGracePeriod: string;
  surgeonBeforeSurgery: string;
  surgeonGracePeriod: string;
  surgeonStartTime: string;
  anesthesiologistBeforeSurgery: string;
  anesthesiologistGracePeriod: string;
  anesthesiologistStartTime: string;
}

interface ORDTimesFormProps {
  updateOnboardingStateFromForm: () => void;
}

export const TimesForm = ({
  updateOnboardingStateFromForm,
}: ORDTimesFormProps) => {
  const initialValues: ArrivalTimesValues = {
    vendorBeforeSurgery: '15 minutes',
    vendorGracePeriod: '5 minutes',
    vendorStartTime: '5 minutes',
    trayDeliveryTime: '2 hours',
    vendorTrayGracePeriod: '30 minutes',
    surgeonBeforeSurgery: '15 minutes',
    surgeonGracePeriod: '5 minutes',
    surgeonStartTime: '5 minutes',
    anesthesiologistBeforeSurgery: '15 minutes',
    anesthesiologistGracePeriod: '5 minutes',
    anesthesiologistStartTime: '5 minutes',
  };

  const onSubmit = (
    values: ArrivalTimesValues,
    { setStatus, setSubmitting }: FormikHelpers<ArrivalTimesValues>,
  ) => {
    alertService.clear();
    onboardingService
      .createTimeline(values)
      .then(() => {
        window.scrollTo(0, 0);
        alertService.success('Timelines data successfully added', {
          keepAfterRouteChange: true,
        });
        updateOnboardingStateFromForm();
      })
      .catch((error) => {
        alertService.error(error);
        window.scrollTo(0, 0);
      });
  };

  return (
    <Container>
      <Row>
        <OnboardingTitle
          image={arrivalTimesLogo}
          assistiveText="Please set the desired arrival times for the following OR personnel. These thresholds will assist in alerting of delays to the ORC."
          title="Desired Arrival Times"
        />
      </Row>
      <Formik initialValues={initialValues} onSubmit={onSubmit}>
        {({ errors, touched, isSubmitting }) => (
          <Form>
            <Row className="mt-5">
              <Col
                className={`${styles.column_container} shadow bg-white mx-3 rounded`}
              >
                <Row
                  className={`justify-content-between ${styles.column_title}`}
                >
                  <Col>
                    <h4>Vendor</h4>
                  </Col>
                  <Col className="d-flex justify-content-end">
                    <img src={ambulanceIcon} alt="ambulance logo" />
                  </Col>
                </Row>
                <Row>
                  <Col className="px-5 pb-5">
                    <p className="my-4 desition_flow_right">
                      Desired arrival on campus before surgery start time.
                    </p>
                    <SelectFormikField
                      name="vendorBeforeSurgery"
                      label="Desired Arrival time before surgery"
                      type="text"
                      options={VendorTimeBeforeSurgery}
                    />
                    <SelectFormikField
                      name="vendorGracePeriod"
                      label="Grace period"
                      type="text"
                      options={VendorGracePeriod}
                    />
                    <p className="my-4 desition_flow_right">
                      Critical arrival threshold (if this threshold is exceeded
                      the ORC will alerted of a suggested reschedule).
                    </p>
                    <SelectFormikField
                      name="vendorStartTime"
                      label="Before / After scheduled surgery start time"
                      type="text"
                      options={VendorStartTime}
                    />
                    <p className="my-4 desition_flow_right">
                      Tray arrival time
                    </p>
                    <SelectFormikField
                      name="trayDeliveryTime"
                      label="Tray delivery prior to surgery start time"
                      type="text"
                      options={TrayDeliveryTime}
                    />
                    <SelectFormikField
                      name="vendorTrayGracePeriod"
                      label="Grace period"
                      type="text"
                      options={VendorTrayGracePeriod}
                    />
                  </Col>
                </Row>
              </Col>
              <Col
                className={`${styles.column_container} shadow bg-white mx-3 rounded`}
              >
                <Row
                  className={`justify-content-between ${styles.column_title}`}
                >
                  <Col>
                    <h4>Surgeon</h4>
                  </Col>
                  <Col className="d-flex justify-content-end">
                    <img src={doctorIcon} alt="doctor logo" />
                  </Col>
                </Row>
                <Row>
                  <Col className="px-5 pb-5">
                    <p className="my-4 desition_flow_right">
                      Desired arrival on campus before surgery start time.
                    </p>
                    <SelectFormikField
                      name="surgeonBeforeSurgery"
                      label="Desired Arrival time before surgery"
                      type="text"
                      options={VendorTimeBeforeSurgery}
                    />
                    <SelectFormikField
                      name="surgeonGracePeriod"
                      label="Grace period"
                      type="text"
                      options={VendorGracePeriod}
                    />
                    <p className="my-4 desition_flow_right">
                      Critical arrival threshold (if this threshold is exceeded
                      the ORC will alerted of a suggested reschedule).
                    </p>
                    <SelectFormikField
                      name="surgeonStartTime"
                      label="Before / After scheduled surgery start time"
                      type="text"
                      options={VendorStartTime}
                    />
                  </Col>
                </Row>
              </Col>
              <Col
                className={`${styles.column_container} shadow bg-white mx-3 rounded`}
              >
                <Row
                  className={`justify-content-between ${styles.column_title}`}
                >
                  <Col>
                    <h4>Anesthesiologist</h4>
                  </Col>
                  <Col className="d-flex justify-content-end">
                    <img src={syringeIcon} alt="syringe logo" />
                  </Col>
                </Row>
                <Row>
                  <Col className="px-5 pb-5">
                    <p className="my-4 desition_flow_right">
                      Desired arrival on campus before surgery start time.
                    </p>
                    <SelectFormikField
                      name="anesthesiologistBeforeSurgery"
                      label="Desired Arrival time before surgery"
                      type="text"
                      options={VendorTimeBeforeSurgery}
                    />
                    <SelectFormikField
                      name="anesthesiologistGracePeriod"
                      label="Grace period"
                      type="text"
                      options={VendorGracePeriod}
                    />
                    <p className="my-4 desition_flow_right">
                      Critical arrival threshold (if this threshold is exceeded
                      the ORC will alerted of a suggested reschedule).
                    </p>
                    <SelectFormikField
                      name="anesthesiologistStartTime"
                      label="Before / After scheduled surgery start time"
                      type="text"
                      options={VendorStartTime}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="justify-content-center  mt-5">
              <Col md={5} className="d-grid gap-2">
                <Button type="submit" variant="info" size="lg">
                  Continue
                </Button>
              </Col>
            </Row>
            <Row className="justify-content-center mt-2">
              <Col md={5} className="d-grid gap-2">
                <Button variant="link" onClick={updateOnboardingStateFromForm} size="lg">
                  Skip to Next Step
                </Button>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </Container>
  );
};
