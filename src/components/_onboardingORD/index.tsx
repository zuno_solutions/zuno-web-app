/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { ORDConnection } from './ORDConnection';
import { ORDRoomsNumeration } from './ORDRoomsNumeration';
import { ORDRoomsNaming } from './ORDRoomsNaming';
import { ORDOnboardingTypes } from '../../types/onboarding';
import { IUser, accountService } from '../../_services/account.service';
import { TimesForm } from './TimesForm';
import { PersonnelUploadReview } from './PersonnelUploadReview';
import { PersoneelImport } from '../PersoneelImport';
import { PersonnelInvite } from './PersonnelInvite';
import { PersonnelManagement } from '../../pages/PersonnelManagement';

export const OnboardingORD = () => {
  const [ordOnboardingStep, setOrdOnboardingStep] = useState<ORDOnboardingTypes>(ORDOnboardingTypes.ORDConnection);

  const userState = accountService.userValue?.state || 0;
  const hospitalState = accountService.hospitalValue?.state || 0;
  const user: IUser = JSON.parse(localStorage.getItem('user') || 'null');

  const getOnboardingState = () => {
    if (hospitalState === 3 && user.role === 4) {
      setOrdOnboardingStep(ORDOnboardingTypes.ORDConnection);
    } else if (hospitalState === 3) {
      setOrdOnboardingStep(ORDOnboardingTypes.ORDRoomsNumeration);
    } else if (hospitalState === 4) {
      setOrdOnboardingStep(ORDOnboardingTypes.ORDRoomsNaming);
    } else if (hospitalState === 5) {
      setOrdOnboardingStep(ORDOnboardingTypes.ORDRoomsArrivalTimes);
    } else if (hospitalState === 5) {
      setOrdOnboardingStep(ORDOnboardingTypes.ORDPersonnelUploadReview);
    } else {
      setOrdOnboardingStep(ORDOnboardingTypes.ORDConnection);
    }
  };

  useEffect(() => {
    getOnboardingState();
  }, [userState, hospitalState]);

  const steps = Object.values(ORDOnboardingTypes);
  const nextStep = steps.indexOf(ordOnboardingStep) + 1;

  const updateOnboardingStateFromForm = () => {
    setOrdOnboardingStep(steps[nextStep]);
  };

  function getBody() {
    switch (ordOnboardingStep) {
      case ORDOnboardingTypes.ORDConnection:
        return (
          <ORDConnection
            updateOnboardingStateFromForm={updateOnboardingStateFromForm}
          />
        );
      case ORDOnboardingTypes.ORDRoomsNumeration:
        return (
          <ORDRoomsNumeration
            updateOnboardingStateFromForm={updateOnboardingStateFromForm}
          />
        );
      case ORDOnboardingTypes.ORDRoomsNaming:
        return (
          <ORDRoomsNaming
            updateOnboardingStateFromForm={updateOnboardingStateFromForm}
          />
        );
      case ORDOnboardingTypes.ORDRoomsArrivalTimes:
        return <TimesForm updateOnboardingStateFromForm={updateOnboardingStateFromForm} />;
      case ORDOnboardingTypes.ORDPersonnelUpload:
        return <PersoneelImport updateOnboardingStateFromForm={updateOnboardingStateFromForm} />;
      case ORDOnboardingTypes.ORDPersonnelUploadReview:
        return (
          <PersonnelUploadReview
            updateOnboardingStateFromForm={updateOnboardingStateFromForm}
          />
        );
      case ORDOnboardingTypes.PersonnelInvite:
        return (
          <PersonnelInvite
            updateOnboardingStateFromForm={updateOnboardingStateFromForm}
          />
        );
      case ORDOnboardingTypes.PersonnelManagement:
        return (
          <PersonnelManagement
            updateOnboardingStateFromForm={updateOnboardingStateFromForm}
          />
        );
      default:
        return (
          <ORDConnection
            updateOnboardingStateFromForm={updateOnboardingStateFromForm}
          />
        );
    }
  }

  return (
    <Container fluid className="px-0">
      {getBody()}
    </Container>
  );
};
