import React from 'react';
import {
  Button, Col, Container, Row,
} from 'react-bootstrap';
import medicalStaffIcon from '../../../assets/images/medical_staff_icon.png';
import { OnboardingTitle } from '../../OnboardingTitle/OnboardingTitle';
import { PersonnelReviewTable } from '../../PersonnelReviewTable';

interface ORDPersonnelUploadReviewProps {
  updateOnboardingStateFromForm: () => void;
}

export const PersonnelUploadReview = ({
  updateOnboardingStateFromForm,
}: ORDPersonnelUploadReviewProps) => (
  <Container fluid>
    <Row>
      <OnboardingTitle
        image={medicalStaffIcon}
        assistiveText={
          'This is your opportunity to "quick edit" your contact list. When all of the information is correct, click save.'
        }
        title="Personnel Upload Review"
      />
    </Row>
    <Row className="mx-5">
      <PersonnelReviewTable />
    </Row>
    <Row className="justify-content-center mt-2">
      <Col md={5} className="d-grid gap-2">
        <Button variant="info" size="lg" onClick={updateOnboardingStateFromForm}>
          Continue
        </Button>
      </Col>
    </Row>
  </Container>
);
