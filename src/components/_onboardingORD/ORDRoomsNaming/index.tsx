/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { FieldArray, Form, Formik } from 'formik';
import styles from './ORDRoomsNaming.module.scss';
import { accountService } from '../../../_services/account.service';
import orIcon from '../../../assets/images/OR_icon.png';
import { OnboardingTitle } from '../../OnboardingTitle/OnboardingTitle';
import { alertService } from '../../../_services/alert.service';
import { onboardingService } from '../../../_services/onboarding.service';
import { FloorFormConfig } from '../FloorFormConfig';

export interface IRoomSetting {
  room_name: string;
  specialty: string;
  room_no: number;
}

interface ORDRoomsNamingProps {
  updateOnboardingStateFromForm: () => void;
}

export const ORDRoomsNaming = ({
  updateOnboardingStateFromForm,
}: ORDRoomsNamingProps) => {
  const { hospitalValue } = accountService;
  const [displayedRooms, setDisplayedRooms] = useState<number[]>([8, 8]);

  const floorsTotal = hospitalValue?.operating_room_floors || 0;

  const floors = Array.from({ length: floorsTotal || 0 }, (_, i) => i + 1);

  const formattedData = (
    formValues: IRoomSetting[],
  ): { floor_no: number; rooms: IRoomSetting[] }[] => {
    const body = floors.map((floor) => {
      const indexInitial = floor === 1 ? 0 : displayedRooms[floor - 1];
      const indexFinal = displayedRooms
        .slice(0, floor)
        .reduce(
          (previousValue, currentValue) => previousValue + currentValue,
          0,
        );

      return {
        floor_no: floor,
        rooms: formValues.slice(indexInitial, indexFinal),
      };
    });
    return body;
  };

  const handleSubmit = (values: any) => {
    alertService.clear();
    const body = formattedData(values.rooms);
    onboardingService
      .addRoomsInfo(body)
      .then(() => {
        alertService.success('Rooms data successfully added', {
          keepAfterRouteChange: true,
        });
        updateOnboardingStateFromForm();
      })
      .catch((error) => {
        alertService.error(error);
        window.scrollTo(0, 0);
      });
  };

  // const numberOfRooms = accountService.roomsValue || [{ floor_no: 1, roomsCount: 1 }];

  // //PENDIENTES: cambiar cuando el request pueda deolver el numero de rooms por piso
  const initialDisplayedRooms = (data: { roomsCount: number; }[]) => data.map(
    (floorState: { roomsCount: number }) => floorState.roomsCount,
  );

  const getRoomsNumber = async () => accountService.getRoomsData();

  useEffect(() => {
    // //PENDIENTES: llamar el endpoint de numero de rooms por piso
    getRoomsNumber().then((data) => setDisplayedRooms(initialDisplayedRooms(data)));
  }, []);

  const getinitialRoomsArray = () => {
    let initialState: IRoomSetting[] = [];
    displayedRooms.forEach((roomsNumber) => {
      initialState = [
        ...initialState,
        ...Array.from({ length: roomsNumber || 0 }, (_, i) => ({
          room_name: '',
          specialty: '',
          room_no: i + 1,
        })),
      ];
    });
    return initialState;
  };

  return (
    <>
      <OnboardingTitle
        image={orIcon}
        assistiveText="Assistive text indicating indicating the purpose of this screen: enter the business details of the hospital or surgery center."
        title="Make the name room and specialty"
      />
      <Formik
        initialValues={{ rooms: getinitialRoomsArray() }}
        onSubmit={handleSubmit}
      >
        {({ values }) => (
          <Form>
            <FieldArray
              name="rooms"
              render={() => (
                <Row>
                  {floors.map((floor) => (
                    <FloorFormConfig
                      key={floor}
                      floor={floor}
                      roomsNumber={displayedRooms[floor - 1]}
                      // initialRoomsNumber={
                      //   floor === 1 ? 0 : displayedRooms[floor - 2]
                      // }
                      initialRoomsNumber={displayedRooms
                        .slice(0, floor - 1)
                        .reduce(
                          (previousValue, currentValue) => previousValue + currentValue,
                          0,
                        )}
                    />
                  ))}
                </Row>
              )}
            />
            <Row className="justify-content-center">
              <Col md={5}>
                <Row>
                  <Button
                    variant="info"
                    className="my-2"
                    size="lg"
                    type="submit"
                  >
                    Next
                  </Button>
                </Row>
                <Row>
                  <Button
                    variant="link"
                    size="lg"
                    onClick={updateOnboardingStateFromForm}
                  >
                    Skip to Next Step
                  </Button>
                </Row>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </>
  );
};
