import React, { useEffect, useState } from 'react';
import {
  Button, Col, Row, ToggleButton,
} from 'react-bootstrap';
import styles from './ORDRoomsNumeration.module.scss';
import { FloorConfig } from '../FloorConfig';
import { accountService } from '../../../_services/account.service';
import orIcon from '../../../assets/images/OR_icon.png';
import { OnboardingTitle } from '../../OnboardingTitle/OnboardingTitle';
import { alertService } from '../../../_services/alert.service';
import { onboardingService } from '../../../_services/onboarding.service';

interface ORDRoomsNumerationProps {
  updateOnboardingStateFromForm: () => void;
}

export const ORDRoomsNumeration = ({
  updateOnboardingStateFromForm,
}: ORDRoomsNumerationProps) => {
  const { hospitalValue } = accountService;
  const [currentFloor, setCurrentFloor] = useState<number>(1);
  const [floorsData, setFloorsData] = useState<number[]>([]);
  const [confirmedFloors, setConfirmedFloors] = useState<boolean[]>([]);
  const [displayedRooms, setDisplayedRooms] = useState<number[]>([]);

  const floorsTotal = hospitalValue?.operating_room_floors || 0;

  const floors = Array.from({ length: floorsTotal || 0 }, (_, i) => i + 1);

  const nextFloor = currentFloor === floorsTotal ? 0 : currentFloor + 1;

  const selectFloor = (e: any) => {
    const floorId = Number(e.target.value);
    setCurrentFloor(floorId);
  };

  const showNextFloor = () => {
    setCurrentFloor(nextFloor);
    window.scrollTo(0, 0);
  };

  const handleSubmit = () => {
    if (floorsData.indexOf(0) !== -1) {
      setCurrentFloor(floorsData.indexOf(0) + 1);
      window.scrollTo(0, 0);
    } else {
      alertService.clear();
      onboardingService
        .addRooms(floorsData)
        .then(() => {
          alertService.success('Rooms successfully added', {
            keepAfterRouteChange: true,
          });
          updateOnboardingStateFromForm();
        })
        .catch((error) => {
          alertService.error(error);
          window.scrollTo(0, 0);
        });
    }
  };

  const initialDataState = () => floors.map(() => 0);
  const initialConfirmedState = () => floors.map(() => false);
  const initialDisplayedRooms = () => floors.map(() => 8);

  const setNumberFromCurrentFloor = (roomId: number) => {
    const newArr = [...floorsData]; // copying the old datas array
    newArr[currentFloor - 1] = roomId; // replace e.target.value with whatever you want to change it to
    setFloorsData(newArr);
  };

  const setConfirmedFromCurrentFloor = () => {
    const newArr = [...confirmedFloors]; // copying the old datas array
    newArr[currentFloor - 1] = true; // replace e.target.value with whatever you want to change it to
    setConfirmedFloors(newArr);
  };

  const setDisplayedRoomsFromCurrentFloor = (countNumber: number) => {
    const newArr = [...displayedRooms]; // copying the old datas array
    newArr[currentFloor - 1] = countNumber; // replace e.target.value with whatever you want to change it to
    setDisplayedRooms(newArr);
  };

  useEffect(() => {
    setFloorsData(initialDataState());
    setConfirmedFloors(initialConfirmedState());
    setDisplayedRooms(initialDisplayedRooms());
  }, [floorsTotal]);

  return (
    <>
      <OnboardingTitle
        image={orIcon}
        assistiveText="Assistive text indicating indicating the purpose of this screen: enter the business details of the hospital or surgery center."
        title="Floors & Number of Rooms"
      />
      <Row>
        <Col className="my-2">
          <p className={styles.desition_flow}>
            Please select an operating room floor
          </p>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col className="my-4 col-md-4">
          <div
            role="group"
            aria-labelledby="my-radio-group"
            className="d-flex justify-content-between"
          >
            {floors.map((floor: number) => (
              <ToggleButton
                key={floor}
                className="mt-2 mb-0 w-50 me-md-2"
                id={`radio-floor-${floor}`}
                type="radio"
                size="lg"
                variant="outline-info"
                name={`floor ${floor}`}
                value={floor}
                checked={floor === currentFloor}
                onChange={(e) => selectFloor(e)}
              >
                <p className="mt-2 mb-0">{floor}</p>
              </ToggleButton>
            ))}
          </div>
        </Col>
      </Row>
      <Row>
        {floors.map((floor) => {
          if (floor === currentFloor) {
            return (
              <FloorConfig
                key={floor}
                floor={floor}
                setNumberFromCurrentFloor={setNumberFromCurrentFloor}
                setDisplayedRoomsFromCurrentFloor={
                  setDisplayedRoomsFromCurrentFloor
                }
                setConfirmedFromCurrentFloor={setConfirmedFromCurrentFloor}
                displayedRooms={displayedRooms[floor - 1]}
                confirmed={confirmedFloors[floor - 1]}
                selectedOperationRooms={floorsData[floor - 1]}
              />
            );
          }
          return null;
        })}
      </Row>
      <Row className="justify-content-center">
        <Col md={5}>
          <Row>
            {nextFloor > 0 && (
              <Button className="my-4" variant="link" onClick={showNextFloor}>
                Open Floor
                {' '}
                {nextFloor}
              </Button>
            )}
          </Row>
          <Row>
            <Button
              variant="info"
              className="my-2"
              size="lg"
              onClick={handleSubmit}
            >
              Next
            </Button>
          </Row>
          <Row>
            <Button variant="link">Skip to Next Step</Button>
          </Row>
        </Col>
      </Row>
    </>
  );
};
