import React from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import styles from './EpicConnection.module.scss';
import epicConnectionLogo from '../../assets/images/epic_connection_logo.png';

export interface EpicConnectionProps {
  confirmButtonLabel: string;
  cancelButtonLabel: string;
  confirmButtonClick: () => void;
  cancelButtonClick: () => void;
}

export const EpicConnection = ({
  confirmButtonLabel,
  cancelButtonLabel,
  confirmButtonClick,
  cancelButtonClick,
}: EpicConnectionProps) => (
  <Row className="justify-content-center">
    <Col md={4} className="my-4">
      <div className="rounded-content border-gradient-zuno">
        <img
          className="my-5 pt-3"
          src={epicConnectionLogo}
          alt="payment account icon"
        />
        <p className={`mb-5 ${styles.ord_connection__text}`}>
          CONNECTION ESTABLISHED
        </p>
      </div>
      <p className={`my-4 ${styles.ord_connection__status}`}>
        Running sync, continue your configuration in the meantime.
      </p>
      <div className="d-grid gap-2">
        <Button
          target="_blank"
          variant="info"
          size="lg"
          onClick={confirmButtonClick}
        >
          <span className="align-middle">{confirmButtonLabel}</span>
        </Button>
        <Button
          className="mt-2"
          variant="link"
          size="lg"
          onClick={cancelButtonClick}
        >
          {cancelButtonLabel}
        </Button>
      </div>
    </Col>
  </Row>
);
