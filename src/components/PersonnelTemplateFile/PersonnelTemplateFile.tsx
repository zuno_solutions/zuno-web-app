import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Button } from 'react-bootstrap';
import ReactExport from 'react-export-excel';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import styles from './PersonnelTemplateFile.module.scss';

const { ExcelFile } = ReactExport;
const { ExcelSheet } = ReactExport.ExcelFile;
const { ExcelColumn } = ReactExport.ExcelFile;

const dataSet1 = [
  {
    position: 'Surgeon',
    firstName: 'First',
    lastName: 'Last',
    phone: '+1XXXXXXXXXX',
    email: 'name@company.com',
    manufacturers: 'Company, Other company',
    surgeons: 'First, Last',
  },
];

export const PersonnelTemplateFile = () => (
  <ExcelFile
    filename="zuno_template"
    element={(
      <Button variant="link" className={`pt-3 ${styles.download_sample}`} type="button">
        <FontAwesomeIcon
          size="lg"
          className={styles.icon}
          icon={faQuestionCircle}
        />
        {' '}
        Download excel template
      </Button>
)}
  >
    <ExcelSheet data={dataSet1} name="personnel">
      <ExcelColumn label="Position" value="position" />
      <ExcelColumn label="FirstName" value="firstName" />
      <ExcelColumn label="LastName" value="lastName" />
      <ExcelColumn label="Phone" value="phone" />
      <ExcelColumn label="Email" value="email" />
      <ExcelColumn label="Manufacturers" value="manufacturers" />
      <ExcelColumn label="Surgeons" value="surgeons" />
    </ExcelSheet>
  </ExcelFile>
);
