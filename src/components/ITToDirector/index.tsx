import React from 'react';
import { Container } from 'react-bootstrap';
import 'react-intl-tel-input/dist/main.css';
import { OnboardingTitle } from '../OnboardingTitle/OnboardingTitle';
import hospitalIcon from '../../assets/images/hospital_icon.png';
import { ITData } from '../ITData';

export const ITToDirector = () => (
  <Container>
    <OnboardingTitle
      image={hospitalIcon}
      assistiveText="Assistive text indicating indicating the purpose of this screen: after accepting the invite billing contact will proceed."
      title="Welcome IT Peter Doe"
    />
    <ITData />
  </Container>
);
