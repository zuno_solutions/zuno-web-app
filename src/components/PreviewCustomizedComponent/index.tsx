/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import { IPreviewProps } from 'react-dropzone-uploader';
import { faPause, faPlay, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Col, Row } from 'react-bootstrap';
import { formatBytes, formatDuration } from '../../utils/dropZone';
import uploadExcelIconCloud from '../../assets/images/upload_excel_cloud.png';
import styles from './PreviewCustomizedComponent.module.scss';

export const PreviewCustomizedComponent = ({
  className,
  imageClassName,
  style,
  imageStyle,
  fileWithMeta: { cancel, remove, restart },
  meta: {
    name = '',
    percent = 0,
    size = 0,
    previewUrl,
    status,
    duration,
    validationError,
  },
  isUpload,
  canCancel,
  canRemove,
  canRestart,
  extra: { minSizeBytes },
}: IPreviewProps) => {
  let title = `${name || '?'}`;
  const fileSize = `${formatBytes(size)}`;
  if (duration) title = `${title}, ${formatDuration(duration)}`;

  if (status === 'error_file_size' || status === 'error_validation') {
    return (
      <>
        <div className={className} style={style}>
          <img src={uploadExcelIconCloud} alt="upload excel file" />

          <span className="dzu-previewFileNameError">{title}</span>
          {status === 'error_file_size' && (
            <span>
              {size < minSizeBytes ? 'File too small' : 'File too big'}
            </span>
          )}
          {status === 'error_validation' && (
            <span>{String(validationError)}</span>
          )}
          {canRemove && (
            <button
              type="button"
              onClick={remove}
              className={styles.input__icon}
            >
              <FontAwesomeIcon icon={faTimes} />
            </button>
          )}
        </div>
      </>
    );
  }

  if (
    status === 'error_upload_params'
    || status === 'exception_upload'
    || status === 'error_upload'
  ) {
    title = `${title} (upload failed)`;
  }
  if (status === 'aborted') title = `${title} (cancelled)`;
  if (status === 'done') title = `${title} (successfully uploaded)`;

  return (
    <Row>
      <Col>
        <img src={uploadExcelIconCloud} alt="upload excel file" />
        {previewUrl && (
          <img
            className={imageClassName}
            style={imageStyle}
            src={previewUrl}
            alt={title}
            title={title}
          />
        )}
      </Col>
      <Col>
        <div className={`d-flex ${status === 'done' ? styles.green : null}`}>
          <div className={`${styles.file_name} w-100`}>
            {!previewUrl && (
            <p>
              {title}
              {' '}
              <span className={styles.file_size}>{fileSize}</span>
            </p>
            )}
          </div>
          <div className="flex-shrink-1 d-inline flex-column">
            {status === 'uploading' && canCancel && (
              <div className="inline">
                <button
                  type="button"
                  onClick={cancel}
                  className={`${styles.input__icon} d-inline`}
                >
                  <FontAwesomeIcon icon={faPause} />
                </button>
              </div>

            )}
            {status !== 'preparing'
            && status !== 'getting_upload_params'
            && status !== 'uploading'
            && canRemove && (
              <div className="inline">
                <button
                  type="button"
                  onClick={remove}
                  className={`${styles.input__icon} d-inline`}
                >
                  <FontAwesomeIcon icon={faTimes} />
                </button>
              </div>
            )}
            {[
              'error_upload_params',
              'exception_upload',
              'error_upload',
              'aborted',
              'ready',
            ].includes(status)
            && canRestart && (
              <div className="inline">
                <button
                  type="button"
                  onClick={restart}
                  className={`${styles.input__icon} d-inline`}
                >
                  <FontAwesomeIcon icon={faPlay} />
                </button>
              </div>
            )}
          </div>
        </div>

        {isUpload && (
          <progress
            max={100}
            // value={percent}
            value={
            status === 'done'
            // status === 'done' || status === 'headers_received'
              ? 100
              : 95
              }
          />
        )}

      </Col>
    </Row>
  );
};
