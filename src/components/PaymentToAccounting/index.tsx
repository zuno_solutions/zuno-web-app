import React from 'react';
import { Col, Row } from 'react-bootstrap';
import styles from './PaymentToAccounting.module.scss';
import 'react-intl-tel-input/dist/main.css';
import paymentIcon from '../../assets/images/payment_icon.png';
import PaymentInfo from '../_forms/PaymentInfo';

interface PaymentToAccountingProps {
  setOnboardingState: () => void;
}

export const PaymentToAccounting = ({
  setOnboardingState,
}: PaymentToAccountingProps) => (
  <Row className="justify-content-center">
    <Col className="my-4" md={5} lg={4}>
      <div className={styles.payment_info_img__container}>
        <img src={paymentIcon} alt="payment account icon" />
      </div>
      <h1 className={styles.basic__title}>Payment to Accounting</h1>
      <p>
        Assistive text indicating indicating the purpose of this screen: after
        accepting the invite billing contact will proceed.
      </p>
      <Row>
        <Col className="my-2">
          <p className="h5 mt-5 mb-2">Account owner: John Doe</p>
        </Col>
      </Row>
      <PaymentInfo setOnboardingState={setOnboardingState} accounting />
    </Col>
  </Row>
);
