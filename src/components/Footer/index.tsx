import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Stepper from '../Stepper';
import styles from './Footer.module.scss';

type FooterProps = {
  step?: number
  totalSteps?: number;
};

const defaultProps = {
  step: undefined,
  totalSteps: undefined,
};

const Footer: React.FC<FooterProps> = ({ step, totalSteps }) => (
  <Row className="mt-5 mb-3 px-3 justify-content-between">
    <hr />
    <Col>
      <p className={styles.footer_text}>
        Need help?
        {' '}
        <a href="mailto:contact@zuno.pro" target="_blank" rel="noopener noreferrer">
          <strong className={styles.footer_link}>contact@zuno.pro</strong>
        </a>
        {' '}
      </p>
    </Col>
    <Col md={3} className="d-flex justify-content-end align-items-start">
      {step
      && <Stepper step={step} totalSteps={totalSteps} />}
    </Col>
  </Row>
);

const OnBoardingFooter: React.FC<FooterProps> = ({
  step,
  totalSteps,
}) => (
  <Row className="mt-5 mb-3 px-3 justify-content-between">
    <Col>
      <p className={styles.footer_text}>
        Need help?
        {' '}
        <a
          href="mailto:contact@zuno.pro"
          target="_blank"
          rel="noopener noreferrer"
        >
          <strong className={styles.footer_link}>contact@zuno.pro</strong>
        </a>
        {' '}
      </p>
    </Col>
    <Col md={3} className="d-flex justify-content-end align-items-start">
      {step && <Stepper step={step} totalSteps={totalSteps} />}
    </Col>
  </Row>
);

Footer.defaultProps = defaultProps;
OnBoardingFooter.defaultProps = defaultProps;

export {
  Footer,
  OnBoardingFooter,
};
