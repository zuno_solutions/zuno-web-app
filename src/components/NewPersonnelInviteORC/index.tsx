import React, { useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import { positionNewPersonnelInvite } from '../../types/positionNewPersonnelInvite';
// import { positionStartTime } from '../../types/positionnNewPersonnelInviteORC';
import validationSchema from './validationSchema';
import styles from './NewPersonnelInviteORC.module.scss';
import InputFormikField from '../_forms/InputFormikField';
import { TelephoneInputIntl } from '../_forms/TelephoneInputIntl';
import SelectFormikFieldOptions from '../_forms/SelectFormikFieldOptions';

interface NewPersonnelInviteORCValues {
  position: string;
  firstName: string;
  lastName: string;
  email: string;
  mainPhoneNumber: string;
  companyName: string;
  companyAddress: string;
  companyMainPhoneNumber: string;
}

interface NewPersonnelInviteORCProps {
  handleChange: (personnelPosition: number) => void;
}

const dayWeekOptions = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export const NewPersonnelInviteORC = ({
  handleChange,
}: NewPersonnelInviteORCProps) => {
  const initialValues = {
    position: '4',
    firstName: '',
    lastName: '',
    email: '',
    mainPhoneNumber: '',
    companyName: '',
    companyAddress: '',
    companyMainPhoneNumber: '',
  };
  const onSubmit = (
    values: NewPersonnelInviteORCValues,
    { setStatus, setSubmitting }: FormikHelpers<NewPersonnelInviteORCValues>,
  ) => console.log(values);

  let dayWeekSelected: string;

  const saveSchedule = () => console.log({ dayWeekSelected });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      {({
        errors, touched, isSubmitting, setFieldValue,
      }) => (
        <Form>
          <Row className="mt-5">
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col>
                  <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    Personnel info
                  </p>
                </Col>
              </Row>
              <Row>
                <Col className="px-1 pb-5">
                  <div className={styles.input__container}>
                    <SelectFormikFieldOptions
                      name="position"
                      label="Position"
                      type="text"
                      options={positionNewPersonnelInvite}
                      handleChange={handleChange}
                      setFieldValue={setFieldValue}
                    />
                  </div>
                  <div className={styles.input__container}>
                    <InputFormikField
                      errors={errors}
                      touched={touched}
                      label="First Name"
                      name="firstName"
                      type="text"
                    />
                  </div>
                  <div className={styles.input__container}>
                    <InputFormikField
                      errors={errors}
                      touched={touched}
                      label="Last Name"
                      name="lastName"
                      type="text"
                    />
                  </div>
                  <div className={styles.input__container}>
                    <InputFormikField
                      errors={errors}
                      touched={touched}
                      label="Email"
                      name="email"
                      type="text"
                    />
                  </div>
                  <div className={styles.input__container}>
                    <label
                      htmlFor="mainPhone"
                      className="label_intl_phone_element"
                    >
                      Main Phone Number
                    </label>
                    <TelephoneInputIntl
                      name="mainPhoneNumber"
                      id="mainPhoneNumber"
                      setFieldValue={setFieldValue}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col className="px-1">
                  <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    Availability
                  </p>
                </Col>
              </Row>
              <Row>
                <Col className="px-1 pb-1">
                  <div className={styles.input__container}>
                    <h6 className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                      Select all days that you are usually scheduled.
                    </h6>
                  </div>
                  <div
                    role="group"
                    aria-labelledby="my-radio-group"
                    className={`d-flex justify-content-between ${styles.div_circle__week}`}
                  >
                    {dayWeekOptions.map((dayWeek) => (
                      <Button
                        type="button"
                        className={`rounded-circle mx-3 ${styles.button_unmargin}`}
                        variant="info"
                        onClick={saveSchedule}
                      />
                    ))}
                  </div>
                </Col>
                <Col>
                  <h6 className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    columna inicio
                  </h6>
                  {/* <SelectFormikFieldOptions
                    name="startTime"
                    label="startTime"
                    type="text"
                    options={positionStartTime}
                    handleChange={handleChange}
                    setFieldValue={setFieldValue}
                  /> */}
                </Col>
                <Col>
                  <h6 className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    columna fin
                  </h6>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="d-flex justify-content-center mt-3">
            <Col xs={9} className="d-flex justify-content-around">
              <Button
                className={styles.button}
                type="submit"
                disabled={isSubmitting}
                variant="info"
                size="lg"
              >
                Invite
              </Button>
              <Button
                className={styles.button}
                type="submit"
                disabled={isSubmitting}
                variant="outline-info"
                size="lg"
              >
                Save and Invite Later
              </Button>
            </Col>
          </Row>
        </Form>
      )}
    </Formik>
  );
};
