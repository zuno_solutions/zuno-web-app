import React, { Fragment, useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button } from 'react-bootstrap';
import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons';
import { useHistory } from 'react-router-dom';
import styles from './setPassword.module.scss';
import validationSchema from './validationSchema';
import { accountService } from '../../../_services/account.service';
import { alertService } from '../../../_services/alert.service';
import InputFormikField from '../InputFormikField';

interface Values {
  confirmPassword: string;
  password: string;
}

export const SetPassword = () => {
  const [passwordShown, setPasswordShown] = useState<any>({
    password: false,
    confirmPassword: false,
  });

  const history = useHistory();
  const email = accountService.emailValue;
  const togglePasswordVisiblity = (state: string) => {
    setPasswordShown((prevState: any) => (passwordShown[state]
      ? { ...prevState, [state]: false }
      : { ...prevState, [state]: true }));
  };

  const initialValues: Values = {
    password: '',
    confirmPassword: '',
  };
  const onSubmit = (
    values: Values,
    { setStatus, setSubmitting }: FormikHelpers<Values>,
  ) => {
    const { password } = values;
    setStatus();
    setSubmitting(true);
    accountService
      .signup({ email, password })
      .then(() => {
        alertService.success('Registration successful, please login', {
          keepAfterRouteChange: true,
        });
        history.push('login');
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  return (
    <>
      <h1 className={styles.form_title}>Set your password.</h1>
      <p>
        Requirements: min 6 characters, at least 1 capital, 1 Number and 1
        special character.
      </p>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({ errors, touched, isSubmitting }) => (
          <Form>
            <div className={styles.input__container}>
              <InputFormikField
                label="Enter your password"
                name="password"
                type={passwordShown.password ? 'text' : 'password'}
                errors={errors}
                touched={touched}
                icon={passwordShown.password ? faEyeSlash : faEye}
                onClickIcon={() => togglePasswordVisiblity('password')}
              />
            </div>
            <div className={styles.input__container}>
              <InputFormikField
                name="confirmPassword"
                label="Reenter password"
                type={passwordShown.confirmPassword ? 'text' : 'password'}
                errors={errors}
                touched={touched}
                icon={passwordShown.confirmPassword ? faEyeSlash : faEye}
                onClickIcon={() => togglePasswordVisiblity('confirmPassword')}
              />
            </div>
            <div className={`d-grid gap-2 ${styles.submit__button}`}>
              <Button
                type="submit"
                disabled={isSubmitting}
                variant="info"
                size="lg"
              >
                Continue
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};
