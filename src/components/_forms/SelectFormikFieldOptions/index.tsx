/* eslint-disable react/require-default-props */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { ErrorMessage, Field } from 'formik';
import styles from './SelectFormikFieldOptions.module.scss';

interface InputProps {
  type: string;
  name: string;
  label: string;
  options: { value: number, name: string }[];
  placeholder?: string
  handleChange?: (personnelPosition: number) => void
  disabled?: boolean;
  setFieldValue: any
}

const SelectFormikFieldOptions = ({
  type, name, options, label, placeholder, handleChange, setFieldValue, disabled,
}: InputProps) => (
  <div className="form-group">
    <div className={`mt-2 ${styles.input__container}`}>
      <label htmlFor={name} className={styles.label_element}>
        {label}
      </label>
      <Field
        as="select"
        name={name}
        type={type}
        className={`${styles.text_input} ${disabled ? styles.disabled : null}`}
        onChange={handleChange ? (e:any) => {
          setFieldValue(name, e.target.value);
          handleChange(e.target.value);
        } : null}
        disabled={disabled}
      >
        {placeholder && <option value="">{placeholder}</option>}
        {options.map((option) => (
          <option value={option.value} key={option.value}>
            {option.name}
          </option>
        ))}
      </Field>
    </div>
    <ErrorMessage
      name={name}
      className={`${styles.invalid_feedback} mt-2 mb-1`}
      component="div"
    />
  </div>
);

export default SelectFormikFieldOptions;
