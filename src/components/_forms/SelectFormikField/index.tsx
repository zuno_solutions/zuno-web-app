/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { ErrorMessage, Field } from 'formik';
import styles from './SelectFormikField.module.scss';

interface InputProps {
  type: string;
  name: string;
  label: string;
  options: string[] | number[];
  // eslint-disable-next-line react/require-default-props
  placeholder?: string
}

const SelectFormikField = ({
  type, name, options, label, placeholder,
}: InputProps) => (
  <div className="form-group">
    <div className={`mt-2 ${styles.input__container}`}>
      <label htmlFor={name} className={styles.label_element}>
        {label}
      </label>
      <Field
        as="select"
        name={name}
        type={type}
        className={`${styles.text_input}`}
      >
        {placeholder && <option value="">{placeholder}</option>}
        {options.map((option) => (
          <option value={option} key={option}>
            {option}
          </option>
        ))}
      </Field>
    </div>
    <ErrorMessage
      name={name}
      className={`${styles.invalid_feedback} mt-2 mb-1`}
      component="div"
    />
  </div>
);

export default SelectFormikField;
