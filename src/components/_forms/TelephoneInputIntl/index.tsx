import { ErrorMessage } from 'formik';
import React from 'react';
import IntlTelInput from 'react-intl-tel-input';
import 'react-intl-tel-input/dist/main.css';
// import ErrorField from '../ErrorField/ErrorField';

import styles from './TelephoneInputIntl.module.scss';

interface TelephoneInputIntlProps {
  id: string;
  name: string;
  setFieldValue: any;
  // eslint-disable-next-line react/require-default-props
  disabled?: boolean;
  // eslint-disable-next-line react/require-default-props
  defaultValue?: string;
  // eslint-disable-next-line react/require-default-props
  value?: string;
  // eslint-disable-next-line react/require-default-props
  onPhoneNumberChange?: (number: string, dialCode: string | undefined) => void
}

export const TelephoneInputIntl = ({
  id,
  name,
  setFieldValue,
  defaultValue,
  value,
  onPhoneNumberChange,
  ...props
}: TelephoneInputIntlProps) => {
  // process number into string with area code for submission
  const processNumber = (
    isValid: boolean,
    phone: string,
    country: { dialCode?: string | undefined },
  ) => {
    const phoneNumber = `+${country.dialCode} ${phone}`.replace(
      /[\s()-]+/gi,
      '',
    );
    return phoneNumber;
  };

  return (
    <>
      <IntlTelInput
        fieldId={id}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
        containerClassName="intl-tel-input"
        inputClassName="intl-tel-input-level"
        fieldName={name}
        onPhoneNumberChange={onPhoneNumberChange ? (isValid, phone, country) => {
          onPhoneNumberChange(phone, country.dialCode);
          setFieldValue(name, processNumber(isValid, phone, country));
        } : (isValid, phone, country) => {
          setFieldValue(name, processNumber(isValid, phone, country));
        }}
        // separateDialCode
        autoPlaceholder={false}
        format
        defaultValue={defaultValue}
        value={value}
      />
      {/* <ErrorField formik={formik} label={name} /> */}
      <ErrorMessage
        name={name}
        className={`${styles.invalid_feedback} mt-2 mb-1`}
        component="div"
      />
    </>
  );
};
