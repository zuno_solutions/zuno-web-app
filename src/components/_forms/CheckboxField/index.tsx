import React from 'react';
import { ErrorMessage } from 'formik';
import styles from './CheckboxField.module.scss';

interface CheckboxFieldProps {
  handleChange: any;
  values: any;
  label: JSX.Element | JSX.Element[] | string;
  name: string;
}

export const CheckboxField = ({
  handleChange,
  values, label, name,
}: CheckboxFieldProps) => (
  <>
    <label>
      <input
        id={name}
        name={name}
        type="checkBox"
        onChange={handleChange}
        checked={values[name]}
        className="form-check-input"
      />
      {label}
    </label>
    <ErrorMessage
      name={name}
      className={`${styles.invalid_feedback} mt-2 mb-1`}
      component="div"
    />
  </>
);
