import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ErrorMessage, Field } from 'formik';
import styles from './InputFormikField.module.scss';

interface InputProps {
  type: string;
  name: string;
  label: string;
  errors?: { [key: string]: string };
  touched?: { [key: string]: boolean };
  variant?: 'top_combo' | 'bottom_combo';
  onClickIcon?: any;
  icon?: any;
  disabled?: boolean;
}

const defaultProps = {
  variant: null,
  onClickIcon: null,
  icon: null,
  errors: null,
  touched: null,
  disabled: null,
};

const InputFormikField = ({
  type,
  name,
  errors,
  touched,
  label,
  variant,
  onClickIcon,
  icon,
  disabled,
}: InputProps) => (
  <div className="form-group">
    <div className={`${styles.input__container}`}>
      <label htmlFor={name} className={styles.label_element}>
        {label}
      </label>
      <Field
        disabled={disabled}
        name={name}
        type={type}
        className={`form-control ${
          errors && errors[name] && touched && touched[name]
            ? ' is-invalid'
            : ''
        } ${
          (!errors || !errors[name] || !touched || !touched[name]) && variant
            ? styles[variant]
            : ''
        } ${styles.text_input}`}
      />
      <div className={styles.input__actions_container}>
        {icon && (
          <button
            type="button"
            onClick={onClickIcon}
            className={styles.input_arrow__icon}
          >
            <FontAwesomeIcon icon={icon} />
          </button>
        )}
      </div>
    </div>
    <ErrorMessage
      name={name}
      className={`${styles.invalid_feedback} mt-2 mb-1`}
      component="div"
    />
  </div>
);

InputFormikField.defaultProps = defaultProps;
export default InputFormikField;
