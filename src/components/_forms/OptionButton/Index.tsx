import React from 'react';
import { ToggleButton } from 'react-bootstrap';
import styles from './optionButton.module.scss';

interface OptionButtonProps {
  handleChange: any;
  values: any;
  value: any;
  name: string;
  label?: string | number;
}

const defaultProps = {
  label: undefined,
};

const OptionButton = ({
  handleChange,
  values,
  value,
  name,
  label = value,
}: OptionButtonProps) => (
  <ToggleButton
    className={styles.button}
    key={value}
    id={`radio-${value}`}
    type="radio"
    size="lg"
    variant="outline-info"
    name={name}
    value={value}
    checked={values[name] === value}
    onChange={handleChange}
  >
    <div>{label}</div>
  </ToggleButton>
);

OptionButton.defaultProps = defaultProps;
export default OptionButton;
