import React, { useState } from 'react';
import { Form, Formik } from 'formik';
import { Row, ToggleButton } from 'react-bootstrap';
import styles from './PaymentInfo.module.scss';
import validationSchema from './validationSchema';
import 'react-intl-tel-input/dist/main.css';
import { PaymentDataCard } from '../PaymentDataCard';
import { PaymentACH } from '../PaymentACH';
import { Pricing } from '../../Pricing';
import InputFormikField from '../InputFormikField';
import { TelephoneInputIntl } from '../TelephoneInputIntl';

export interface PaymentFormValues {
  billingAddress: string;
  mainPhone: string;
  firstName: string;
  lastName: string;
  invoicesEmail: string;
}

interface PaymentInfoProps {
  setOnboardingState: () => void;
  accounting?: boolean;
}

const defaultProps = {
  accounting: false,
};

const PaymentInfo = ({
  setOnboardingState,
  accounting,
}: PaymentInfoProps) => {
  const [payment, setPayment] = useState<string>('');
  const [billingAddress, setBillingAddress] = useState('');
  const [mainPhone, setMainPhone] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [invoicesEmail, setInvoicesEmail] = useState('');

  const initialValues = {
    billingAddress: '',
    mainPhone: '',
    firstName: '',
    lastName: '',
    invoicesEmail: '',
  };
  const onSubmit = (values: PaymentFormValues) => {
    console.log(billingAddress, mainPhone, firstName, lastName, invoicesEmail);
    setBillingAddress(values.billingAddress);
    setMainPhone(values.mainPhone);
    setFirstName(values.firstName);
    setLastName(values.lastName);
    setInvoicesEmail(values.invoicesEmail);
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({
          errors, touched, handleSubmit, setFieldValue,
        }) => (
          <Form>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="Billing Address (if different than above)"
                name="billingAddress"
                type="text"
              />
            </div>
            <div className={styles.input__container}>
              <label
                htmlFor="mainPhone"
                className="label_intl_phone_element"
              >
                Main Phone Number
              </label>
              <TelephoneInputIntl
                name="mainPhone"
                id="mainPhone"
                setFieldValue={setFieldValue}
              />
            </div>
            <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
              Billing contact
            </p>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="First Name"
                name="firstName"
                type="text"
              />
            </div>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="Last Name"
                name="lastName"
                type="text"
              />
            </div>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="Email (Invoices and receipts will be sent here)"
                name="invoicesEmail"
                type="text"
              />
            </div>

            {accounting && (
            <>
              <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>Pricing</p>
              <Pricing />
            </>
            )}
            <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>Payment Info</p>
            <Row>
              <div
                role="group"
                aria-labelledby="payment-form-group"
                className="d-flex justify-content-between"
              >
                <ToggleButton
                  className="mt-2 mb-0 w-50 me-md-2"
                  key="CreditCard"
                  id="radio-CreditCard"
                  type="radio"
                  size="lg"
                  variant="outline-info"
                  name="CreditCard"
                  value="CreditCard"
                  checked={payment === 'CreditCard'}
                  onChange={() => setPayment('CreditCard')}
                >
                  <span className="align-middle">By Credit Card</span>
                </ToggleButton>
                <ToggleButton
                  className="mt-2 mb-0 w-50 me-md-2"
                  key="ACH"
                  id="radio-ACH"
                  type="radio"
                  size="lg"
                  variant="outline-info"
                  name="ACH"
                  value="ACH"
                  checked={payment === 'ACH'}
                  onChange={() => setPayment('ACH')}
                >
                  <span className="align-middle">By ACH</span>
                </ToggleButton>
              </div>
            </Row>
            {payment === 'CreditCard' && (
            <PaymentDataCard
              formikOnsubmit={handleSubmit}
              setOnboardingState={setOnboardingState}
            />
            )}
            {payment === 'ACH' && <PaymentACH />}
          </Form>
        )}
      </Formik>
    </>
  );
};

PaymentInfo.defaultProps = defaultProps;
export default PaymentInfo;
