import * as yup from 'yup';

const validationSchema = yup.object().shape({
  facilityLegalName: yup.string().required(),
  phisicalAddress: yup.string().required(),
  facilityName: yup.string().required(),
  mainPhone: yup
    .string()
    .min(6, 'Phone should be of minimum 6 characters length')
    .required(),
});

export default validationSchema;
