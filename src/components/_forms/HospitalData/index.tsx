import React from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import {
  Button, Col, Container, Row,
} from 'react-bootstrap';
import styles from './hospitalData.module.scss';
import validationSchema from './validationSchema';
import 'react-intl-tel-input/dist/main.css';
import hospitalIcon from '../../../assets/images/hospital_icon.png';
import { onboardingService } from '../../../_services/onboarding.service';
import { alertService } from '../../../_services/alert.service';
import OptionButton from '../OptionButton/Index';
import InputFormikField from '../InputFormikField';
import { TelephoneInputIntl } from '../TelephoneInputIntl';

export interface HospitalFormValues {
  facilityLegalName: string;
  phisicalAddress: string;
  facilityName: string;
  mainPhone: string;
  orNumber: string;
}

interface HospitalDataProps {
  setOnboardingState: () => void;
}

const floorsNumberOptions = ['1', '2', '3'];

export const HospitalData = ({ setOnboardingState }: HospitalDataProps) => {
  const initialValues = {
    facilityLegalName: '',
    phisicalAddress: '',
    facilityName: '',
    mainPhone: '',
    orNumber: '1',
  };
  const onSubmit = (
    values: HospitalFormValues,
    { setStatus, setSubmitting }: FormikHelpers<HospitalFormValues>,
  ) => {
    setStatus();
    setSubmitting(true);
    onboardingService
      .hospitals(values)
      .then(() => {
        alertService.success('Hospital Data saved', {
          keepAfterRouteChange: true,
        });
        setOnboardingState();
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  return (
    <Container>
      <div className={styles.hospital_info_img__container}>
        <img src={hospitalIcon} alt="hospital account icon" />
      </div>
      <h1 className={styles.hospital_info__title}>Facility Info</h1>
      <p>
        Assistive text indicating indicating the purpose of this screen: enter
        the business details of the hospital or surgery center.
      </p>
      <Row className="justify-content-center">
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
        >
          {({
            errors, touched, isSubmitting, setFieldValue, handleChange, values,
          }) => (
            <Form>
              <Row className="justify-content-center">
                <Col md={5}>

                  <div className={`${styles.input__container} form-group`}>
                    <InputFormikField
                      errors={errors}
                      touched={touched}
                      label="Facility Legal Name"
                      name="facilityLegalName"
                      type="text"
                    />
                  </div>
                </Col>
                <Col md={5}>
                  <div className={`${styles.input__container} form-group`}>
                    <InputFormikField
                      errors={errors}
                      touched={touched}
                      label="Phisical Address"
                      name="phisicalAddress"
                      type="text"
                    />
                  </div>
                </Col>
              </Row>
              <Row className="justify-content-center">
                <Col md={5}>
                  <div className={`${styles.input__container} form-group`}>
                    <InputFormikField
                      errors={errors}
                      touched={touched}
                      label="Facility Name"
                      name="facilityName"
                      type="text"
                    />
                  </div>
                </Col>
                <Col md={5}>
                  <div className={styles.input__container}>
                    <label
                      htmlFor="mainPhone"
                      className="label_intl_phone_element"
                    >
                      Main Phone Number
                    </label>
                    <TelephoneInputIntl
                      name="mainPhone"
                      id="mainPhone"
                      setFieldValue={setFieldValue}
                    />
                  </div>
                </Col>
              </Row>
              <Row className="justify-content-center">
                <Col xs md="6" className="mt-4">
                  <div
                    id="my-radio-group"
                    className={`my-3 ${styles.label_buttons_element}`}
                  >
                    Number of operating room floors?
                  </div>
                  <div
                    role="group"
                    aria-labelledby="my-radio-group"
                    className="d-flex justify-content-between"
                  >
                    {floorsNumberOptions.map((floornumber) => (
                      <OptionButton
                        name="orNumber"
                        value={floornumber}
                        label={floornumber}
                        handleChange={handleChange}
                        values={values}
                      />
                    ))}

                  </div>
                  <div className="d-grid gap-2 mx-1 mt-5">
                    <Button
                      type="submit"
                      disabled={isSubmitting}
                      variant="info"
                      size="lg"
                    >
                      Save Details and go to step 2 (Personal info)
                    </Button>
                  </div>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </Row>
    </Container>
  );
};
