import * as yup from 'yup';

const validationSchema = yup.object().shape({
  firstName: yup
    .string()
    .matches(/^[A-Za-z ]*$/, 'Please enter valid name')
    .max(40)
    .required('First name is required'),
  lastName: yup
    .string()
    .matches(/^[A-Za-z ]*$/, 'Please enter valid name')
    .max(40)
    .required('Last name is required'),
  mainPhone: yup
    .string()
    .min(6, 'Phone should be of minimum 6 characters length')
    .required('Phone number is required'),
});

export default validationSchema;
