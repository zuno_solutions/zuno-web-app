import React from 'react';
import { Form, Formik } from 'formik';
import { Button } from 'react-bootstrap';
import styles from './PaymentACH.module.scss';
import validationSchema from './validationSchema';
import 'react-intl-tel-input/dist/main.css';
import achImg from '../../../assets/images/ach_image.png';
import { CheckboxField } from '../CheckboxField';
import InputFormikField from '../InputFormikField';

export const PaymentACH = () => {
  const initialValues = {
    routingNumber: '',
    accountNumber: '',
    checkNumber: '',
    check: false,
  };
  const onSubmit = () => {
    console.log('ACH Payment info submitted');
  };

  return (
    <>
      <div className="my-4">
        <img src={achImg} alt="Email confirmation icon" />
      </div>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {({
          errors, touched, isSubmitting, handleChange, values,
        }) => (
          <Form>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="Routing Number"
                name="routingNumber"
                type="text"
              />
            </div>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="Account Number"
                name="accountNumber"
                type="text"
              />
            </div>
            <div className={styles.input__container}>
              <InputFormikField
                errors={errors}
                touched={touched}
                label="Check Number"
                name="checkNumber"
                type="text"
              />
            </div>

            <div className={`${styles.check__form} form-check mt-4`}>
              <CheckboxField
                handleChange={handleChange}
                values={values}
                label={(
                  <p>
                    I Accept the
                    {' '}
                    <a href="/">Terms & Conditions</a>
                    {' '}
                  </p>
            )}
                name="check"
              />
            </div>
            <div className="d-grid gap-2">
              <Button
                type="submit"
                disabled={isSubmitting}
                variant="info"
                size="lg"
              >
                Submit
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};
