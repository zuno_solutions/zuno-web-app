import React from 'react';
import {
  Form, Formik, FormikHelpers,
} from 'formik';
import {
  Button, Col, Container, Row,
} from 'react-bootstrap';
import styles from './personalData.module.scss';
import validationSchema from './validationSchema';
import userIcon from '../../../assets/images/user_icon.png';
import { onboardingService } from '../../../_services/onboarding.service';
import { alertService } from '../../../_services/alert.service';
import { OnboardingTitle } from '../../OnboardingTitle/OnboardingTitle';
import InputFormikField from '../InputFormikField';
import { TelephoneInputIntl } from '../TelephoneInputIntl';

export interface PersonalFormValues {
  firstName: string;
  lastName: string;
  jobTitle: string;
  mainPhone: string;
  mobilePhone: string;
}

interface PersonalDataProps {
  setOnboardingState: () => void;
}

export const PersonalData = ({ setOnboardingState }: PersonalDataProps) => {
  const initialValues = {
    firstName: '',
    lastName: '',
    jobTitle: '',
    mainPhone: '',
    mobilePhone: '',
  };
  const onSubmit = (
    values: PersonalFormValues,
    { setStatus, setSubmitting }: FormikHelpers<PersonalFormValues>,
  ) => {
    setStatus();
    setSubmitting(true);
    onboardingService
      .personalData(values)
      .then(() => {
        alertService.success('Personal Data saved', {
          keepAfterRouteChange: true,
        });
        setOnboardingState();
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  return (
    <Container>
      <Row>
        <OnboardingTitle
          image={userIcon}
          assistiveText="Assistive text indicating indicating the purpose of this screen: enter
          the personal information."
          title="Personal Info."
        />
      </Row>
      <Row className="justify-content-center">
        <Col className="my-2 col-md-6">
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
          >
            {({
              errors, touched, isSubmitting, setFieldValue,
            }) => (
              <Form>
                <div className={styles.input__container}>
                  <InputFormikField
                    errors={errors}
                    touched={touched}
                    label="First Name"
                    name="firstName"
                    type="text"
                  />
                </div>
                <div className={styles.input__container}>
                  <InputFormikField
                    errors={errors}
                    touched={touched}
                    label="Last Name"
                    name="lastName"
                    type="text"
                  />
                </div>
                <div className={styles.input__container}>
                  <InputFormikField
                    errors={errors}
                    touched={touched}
                    label="Title"
                    name="jobTitle"
                    type="text"
                  />
                </div>
                <div className={styles.input__container}>
                  <label
                    htmlFor="mainPhone"
                    className="label_intl_phone_element"
                  >
                    Main Phone Number
                  </label>
                  <TelephoneInputIntl
                    name="mainPhone"
                    id="mainPhone"
                    setFieldValue={setFieldValue}
                  />
                </div>
                <div className={`d-grid gap-2 ${styles.submit__button}`}>
                  <Button
                    type="submit"
                    disabled={isSubmitting}
                    variant="info"
                    size="lg"
                  >
                    Save and go to step 3 (Billing)
                  </Button>
                </div>
              </Form>
            )}
          </Formik>
        </Col>
      </Row>
    </Container>
  );
};
