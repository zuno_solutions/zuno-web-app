import React from 'react';
import { Col, Row } from 'react-bootstrap';
import styles from './ChooseEMR.module.scss';
import epicLogo from '../../../assets/images/epic_logo.png';
import cernerLogo from '../../../assets/images/cerner_logo.png';

interface ChooseEMRProps {
  emr: string;
  setEmr: (emrName: string) => void;
}

export const ChooseEMR = ({ emr, setEmr }: ChooseEMRProps) => (
  <>
    <Row className="justify-content-center">
      <Col className="my-4 col-md-5">
        <p className={styles.desition_flow}>Choose your EMR</p>
        <p className={`my-4 ${styles.ord_connection__status}`}>
          By selecting your EMR provider below you will be redirected to
          another browser window, where you will need to enter your admin
          login credentials and authorize communication with Zuno.
          {' '}
        </p>
      </Col>
    </Row>
    <Row className="justify-content-center">
      <Col className="col-md-4">
        <label className={styles.radioButton}>
          <input
            type="radio"
            name="test"
            value="epic"
            checked={emr === 'epic'}
            onChange={() => setEmr('epic')}
          />
          <div
            className={`${styles.radioButton} rounded-content border-gradient-zuno`}
          >
            <img
              className={styles.imgButton}
              src={epicLogo}
              alt="cerner logo"
            />
          </div>
        </label>
      </Col>
      <Col className="col-md-4">
        <label className={styles.radioButton}>
          <input
            type="radio"
            name="test"
            value="cerner"
            checked={emr === 'cerner'}
            className={styles.radio}
            onChange={() => setEmr('cerner')}
          />
          <div
            className={`${styles.radioButton} rounded-content border-gradient-zuno`}
          >
            <img
              className={styles.imgButton}
              src={cernerLogo}
              alt="cerner logo"
            />
          </div>
        </label>
      </Col>
    </Row>
  </>
);
