import React from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import styles from './InviteForm.module.scss';
import validationSchema from './validationSchema';
import { alertService } from '../../../_services/alert.service';
import { CheckboxField } from '../CheckboxField';
import { accountService } from '../../../_services/account.service';
import InputFormikField from '../InputFormikField';
import { TelephoneInputIntl } from '../TelephoneInputIntl';

export interface InviteValues {
  firstName: string;
  lastName: string;
  email: string;
  mainPhone: string;
  check: boolean;
}

interface InviteFormProps {
  setOnboardingState: () => void;
  handleFormSubmit: (values: InviteValues) => Promise<any>;
  check?: boolean;
  mainPhone?: boolean;
  submitButtonText?: string;
}

const defaultProps = {
  check: false,
  mainPhone: false,
  submitButtonText: null,
};

const InviteForm = ({
  setOnboardingState,
  handleFormSubmit,
  check,
  submitButtonText,
  mainPhone,
}: InviteFormProps): JSX.Element => {
  const initialValues = {
    firstName: '',
    lastName: '',
    email: '',
    check: false,
  };
  const onSubmit = (
    values: InviteValues,
    { setStatus, setSubmitting }: FormikHelpers<InviteValues>,
  ) => {
    setStatus();
    setSubmitting(true);
    handleFormSubmit(values)
      .then(() => {
        alertService.success('Your invite has been set', {
          keepAfterRouteChange: true,
        });
        setOnboardingState();
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  const logOut = () => {
    accountService.logout();
  };

  return (
    <Row className="justify-content-center">
      <Col className="col-md-5">
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
        >
          {({
            errors, touched, isSubmitting, handleChange, values, setFieldValue,
          }) => (
            <Form>
              <div className={styles.input__container}>
                <InputFormikField
                  errors={errors}
                  touched={touched}
                  label="First Name"
                  name="firstName"
                  type="text"
                />
              </div>
              <div className={styles.input__container}>
                <InputFormikField
                  errors={errors}
                  touched={touched}
                  label="Last Name"
                  name="lastName"
                  type="text"
                />
              </div>
              <div className={styles.input__container}>
                <InputFormikField
                  errors={errors}
                  touched={touched}
                  name="email"
                  label="Email"
                  type="text"
                />
              </div>
              {mainPhone && (
              <div className={styles.input__container}>
                <label
                  htmlFor="mainPhone"
                  className="label_intl_phone_element"
                >
                  Main Phone Number
                </label>
                <TelephoneInputIntl
                  name="mainPhone"
                  id="mainPhone"
                  setFieldValue={setFieldValue}
                />
              </div>
              )}
              {check && (
              <div className={`${styles.check__form} form-check my-4`}>
                <CheckboxField
                  handleChange={handleChange}
                  values={values}
                  label={(
                    <p>
                      I Accept the
                      {' '}
                      <a href="/">Terms & Conditions</a>
                      {' '}
                    </p>
                    )}
                  name="check"
                />
              </div>
              )}
              <div className="d-grid gap-2 mt-4">
                <Button
                  type="submit"
                  disabled={isSubmitting}
                  variant="info"
                  size="lg"
                >
                  {submitButtonText || 'Save and Continue'}
                </Button>
              </div>
              {!submitButtonText && (
              <div className="d-grid gap-2">
                <Button variant="link" onClick={logOut}>
                  Save and complete later
                </Button>
              </div>
              )}
            </Form>
          )}
        </Formik>
      </Col>
    </Row>
  );
};

InviteForm.defaultProps = defaultProps;
export default InviteForm;
