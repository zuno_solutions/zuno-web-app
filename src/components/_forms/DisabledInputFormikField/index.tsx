import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Field } from 'formik';
import styles from './DisabledInputFormikField.module.scss';

interface InputProps {
  type: string;
  name: string;
  label: string;
  onClickIcon?: any;
  icon?: any;
  disabled?: boolean;
}

const defaultProps = {
  onClickIcon: null,
  icon: null,
  disabled: null,
};

const DisabledInputFormikField = ({
  type,
  name,
  label,
  onClickIcon,
  icon,
  disabled,
}: InputProps) => (
  <div className="form-group">
    <div className={`${styles.input__container}`}>
      <label htmlFor={name} className={styles.label_element}>
        {label}
      </label>
      <Field
        disabled={disabled}
        name={name}
        type={type}
        className={`form-control  ${styles.text_input} ${styles.disabled}`}
      />
      <div className={styles.input__actions_container}>
        {icon && (
          <button
            type="button"
            onClick={onClickIcon}
            className={styles.input_arrow__icon}
          >
            <FontAwesomeIcon icon={icon} />
          </button>
        )}
      </div>
    </div>
  </div>
);

DisabledInputFormikField.defaultProps = defaultProps;
export default DisabledInputFormikField;
