/* eslint-disable @typescript-eslint/no-var-requires */
// @ts-ignore
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { IUser } from '../../../_services/account.service';
import { alertService } from '../../../_services/alert.service';
import { paymentService } from '../../../_services/payment.service';

const {
  CardComponent,
  CardNumber,
  CardExpiry,
  CardCVV,
} = require('@chargebee/chargebee-js-react-wrapper');

interface IError {
  expiry: { errorCode: string; message: string };
  number: { errorCode: string; message: string };
  cvv: { errorCode: string; message: string };
}

interface PaymentDataCardProps {
  formikOnsubmit: () => void;
  setOnboardingState: () => void;

}

export const PaymentDataCard = ({
  setOnboardingState,
  formikOnsubmit,
}: PaymentDataCardProps) => {
  useEffect(() => {
    try {
      // const instance = (window as any).Chargebee?.getInstance();
      // setChargeBeeInstance(instance)
    } catch (e) {
      // setChargebeeError("Chargebee instance don't load, please reload")
      console.log('error in chargebee');
    }
  }, []);

  const user: IUser = JSON.parse(localStorage.getItem('user') || 'null');

  const subscriptionId = 'cbdemo_lite-USD-monthly';

  // locale
  const locale = 'en';
  const cardRef = React.createRef<any>();

  const [token] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);
  const [values, setValues] = useState({ firstName: '' });
  const [errors, setErrors] = useState({
    number: { errorCode: '', message: '' },
    cvv: { errorCode: '', message: '' },
    expiry: { errorCode: '', message: '' },
  });

  const history = useHistory();

  const onSubmitPayment = () => {
    setLoading(true);
    // Call tokenize method through card component's ref
    cardRef.current.tokenize({}).then((data: any) => paymentService
      .createPlanSubscriptionsDB({
        cardToken: data.token,
        subscriptionId,
      })
      .then(() => {
        alertService.success('Payment completed');
        setLoading(false);
        if (user.role === 1) {
          setOnboardingState();
        } else {
          history.push('/dashboard');
        }
      })
      .catch((error) => {
        setLoading(false);
        alertService.error(error);
        setLoading(false);
      }));
  };

  useEffect(() => {
    if (loading) {
      onSubmitPayment();
    }
  }, [loading]);

  const handleSubmit = () => {
    formikOnsubmit();
    setLoading(true);
  };

  // const onSubmitPayment = () => {
  //   setLoading(true);
  //   cardRef.current
  //     .tokenize({
  //       billingAddress,
  //       mainPhone,
  //       firstName,
  //       lastName,
  //       invoicesEmail,
  //     })
  //     .then(async (data: any) => {
  //       try {
  //         const res = await paymentService.createPlanSubscriptionsDB({
  //           cardToken: data.token,
  //           subscriptionId,
  //         });
  //         if (res.message != null) throw new Error(res.message);
  //         handleSubmit && (await handleSubmit());
  //       } catch (error) {

  //         if (error.message.includes("Error message: (3001)"))
  //           setErrorCreate("Insufficient funds.");
  //         else setErrorCreate(error.message);
  //       } finally {
  //         setLoading(false);
  //       }
  //     })
  //     .catch((error: any) => {
  //       setLoading(false);
  //     });
  // };

  const handleChange = (event: any) => {
    const { target } = event;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const { name } = target;

    setValues((prevState: any) => ({
      ...prevState,
      [name]: value,
    }));
  };

  // Method to trigger on card component ready
  const onReady = () => {
    console.log('card component ready');
  };

  // Method to trigger on field focus
  const onFocus = (event: any) => {
    console.log(event.field, 'focused');
  };

  // Method to trigger on field blur
  const onBlur = (event: any) => {
    console.log(event.field, 'blurred');
  };

  // Validation error handling
  const onChange = (event: any) => {
    const errorsArray = errors;
    let eventErrorMessage = '';

    if (event.error) {
      // If error is present, display the error
      errorsArray[event.field as keyof IError] = event.error;
      eventErrorMessage = event.error.message;
    } else {
      errorsArray[event.field as keyof IError] = null as any;
      // If there's no error, check for existing error
      const errorsObj = Object.values(errorsArray).filter((val) => val);

      // The errorObject holds a message and code
      // Custom error messages can be displayed based on the error code
      const errorObj = errorsObj.pop();

      // Display existing message
      if (errorObj) eventErrorMessage = errorObj.message;
      else eventErrorMessage = '';
    }
    setErrors(errorsArray);
    setErrorMessage(eventErrorMessage);
  };

  return (
    <div className="chargebee_card__container">
      <div className="chargebee_card-wrap">
        <form id="payment">
          <div className="chargebee_card-contain">
            <div className="chargebee_card-fieldset">
              <div className="chargebee_card-field">
                <span className="chargebee_card-label">Name</span>
                <input
                  name="firstName"
                  className={
                    values.firstName
                      ? 'chargebee_card-input val'
                      : 'chargebee_card-input'
                  }
                  type="text"
                  placeholder="John Doe"
                  value={values.firstName}
                  onChange={handleChange}
                />
              </div>
              {/* Pass all options as props to card component */}
              {/* Assign ref to call internal methods */}
              <CardComponent
                ref={cardRef}
                className="fieldset field"
                locale={locale}
                onReady={onReady}
                onChange={onChange}
                icon={false}
              >
                {/* ^ Attach ready, change listeners to card component */}
                <div className="chargebee_card-field">
                  {/* Card number field */}
                  <span className="chargebee_card-label">Card Number</span>
                  <CardNumber
                    className="chargebee_card-input"
                    onFocus={onFocus}
                    onBlur={onBlur}
                  />
                </div>
                <div className="chargebee_card-fields">
                  <div className="chargebee_card-field">
                    {/* Card expiry field */}
                    <span className="chargebee_card-label">
                      Expiration Date
                    </span>
                    <CardExpiry
                      className="chargebee_card-input"
                      onFocus={onFocus}
                      onBlur={onBlur}
                    />
                  </div>
                  <div className="chargebee_card-field">
                    {/* Card cvv field */}
                    <span className="chargebee_card-label">CVV</span>
                    <CardCVV
                      className="chargebee_card-input"
                      onFocus={onFocus}
                      onBlur={onBlur}
                    />
                  </div>
                </div>
              </CardComponent>
            </div>
          </div>
          <div className="d-grid gap-2 mt-2">
            <Button variant="info" size="lg" onClick={handleSubmit}>
              Submit
            </Button>
          </div>
          {errorMessage && (
            <div className="error" role="alert">
              {errorMessage}
            </div>
          )}
          {token && <div className="token">{token}</div>}
        </form>
      </div>
    </div>
  );
};
