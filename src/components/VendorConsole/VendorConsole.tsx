/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import moment from 'moment';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';
import styles from './VendorConsole.module.scss';

interface VendorConsoleProps {
  item: ISurgeryItem;
}

export const VendorConsole = ({ item }: VendorConsoleProps) => (
  <Col
    className={`d-flex justify-content-between flex-column pt-4 pb-5 ${styles.container}`}
  >
    <Row>

      <h5>
        {item.trays}
        {' '}
        Trays
      </h5>
      <p
        className={styles.status}
        style={{
          color: item.traysStatus === 'late' ? '#FFCC00' : '#34C759',
        }}
      >
        {item.traysStatus}
        {' '}
        {item.traysStatus === 'late' && (
        <span>
          {item.late}
          {' '}
          MIN
        </span>
        )}
      </p>
    </Row>
    <Row>
      <h4>Vendor </h4>
      <p className={styles.subtitle}>{item.vendor}</p>
      <p
        className={styles.status}
        style={{
          color: item.surgeonStatus === 'late' ? '#FFCC00' : '#34C759',
        }}
      >
        {item.surgeonStatus}
        {' '}
        {item.surgeonStatus === 'late' && (
        <span>
          {item.late}
          {' '}
          MIN
        </span>
        )}
      </p>

      <h4>Manufacturer</h4>
      <p className={styles.subtitle}>{item.manufacturer}</p>
    </Row>
  </Col>
);
