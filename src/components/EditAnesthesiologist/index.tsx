/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import { Form, Formik, FormikHelpers } from 'formik';
import { Button, Col, Row } from 'react-bootstrap';
import SelectFormikField from '../_forms/SelectFormikField';
import { positionNewPersonnelInvite } from '../../types/positionNewPersonnelInvite';
import validationSchema from './validationSchema';
import { InviteValues } from '../_forms/InviteForm';
import styles from './EditAnesthesiologist.module.scss';
import { TelephoneInputIntl } from '../_forms/TelephoneInputIntl';
import SelectFormikFieldOptions from '../_forms/SelectFormikFieldOptions';
import { personnelService } from '../../_services/personnel.service';
import DisabledInputFormikField from '../_forms/DisabledInputFormikField';
import { alertService } from '../../_services/alert.service';

interface EditAnesthesiologistValues {
  position: string;
  firstName: string;
  lastName: string;
  email: string;
  mainPhoneNumber: string;
  companyName: string;
  companyAddress: string;
  companyMainPhoneNumber: string;
}

interface EditAnesthesiologistProps {
  row: any;
  onHide: () => void;
  updatePersonnel: () => void;
}

export const EditAnesthesiologist = ({ row, onHide, updatePersonnel }: EditAnesthesiologistProps) => {
  const [position, setPosition] = useState<string>('7');
  const [firstName, setFirstName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [mainPhoneNumber, setMainPhoneNumber] = useState<string>('');
  const [mainDialCode, setMainDialCode] = useState<string | undefined>('');
  const [companyName, setCompanyName] = useState<string>('');
  const [companyAddress, setCompanyAddress] = useState<string>('');
  const [companyMainPhoneNumber, setCompanyMainPhoneNumber] = useState<string>('');

  const initialValues = {
    position,
    firstName,
    lastName,
    email,
    mainPhoneNumber,
    companyName,
    companyAddress,
    companyMainPhoneNumber,
  };

  const onSubmit = (
    values: EditAnesthesiologistValues,
    { setStatus, setSubmitting }: FormikHelpers<EditAnesthesiologistValues>,
  ) => {
    const newValues = mainDialCode ? {
      ...values,
      mainPhoneNumber: `+${mainDialCode} ${mainPhoneNumber}`.replace(
        /[\s()-]+/gi,
        '',
      ),
    } : values;
    setStatus();
    setSubmitting(true);
    personnelService
      .editRawPersonnel(row.id, newValues)
      .then(() => {
        updatePersonnel();
        onHide();
        // setSuccessSubmit(true);
      })
      .catch((error) => {
        setSubmitting(false);
        alertService.error(error);
      });
  };

  const onPhoneNumberChange = (number: string, dialCode: string | undefined) => {
    setMainPhoneNumber(number);
    setMainDialCode(dialCode);
  };

  useEffect(() => {
    const getPersonnelData = async () => personnelService.getRawPersonnelById(row.id);
    getPersonnelData().then((data) => {
      setFirstName(data.rawPersonnel.first_name);
      setLastName(data.rawPersonnel.last_name);
      setEmail(data.rawPersonnel.email);
      setMainPhoneNumber(data.rawPersonnel.phone);
      // setCompanyName(data)
      // setCompanyAddress(data)
      // setCompanyMainPhoneNumber(data)
    });
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {({
        errors, touched, isSubmitting, setFieldValue,
      }) => (
        <Form>
          <Row className="mt-2">
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col>
                  <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    Personnel info
                  </p>
                </Col>
              </Row>
              <Row>
                <Col className="px-1 pb-5">
                  <div className={styles.input__container}>
                    <SelectFormikFieldOptions
                      name="position"
                      label="Position"
                      type="text"
                      options={positionNewPersonnelInvite}
                      // handleChange={handleChange}
                      setFieldValue={setFieldValue}
                      disabled
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="First Name"
                      name="firstName"
                      type="text"
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Last Name"
                      name="lastName"
                      type="text"
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Email"
                      name="email"
                      type="text"
                    />
                  </div>
                  <div className={styles.input__container}>
                    <label
                      htmlFor="mainPhone"
                      className="label_intl_phone_element"
                    >
                      Main Phone Number
                    </label>
                    <TelephoneInputIntl
                      name="mainPhoneNumber"
                      id="mainPhoneNumber"
                      setFieldValue={setFieldValue}
                      value={mainPhoneNumber}
                      onPhoneNumberChange={onPhoneNumberChange}
                    />
                  </div>
                </Col>
              </Row>
            </Col>
            <Col className={`${styles.column_container} bg-white mx-3 rounded`}>
              <Row>
                <Col>
                  <p className={`mb-2 mt-4 ${styles.payment__subtitle}`}>
                    Company / Distributor
                  </p>
                </Col>
              </Row>
              <Row>
                <Col className="px-1 pb-5">
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Name"
                      name="companyName"
                      type="text"
                    />
                  </div>
                  <div className={styles.input__container}>
                    <DisabledInputFormikField
                      label="Address"
                      name="companyAddress"
                      type="text"
                    />
                  </div>
                  <div className={styles.input__container}>
                    <label
                      htmlFor="mainPhone"
                      className="label_intl_phone_element"
                    >
                      Company Main Phone Number
                    </label>
                    <TelephoneInputIntl
                      setFieldValue={setFieldValue}
                      name="companyMainPhoneNumber"
                      id="companyMainPhoneNumber"
                    />
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="d-flex justify-content-center">
            <Col xs={9} className="d-flex justify-content-around">
              <Button
                className={styles.button}
                type="submit"
                disabled={isSubmitting}
                variant="info"
                size="lg"
              >
                Save
              </Button>
            </Col>
          </Row>
          <Row className="d-flex justify-content-center">
            <Col xs={9} className="d-flex justify-content-around">
              <Button
                className={styles.button}
                disabled={isSubmitting}
                variant="link"
                size="lg"
                onClick={onHide}
              >
                Cancel
              </Button>
            </Col>
          </Row>
        </Form>
      )}
    </Formik>
  );
};
