import React from 'react';
import { Redirect } from 'react-router-dom';

import { HospitalData } from '../_forms/HospitalData';
import { PaymentInfoPage } from '../PaymentInfoPage';
import { OnboardingSteps } from '../../pages/OnboardingPage';
import { PersonalData } from '../_forms/PersonalData';
import { ITDataPage } from '../ITDataPage';
import { ORDDataPage } from '../ORDDataPage';
import { Role } from '../../_helpers/role';
import { IUser } from '../../_services/account.service';

interface SetAccountProps {
  onboardingStatus: string;
  setOnboardingState: () => void;
  roles: Role[];
}

export const SetAccount = ({
  onboardingStatus,
  setOnboardingState,
  roles,
}: SetAccountProps) => {
  const user: IUser = JSON.parse(localStorage.getItem('user') || 'null');

  if (roles && roles.indexOf(user.role) === -1 && user.role === 2) {
    // role not authorized so redirect to home page
    return <Redirect to={{ pathname: '/set-account/payment' }} />;
  } if (roles && roles.indexOf(user.role) === -1 && user.role === 3) {
    return <Redirect to={{ pathname: '/set-account/it' }} />;
  } if (roles && roles.indexOf(user.role) === -1 && user.role === 4) {
    return <Redirect to={{ pathname: '/set-account/ord' }} />;
  }

  function getBody() {
    switch (onboardingStatus) {
      case OnboardingSteps.HospitalData:
        return <HospitalData setOnboardingState={setOnboardingState} />;
      case OnboardingSteps.PersonalData:
        return <PersonalData setOnboardingState={setOnboardingState} />;
      case OnboardingSteps.PaymentData:
        return <PaymentInfoPage setOnboardingState={setOnboardingState} />;
      case OnboardingSteps.ITData:
        return <ITDataPage setOnboardingState={setOnboardingState} />;
      case OnboardingSteps.ORDData:
        return <ORDDataPage setOnboardingState={setOnboardingState} />;
      // case UserProfile.It:
      //   return <It/>;
      default:
        return <HospitalData setOnboardingState={setOnboardingState} />;
    }
  }

  return <>{getBody()}</>;
};
