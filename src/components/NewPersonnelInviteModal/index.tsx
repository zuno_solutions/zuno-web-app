import React, { useState } from 'react';
import { Modal } from '../_modals/Modal';
import styles from './NewPersonnelInviteModal.module.scss';
import medicalStaff from '../../assets/images/medical_staff_icon.png';
import { NewPersonnelInviteVendor } from '../NewPersonnelInviteVendor';
import { NewPersonnelInviteSelect } from '../NewPersonnelInviteSelect';
import { NewPersonnelInviteSurgeon } from '../NewPersonnelInviteSurgeon';
import { NewPersonnelInviteAnesthesiologist } from '../NewPersonnelInviteAnesthesiologist';
import { roleText } from '../../_helpers/role';
import successLogo from '../../assets/images/check_circle_outlined.png';

interface NewPersonnelInviteModalProps {
  onHide: () => void;
  show: boolean
}

export const NewPersonnelInviteModal = ({
  onHide,
  show,
}: NewPersonnelInviteModalProps) => {
  const [formForPosition, setformForPosition] = useState<keyof typeof roleText | 0>(0);
  const [succededSubmit, setSuccededSubmit] = useState<boolean>(false);

  const handleSuccededSubmit = () => {
    setSuccededSubmit(true);
  };
  const handleChange = (personnelPosition: string) => {
    const role = Number(personnelPosition);
    // @ts-ignore
    setformForPosition(role);
  };

  const getHeader = (image: string | undefined) => (
    <>
      {!succededSubmit && (
      <div className={`mb-4 ${styles.modal_image_header_padding}`}>
        <img src={image} alt="log out logo" />
      </div>
      )}
      <div>
        {formForPosition === 0 && !succededSubmit && (
        <>
          <h3>Invite Personnel</h3>
          <div className="d-flex justify-content-center">
            <h5 className={`${styles.subtitle_title}`}>
              Choose the appropriate position below.
            </h5>
          </div>
        </>
        )}
        {formForPosition !== 0 && !succededSubmit && (
        <h3>
          Add New
          {' '}
          {roleText[formForPosition]}
        </h3>
        )}
        {succededSubmit && (
        <div className={styles.modal_image_header}>
          <img src={successLogo} alt="success logo" />
        </div>
        )}
      </div>
    </>
  );

  return (
    <>
      <Modal
        show={show}
        onHide={onHide}
        title={getHeader(medicalStaff)}
        dialogClassName={styles.modal_width}
        closeButtonText={succededSubmit ? 'Close' : 'Cancel'}
      >
        <>
          {formForPosition === 0
          && <NewPersonnelInviteSelect handleChange={handleChange} />}
          {formForPosition === 7
          && <NewPersonnelInviteAnesthesiologist handleChange={handleChange} handleSuccededSubmit={handleSuccededSubmit} succededSubmit={succededSubmit} />}
          {formForPosition === 5
          && <NewPersonnelInviteVendor handleChange={handleChange} handleSuccededSubmit={handleSuccededSubmit} succededSubmit={succededSubmit} />}
          {formForPosition === 6
          && <NewPersonnelInviteSurgeon handleChange={handleChange} handleSuccededSubmit={handleSuccededSubmit} succededSubmit={succededSubmit} />}
        </>
      </Modal>
    </>
  );
};
