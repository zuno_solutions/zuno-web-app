/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import styles from './SwimmingLanesSurgeryLane.module.scss';

export const SwimmingLanesSurgeryLane = ({
  group,
  selectedRoom,
}: {
  group: any;
  selectedRoom: number | string | undefined
}) => (
  <div className={styles.custom_group}>
    <p
      className={styles.title}
      style={
              { background: group.color }
            }
    >
      {group.title}

    </p>
  </div>
);
