/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import moment, { Moment } from 'moment';
import styles from './NotificationItem.module.scss';
import scalpelIcon from '../../assets/images/scalpel_icon.png';
import syringeIcon from '../../assets/images/syringe_blue_icon.png';

export const NotificationItem = ({
  item,
  sideBarCollapsed,
}: {
  sideBarCollapsed: boolean,
  item: { id: string;
    group: any;
    title: string;
    surgeon: string;
    status: string;
    start_time: number;
    late: number;
    end_time: number;
    calendarDetails: string;
}}) => (
  <>
    {!sideBarCollapsed
  && (
  <div className={`my-1 mx-2 p-2 ${styles.item} ${item.status === 'late'
    ? styles.late : (item.status === 'cancelled' ? styles.cancelled : styles.arrived)}`}
  >
    <div>
      <p className={`py-2 ${styles.surgery}`}>
        {item.title}
      </p>
      <p className={styles.surgeon}>
        {item.surgeon}
      </p>
      <p>
        {item.calendarDetails}
      </p>
      <span>
        {item.status}
        {' '}

        {item.status === 'late' && `${item.late} min`}
      </span>
    </div>
  </div>
  )}
    {sideBarCollapsed
  && (
  <div className={`my-3 ${styles.icon}`}>
    <img
      src={scalpelIcon}
      alt="scalpel icon"
    />
  </div>
  )}
  </>
);
