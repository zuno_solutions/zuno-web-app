import React, { useState } from 'react';
import {
  Button, Col, Container, Row, ToggleButton,
} from 'react-bootstrap';
import styles from './ORDDataPage.module.scss';
import hospitalIcon from '../../assets/images/hospital_icon.png';

import { OnboardingTitle } from '../OnboardingTitle/OnboardingTitle';
import InviteForm from '../_forms/InviteForm';
import { InviteValues } from '../../types/forms';
import { invitesService } from '../../_services/invites.service';
import { accountService } from '../../_services/account.service';

interface ORDDataPageProps {
  setOnboardingState: () => void;
}

export const ORDDataPage = ({ setOnboardingState }: ORDDataPageProps) => {
  const [workflow, setWorkflow] = useState<string>('');

  const handleSetWorkflow = (e: React.ChangeEvent<any>): void => {
    setWorkflow(e.target.value);
  };
  const handleFormSubmit = (values: InviteValues): Promise<any> => invitesService.inviteORD(values);

  const logOut = () => {
    accountService.logout();
  };

  return (
    <Container>
      <Row>
        <OnboardingTitle
          image={hospitalIcon}
          assistiveText="The next steps will be to set up to set up the operating room and personnel."
          title="Operating Room Director"
        />
        <Col className="mb-4">
          <p className={styles.desition_flow}>
            Send to Operating Room Director to complete?
          </p>
        </Col>
      </Row>
      <Row className="justify-content-around">
        <Col className="col-md-5">
          <div
            role="group"
            aria-labelledby="my-radio-group"
            className="d-flex justify-content-between"
          >
            <ToggleButton
              className={styles.button}
              key="SendForm"
              id="radio-SendForm"
              type="radio"
              size="lg"
              variant="outline-info"
              name="SendForm"
              value="SendForm"
              checked={workflow === 'SendForm'}
              onChange={(e) => handleSetWorkflow(e)}
            >
              <span className="align-middle">Yes</span>
            </ToggleButton>
            <ToggleButton
              className={styles.button}
              key="CompleteMyself"
              id="radio-CompleteMyself"
              type="radio"
              size="lg"
              variant="outline-info"
              name="CompleteMyself"
              value="CompleteMyself"
              checked={workflow === 'CompleteMyself'}
              onChange={(e) => handleSetWorkflow(e)}
            >
              <span className="align-middle">No</span>
            </ToggleButton>
          </div>
        </Col>
      </Row>
      <Row className="justify-content-center">
        {workflow === 'SendForm' && (
        <>
          <div className="col-md-5 align-self-center px-2 pt-4">
            <p className={`py-2 ${styles.left_text}`}>ORD or Similar</p>
          </div>
          <InviteForm
            setOnboardingState={setOnboardingState}
            handleFormSubmit={handleFormSubmit}
          />
        </>
        )}
        <Col className="col-md-5">
          {workflow === 'CompleteMyself' && (
            <>
              <div className="d-grid gap-2 mt-5">
                <Button variant="info" size="lg" href="/set-account/ord">
                  <span className="align-middle">Save and Continue</span>
                </Button>
              </div>
              <div className="d-grid gap-2">
                <Button onClick={logOut} variant="link">
                  Save and complete later
                </Button>
              </div>
            </>
          )}
        </Col>
      </Row>
    </Container>
  );
};
