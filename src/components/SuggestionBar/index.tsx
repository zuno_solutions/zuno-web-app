/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import AutoSuggest from 'react-autosuggest';

const companies: string[] = [
  'Company1',
  'Company2',
  'Big Corp',
  'Happy Toy Company',
];

const lowerCasedCompanies = companies.map((language) => language.toLowerCase());

export const SuggestionBar = () => {
  const [value, setValue] = useState('');
  const [suggestions, setSuggestions] = useState<string[]>([]);

  function getSuggestions(valueSearched: string): string[] {
    return lowerCasedCompanies.filter((language) => language.startsWith(valueSearched.trim().toLowerCase()));
  }

  return (
    <div>
      <AutoSuggest
        suggestions={suggestions}
        onSuggestionsClearRequested={() => setSuggestions([])}
        onSuggestionsFetchRequested={({ value }) => {
          setValue(value);
          setSuggestions(getSuggestions(value));
        }}
        onSuggestionSelected={(_, { suggestionValue }) => console.log(`Selected: ${suggestionValue}`)}
        getSuggestionValue={(suggestion) => suggestion}
        renderSuggestion={(suggestion) => <span>{suggestion}</span>}
        inputProps={{
          placeholder: "Type 'c'",
          value,
          onChange: (_, { newValue, method }) => {
            setValue(newValue);
          },
        }}
        highlightFirstSuggestion
      />
    </div>
  );
};
