/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import moment from 'moment';
import React from 'react';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';
import styles from './RoomsConsole.module.scss';

interface RoomsConsoleProps {
  operationRooms: IOperationRoom[];
  surgeries: ISurgeryItem[];
  handleRoomSelection: (room: number | string) => void
  selectedRoom: number | string | undefined,
}

export const RoomsConsole = ({
  operationRooms, surgeries, handleRoomSelection, selectedRoom,
}: RoomsConsoleProps) => (
  <div className={`pt-4 ${styles.container}`}>
    <h5>Surgeries per Room</h5>
    {operationRooms.map((room: IOperationRoom, index) => {
      const bodyColor = selectedRoom === Number(room.id) ? room.color : '#e5e5ea';
      const bodyText = selectedRoom === Number(room.id) ? 'white' : '#231f20';
      return (
        <button key={room.id} type="button" value={room.id} onClick={() => handleRoomSelection(room.id)} className={styles.button}>
          <div
            className={`className="d-flex justify-content-between" ${styles.room}`}
            style={
              { background: room.color }
            }
          >
            <p>
              Room
              {' '}
              {room.id.toString().padStart(2, '0')}
            </p>
          </div>
          <div
            className={styles.surgeries}
            style={
              {
                background: bodyColor,
                color: bodyText,
              }
            }
          >
            {surgeries.filter((surgery) => surgery.group.toString() === room.id && moment(surgery.start_time).isSame(new Date(), 'day')).length}
          </div>
        </button>
      );
    })}
  </div>
);
