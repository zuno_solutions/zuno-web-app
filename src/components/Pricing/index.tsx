import React from 'react';
import { Col, Row } from 'react-bootstrap';
import styles from './Pricing.module.scss';

export const Pricing = () => (
  <Row className="shadow rounded-content my-4 mx-0 align-items-center">
    <Col className="px-3 py-4">
      <div className={styles.offer__price}>$15,000 Annually</div>
    </Col>
    <Col className={`border-start px-3 py-4 ${styles.offer__content}`} md={8}>
      <p>which includes</p>
      <ul>
        <li>Zunō</li>
        <li>OR Intelligence Suite</li>
      </ul>
      <a href="/#">More Info</a>
    </Col>
  </Row>
);
