/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import moment from 'moment';
import React from 'react';
import { Col } from 'react-bootstrap';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';
import styles from './ProcedureConsole.module.scss';

interface ProcedureConsoleProps {
  item: ISurgeryItem;
}

export const ProcedureConsole = ({ item }: ProcedureConsoleProps) => (
  <Col
    className={`d-flex justify-content-between flex-column pt-4 pb-5 ${styles.container}`}
  >
    <h5>Procedure Type</h5>
    <p className={styles.subtitle}>{item.title}</p>
    <p>
      Procedure Duration:
      {' '}
      {item.duration}
    </p>
    <h5>Surgeon</h5>
    <p className={styles.subtitle}>{item.surgeon}</p>
    <p
      className={styles.status}
      style={{
        color: item.surgeonStatus === 'late' ? '#FFCC00' : '#34C759',
      }}
    >
      {item.surgeonStatus}
      {' '}
      {item.surgeonStatus === 'late' && (
      <span>
        {item.late}
        {' '}
        MIN
      </span>
      )}
    </p>
    <h5>Anesthesiologist</h5>

    <p className={styles.subtitle}>{item.anesthesiologist}</p>
    <p
      className={styles.status}
      style={{
        color: item.anesthesiologistStatus === 'late' ? '#FFCC00' : '#34C759',
      }}
    >
      {item.anesthesiologistStatus}
      {' '}
      {item.anesthesiologistStatus === 'late' && (
      <span>
        {item.late}
        {' '}
        min
      </span>
      )}
    </p>
  </Col>
);
