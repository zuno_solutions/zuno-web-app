import React, { useState } from 'react';
import {
  Col, Container, Row, ToggleButton,
} from 'react-bootstrap';
import styles from './ITDataPage.module.scss';
import hospitalIcon from '../../assets/images/hospital_icon.png';

import { OnboardingTitle } from '../OnboardingTitle/OnboardingTitle';
import InviteForm from '../_forms/InviteForm';
import { InviteValues } from '../../types/forms';
import { ITData } from '../ITData';
import { invitesService } from '../../_services/invites.service';

interface ITDataPageProps {
  setOnboardingState: () => void;
}

export const ITDataPage = ({ setOnboardingState }: ITDataPageProps) => {
  const [workflow, setWorkflow] = useState<string>('');

  const handleSetWorkflow = (e: React.ChangeEvent<any>): void => {
    setWorkflow(e.target.value);
  };
  const handleFormSubmit = (values: InviteValues): Promise<any> => invitesService.inviteIT(values);

  return (
    <Container>
      <Row>
        <OnboardingTitle
          image={hospitalIcon}
          assistiveText="Link Zunō to your EMR"
          title="IT"
        />
        <Col className="mb-4">
          <p className={styles.desition_flow}>
            Send credential request to IT admin?
          </p>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col className="col-md-5 mt-2 mb-4">
          <div
            role="group"
            aria-labelledby="my-radio-group"
            className="d-flex justify-content-around"
          >
            <ToggleButton
              className={styles.button}
              id="radio-SendForm"
              key="SendForm"
              type="radio"
              size="lg"
              variant="outline-info"
              name="SendForm"
              value="SendForm"
              checked={workflow === 'SendForm'}
              onChange={(e) => handleSetWorkflow(e)}
            >
              <div>Yes</div>
            </ToggleButton>
            <ToggleButton
              className={styles.button}
              key="CompleteMyself"
              id="radio-CompleteMyself"
              type="radio"
              size="lg"
              variant="outline-info"
              name="CompleteMyself"
              value="CompleteMyself"
              checked={workflow === 'CompleteMyself'}
              onChange={(e) => handleSetWorkflow(e)}
            >
              <span className="align-middle">No</span>
            </ToggleButton>
          </div>
        </Col>
      </Row>
      {workflow === 'SendForm' && (
        <InviteForm
          setOnboardingState={setOnboardingState}
          handleFormSubmit={handleFormSubmit}
        />
      )}
      {workflow === 'CompleteMyself' && <ITData />}
    </Container>
  );
};
