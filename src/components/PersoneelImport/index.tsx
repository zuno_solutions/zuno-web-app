/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import Dropzone, {
  IDropzoneProps,
} from 'react-dropzone-uploader';
import { getDroppedOrSelectedFiles } from 'html5-file-selector';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';
import styles from './PersoneelImport.module.scss';
import personeelImport from '../../assets/images/personeel_import.png';
import { OnboardingTitle } from '../OnboardingTitle/OnboardingTitle';
import { UploadFileInput } from '../UploadFileInput';
import { PreviewCustomizedComponent } from '../PreviewCustomizedComponent';
import { personnelService } from '../../_services/personnel.service';
// import { PersonnelTemplateFile } from '../PersonnelTemplateFile/PersonnelTemplateFile';

interface PersoneelImportProps {
  updateOnboardingStateFromForm: () => void;
}

export const PersoneelImport = ({
  updateOnboardingStateFromForm,
}: PersoneelImportProps) => {
  const excelFields = [
    'Position',
    'First name',
    'Last name',
    'Phone number',
    'Email',
    'Manufacturer',
  ];

  const user = JSON.parse(localStorage.getItem('user') || '{}');

  const getUploadParams: IDropzoneProps['getUploadParams'] = () => ({
    url: `${process.env.REACT_APP_ZUNO_API_URL}/import-personnel`,
    headers: { Authorization: `Bearer ${user.access_token}` },
  });

  const handleChangeStatus: IDropzoneProps['onChangeStatus'] = (
    { meta },
    status,
  ) => {
    console.log(status, meta);
  };

  const handleSubmit: IDropzoneProps['onSubmit'] = (files, allFiles) => {
    allFiles.forEach((f) => f.remove());
  };

  const handleDownload = () => {
    personnelService.getTemplateFile();
  };

  const getFilesFromEvent = (e: any): File[] | Promise<File[]> => new Promise((resolve) => {
    getDroppedOrSelectedFiles(e).then((chosenFiles: any[]) => {
      resolve(chosenFiles.map((f) => f.fileObject));
    });
  });

  return (
    <>
      <OnboardingTitle
        image={personeelImport}
        assistiveText="Upload the staff list and we will synchronize it with your ZUNO and EPIC account."
        title="Personnel import"
      />
      <Col md={{ span: 8, offset: 2 }} className="shadow p-5 mb-5 rounded">
        <Row>
          <Col md={4}>
            <Row>
              <p className="mb-3 desition_flow_right">Excel Fields: </p>
            </Row>
            {excelFields.map((field) => (
              <p key={field} className={`my-1 py-2 px-3 ${styles.field_pills}`}>
                {field}
              </p>
            ))}

            <p className={`pt-3 ${styles.fields_note}`}>
              Not all fields are required.
            </p>
            <Row>
              {/* <PersonnelTemplateFile /> */}
              <Button variant="link" className={`pt-3 ${styles.download_sample}`} type="button" onClick={handleDownload}>
                <FontAwesomeIcon
                  size="lg"
                  className={styles.icon}
                  icon={faQuestionCircle}
                />
                {' '}
                Download excel template
              </Button>
            </Row>
          </Col>
          <Col>
            <section className={styles.container}>
              <Dropzone
                getUploadParams={getUploadParams}
                onChangeStatus={handleChangeStatus}
                onSubmit={handleSubmit}
                accept=".xls, .xlsx, .csv"
                InputComponent={UploadFileInput}
                PreviewComponent={PreviewCustomizedComponent}
                getFilesFromEvent={getFilesFromEvent}
                // @ts-ignore
                SubmitButtonComponent={null}
              />
            </section>
          </Col>
        </Row>
      </Col>
      <Row className="justify-content-center mt-2">
        <Col md={5} className="d-grid gap-2">
          <Button
            variant="info"
            size="lg"
            onClick={updateOnboardingStateFromForm}
          >
            Continue
          </Button>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md={5} className="d-grid gap-2">
          <Button variant="link" size="lg">
            Skip to Next Step
          </Button>
        </Col>
      </Row>
    </>
  );
};
