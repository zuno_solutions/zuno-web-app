/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-unused-vars */
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { IOperationRoom, ISurgeryItem } from '../../pages/Dashboard';
import styles from './TodaySurgeriesStatus.module.scss';

interface TodaySurgeriesStatusProps {
  surgeries: ISurgeryItem[];

}

export const TodaySurgeriesStatus = ({ surgeries }: TodaySurgeriesStatusProps) => {
  const totalSurgeries = surgeries.length;
  const delayedSurgeries = surgeries.filter((surgery) => surgery.status === 'delayed').length;
  const onTimeSurgeries = surgeries.filter((surgery) => surgery.status === 'on time').length;
  const rescheduledSurgeries = surgeries.filter((surgery) => surgery.status === 'rescheduled').length;
  return (
    <>
      <Row>
        <Col>
          <div className={styles.total}>
            <p className={styles.total_number}>
              {totalSurgeries}
            </p>
            <p>
              surgeries
            </p>
          </div>

        </Col>
        <Col className={`${styles.surgeries}`}>
          <p className="p-2">
            Surgeries Status
          </p>
          <Row className={`pl-2 ${styles.item}`}>
            <Row>
              <Col className="d-flex justify-content-between">
                <span>
                  ON TIME
                </span>
                <span className={`${styles.onTime}`}>
                  {onTimeSurgeries}
                </span>
              </Col>
            </Row>
            <Row>
              <Col className="d-flex justify-content-between">
                <span>
                  Delayed
                </span>
                <span className={`${styles.delayed}`}>
                  {delayedSurgeries}
                </span>
              </Col>
            </Row>
            <Row>
              <Col className="d-flex justify-content-between">
                <span>
                  NEED ATTENTION
                </span>
                <span className={`${styles.attention}`}>
                  {rescheduledSurgeries}
                </span>
              </Col>
            </Row>
          </Row>
        </Col>
      </Row>

    </>
  );
};
