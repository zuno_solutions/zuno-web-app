export enum Role {
  VP = 1,
  ACCOUNTANT = 2,
  IT_DIRECTOR = 3,
  ORD = 4,
  VENDOR = 5,
  SURGEON = 6,
  ANESTHESIOLOGIST = 7,
  TRAY_VENDOR = 8,
}

export const roleText = {
  1: 'VP',
  2: 'Accountant',
  3: 'IT director',
  4: 'ORD',
  5: 'Vendor',
  6: 'Surgeon',
  7: 'Anesthesiologist',
  8: 'Tray Vendor',
};

export const roles = [
  {
    value: 1,
    name: 'VP',
  },
  { value: 2, name: 'Accountant' },
  { value: 3, name: 'IT director' },
  { value: 4, name: 'ORD' },
  { value: 5, name: 'Vendor' },
  { value: 6, name: 'Surgeon' },
  { value: 7, name: 'Anesthesiologist' },
  { value: 8, name: 'Tray Vendor' },
];
