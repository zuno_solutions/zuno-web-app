import { accountService } from '../_services/account.service';

function handleResponse(response: {
  text: () => Promise<any>;
  ok: any;
  status: number;
  statusText: any;
}) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);

    if (!response.ok) {
      if ([401, 403].includes(response.status) && accountService.userValue) {
        // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
        accountService.logout();
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}

// helper functions

function authHeader(url: string): HeadersInit {
  // return auth header with jwt if user is logged in and request is to the api url
  const user = JSON.parse(localStorage.getItem('user') || '{}');
  const isLoggedIn = user && user.access_token;
  const isApiUrl = url.startsWith(
    process.env.REACT_APP_ZUNO_API_URL || 'http://localhost:3030',
  );
  if (isLoggedIn && isApiUrl) {
    return { Authorization: `Bearer ${user.access_token}` };
  }
  return {};
}

function get(url: string) {
  const requestOptions: RequestInit = {
    method: 'GET',
    headers: authHeader(url),
  };
  return fetch(url, requestOptions).then(handleResponse);
}

function post(url: string, body: any) {
  const requestOptions: RequestInit = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', ...authHeader(url) },
    // credentials: 'include',
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions).then(handleResponse);
}

function put(url: string, body: any) {
  const requestOptions: RequestInit = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json', ...authHeader(url) },
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions).then(handleResponse);
}

// prefixed with underscored because delete is a reserved word in javascript
function httpDelete(url: string, body?:any) {
  const requestOptions: RequestInit = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json', ...authHeader(url) },
    // headers: authHeader(url),
    body: JSON.stringify(body),
  };
  return fetch(url, requestOptions).then(handleResponse);
}

export const fetchWrapper = {
  get,
  post,
  put,
  delete: httpDelete,
};
