import { InviteValues } from '../components/_forms/InviteForm';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

import { Role } from '../_helpers/role';

export interface IUser {
  jwtToken: string | null;
  role: Role;
  state: number;
}

const baseUrl = `${process.env.REACT_APP_ZUNO_API_URL}/invites`;

function inviteAccountant(values: InviteValues) {
  return fetchWrapper.post(`${baseUrl}/invite-accountant`, {
    email: values.email,
    first_name: values.firstName,
    last_name: values.lastName,
    phone: values.mainPhone,
  });
}

function inviteIT(values: InviteValues) {
  return fetchWrapper.post(`${baseUrl}/invite-IT`, {
    email: values.email,
    first_name: values.firstName,
    last_name: values.lastName,
  });
}

function inviteORD(values: InviteValues) {
  return fetchWrapper.post(`${baseUrl}/invite-ORD`, {
    email: values.email,
    first_name: values.firstName,
    last_name: values.lastName,
  });
}

export const invitesService = {
  inviteAccountant,
  inviteIT,
  inviteORD,
};
