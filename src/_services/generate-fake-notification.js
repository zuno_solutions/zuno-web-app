/* eslint-disable no-nested-ternary */
/* eslint-disable no-plusplus */
import faker from 'faker';
// import randomColor from 'randomcolor';
import moment from 'moment';

const isToday = (someDate) => {
  const tomorrow = moment().add(1, 'days');
  if (someDate.isSame(new Date(), 'day')) {
    return 'Today';
  } if (someDate.isSame(tomorrow, 'day')) { return 'Tomorrow'; } return someDate.format('dddd');
};

const durationAsString = (duration) => {
  // Get Days
  const days = Math.floor(duration.asDays()); // .asDays returns float but we are interested in full days only
  const daysFormatted = days ? `${days}d ` : ''; // if no full days then do not display it at all
  // Get Hours
  const hours = duration.hours();
  const hoursFormatted = `${hours}h `;
  // Get Minutes
  const minutes = duration.minutes();
  const minutesFormatted = `${minutes}m`;

  return [daysFormatted, hoursFormatted, minutesFormatted].join('');
};

const getTimeFromDate = (startTime, endTime) => {
  const startMoment = moment(startTime);
  const endMoment = moment(endTime);
  const hoursDuration = moment.duration(endMoment.diff(startMoment));
  return `${isToday(startMoment)} ${startMoment.format('LT')} - ${endMoment.format('LT')} (${durationAsString(hoursDuration)})`;
};

export default function (groupCount = 7, itemCount = 10, daysInPast = 30) {
  // const randomSeed = Math.floor(Math.random() * 1000);
  const groups = [];
  for (let i = 0; i < groupCount; i++) {
    groups.push({
      id: `${i + 1}`,
      title: `Room ${(i + 1).toString().padStart(2, '0')} `,
      rightTitle: faker.name.lastName(),
      height: 100,
    });
  }

  const status = ['late', 'arrived', 'cancelled'];
  let items = [];
  for (let i = 0; i < itemCount; i++) {
    const startDate = faker.date.recent(daysInPast).valueOf() + (daysInPast * 0.3) * 86400 * 1000;
    const startValue = Math.floor(moment(startDate).valueOf() / 10000000) * 10000000;
    const endValue = moment(startDate + faker.datatype.number({ min: 2, max: 20 }) * 15 * 60 * 1000).valueOf();
    const group = faker.datatype.number({ min: 1, max: groups.length });
    items.push({
      id: `${i}`,
      group,
      title: `Room ${(group).toString().padStart(2, '0')} - ${faker.lorem.word()}`,
      surgeon: `Dr. ${faker.name.firstName()} ${faker.name.lastName()}`,
      status: status[Math.floor(Math.random() * status.length)],
      start_time: startValue,
      end_time: endValue,
      late: Math.floor(Math.random() * 50),
      calendarDetails: getTimeFromDate(startValue, endValue),
      // itemProps: {
      //   'data-tip': faker.hacker.phrase(),
      // },
    });
  }

  items = items.sort((a, b) => b - a);

  return { groups, items };
}
