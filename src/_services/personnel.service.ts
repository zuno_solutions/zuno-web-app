/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable camelcase */
import qs from 'qs';
import axios from 'axios';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

export const perPage = 15;
const baseUrl = `${process.env.REACT_APP_ZUNO_API_URL}`;
const user = JSON.parse(localStorage.getItem('user') || '{}');

export interface IPersonnel {
  email: string;
  first_name: string;
  id: number;
  last_name: string;
  manufacturers: IManufacturer[];
  phone: string;
  role: number;
  surgeons: ISurgeon[];
}

export interface IManufacturer {
  id: number;
  manufacturer: string;
  updated_at?: Date;
  created_at?: Date;
}

export interface ISurgeon {
  updated_at?: Date;
  created_at?: Date;
  email: string;
  hospital_id: number;
  first_name: string;
  id: number;
  last_name: string;
  phone: string;
  role: number;
  surgeons: ISurgeon[];
}

export interface IQuery {
  offset?: number;
  limit?: number;
  search?: string;
  sort_by?: string | string[];
}

function getPersonnel(query: IQuery = {}) {
  const { limit = 15 } = query;
  return fetchWrapper
    .get(`${baseUrl}/import-personnel?${qs.stringify({ ...query, limit })}`)
    .then((response) => response.data);
}

// function getTemplateFile() {
//   return fetchWrapper
//     .get(`${baseUrl}/import-personnel/download-sample-file`)
//     .then((response) => response.blob);
// }

function getTemplateFile() {
  return axios.get(`${baseUrl}/import-personnel/download-sample-file`, {
    method: 'GET',
    responseType: 'blob', // important
    headers: { Authorization: `Bearer ${user.access_token}` },
  }).then((response: any) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'zuno_template.xls');
    document.body.appendChild(link);
    link.click();
  });
}

function getPersonnelById(id: number) {
  return fetchWrapper
    .get(`${baseUrl}/import-personnel/${id}`)
    .then((response) => response.data);
}

function getRawPersonnelById(id: number) {
  return fetchWrapper
    .get(`${baseUrl}/personnel/${id}`)
    .then((response) => response.data);
}

function getRawPersonnel(query: IQuery = {}) {
  const { limit = 15 } = query;
  return fetchWrapper
    .get(`${baseUrl}/personnel?${qs.stringify({ ...query, limit })}`)
    .then((response) => response.data);
}

function searchPersonnel({
  offset = 1,
  limit = perPage,
  query,
}: {
  offset?: number;
  limit?: number;
  query: string;
}) {
  return fetchWrapper
    .get(`${baseUrl}/personnel/offset/${offset}/limit/${limit}?search=${query}`)
    .then((response) => response.data);
}

function editPersonnel(
  personnelRow: IPersonnel,
  dataField: string,
  newValue: any,
) {
  const { id } = personnelRow;
  const body = dataField === 'role'
    ? { [dataField]: Number(newValue) }
    : { [dataField]: newValue };
  return fetchWrapper
    .put(`${baseUrl}/import-personnel/${id}/`, body)
    .then((response) => response);
}

function editRawPersonnel(
  id: number,
  personnel: any,
  manufacturersIds?: number[],
  surgeonsIds?: number[],
) {
  const body = {
    role: Number(personnel.position),
    first_name: personnel.firstName,
    last_name: personnel.lastName,
    email: personnel.email,
    phone: personnel.mainPhoneNumber,
    ...(manufacturersIds
      && manufacturersIds.length > 0 && { manufacturers: manufacturersIds }),
    ...(surgeonsIds
        && surgeonsIds.length > 0 && { surgeons: surgeonsIds }),

    // ...surgeonsIds.length > 0 && { surgeons: surgeonsIds },
  };
  return fetchWrapper
    .put(`${baseUrl}/personnel/${id}/`, body)
    .then((response) => response);
}

function deletePersonnel(personnelId: number | number[]) {
  const body = {
    personnel: Array.isArray(personnelId) ? personnelId : [personnelId],
  };
  return fetchWrapper
    .delete(`${baseUrl}/personnel`, body)
    .then((response) => response);
}

function deleteImportedPersonnel(personnelId: number | number[]) {
  const body = {
    personnel: Array.isArray(personnelId) ? personnelId : [personnelId],
  };
  return fetchWrapper
    .delete(`${baseUrl}/import-personnel`, body)
    .then((response) => response);
}

function invitePersonnel(personnelIds: number[], selectedTime: number) {
  const body = {
    personnels: personnelIds,
    scheduled_at: selectedTime,
  };
  return fetchWrapper
    .post(`${baseUrl}/personnel/invite-personnel`, body)
    .then((response) => response);
}

function addManufacturer(
  manufacturer: string,
) {
  const body = { manufacturer };
  return fetchWrapper
    .post(`${baseUrl}/manufacturers`, body)
    .then((response) => response);
}

function addPersonnel(
  personnel: any,
  manufacturersIds?: number[],
  surgeonsIds?: number[],
) {
  const body = {
    role: Number(personnel.position),
    first_name: personnel.firstName,
    last_name: personnel.lastName,
    email: personnel.email,
    phone: personnel.mainPhoneNumber,
    ...(manufacturersIds
      && manufacturersIds.length > 0 && { manufacturers: manufacturersIds }),
    ...(surgeonsIds
        && surgeonsIds.length > 0 && { surgeons: surgeonsIds }),
    // ...surgeonsIds.length > 0 && { surgeons: surgeonsIds },
  };
  return fetchWrapper
    .post(`${baseUrl}/import-personnel/add-personnel`, body)
    .then((response) => response);
}

export const personnelService = {
  getPersonnel,
  getRawPersonnelById,
  getRawPersonnel,
  getPersonnelById,
  searchPersonnel,
  editPersonnel,
  editRawPersonnel,
  deletePersonnel,
  deleteImportedPersonnel,
  invitePersonnel,
  addPersonnel,
  addManufacturer,
  getTemplateFile,
};
