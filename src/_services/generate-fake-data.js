/* eslint-disable no-unused-expressions */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-plusplus */
import faker from 'faker';
// import randomColor from 'randomcolor';
import moment from 'moment';
import { roomConsoleStyles } from '../utils/roomStyles';

let n = -1;

const isToday = (someDate: Moment) => {
  const tomorrow = moment().add(1, 'days');
  if (someDate.isSame(new Date(), 'day')) {
    return 'Today';
  }
  if (someDate.isSame(tomorrow, 'day')) {
    return 'Tomorrow';
  }
  return someDate.format('dddd');
};

const durationAsString = (duration: any) => {
  // Get Days
  const days = Math.floor(duration.asDays()); // .asDays returns float but we are interested in full days only
  const daysFormatted = days ? `${days}d ` : ''; // if no full days then do not display it at all
  // Get Hours
  const hours = duration.hours();
  const hoursFormatted = `${hours}h `;
  // Get Minutes
  const minutes = duration.minutes();
  const minutesFormatted = `${minutes}m`;

  return [daysFormatted, hoursFormatted, minutesFormatted].join('');
};

const getTimeFromDate = (startTime: number, endTime: number) => {
  const startMoment = moment(startTime);
  const endMoment = moment(endTime);
  const hoursDuration = moment.duration(endMoment.diff(startMoment));
  return `${isToday(startMoment)} ${startMoment.format(
    'LT',
  )} - ${endMoment.format('LT')} (${durationAsString(hoursDuration)})`;
};

export default function (groupCount = 30, itemCount = 1000, daysInPast = 30) {
  // const randomSeed = Math.floor(Math.random() * 1000);
  const groups = [];
  for (let i = 0; i < groupCount; i++) {
    n > 6 ? (n = 0) : (n += 1);
    groups.push({
      id: `${i + 1}`,
      title: `Room ${(i + 1).toString().padStart(2, '0')} `,
      rightTitle: faker.name.lastName(),
      color: roomConsoleStyles[n].color,
      height: 100,
    });
  }

  const personnelStatus = ['late', 'on time'];
  const status = ['delayed', 'on time', 'rescheduled'];
  const personnel = ['Surgeon', 'Anesthesiology', 'Vendor'];
  const genders = ['male', 'female'];

  let items = [];
  for (let i = 0; i < itemCount; i++) {
    const startDate = faker.date.recent(daysInPast).valueOf() + daysInPast * 0.3 * 86400 * 1000;
    const startValue = Math.floor(moment(startDate).valueOf() / 10000000) * 10000000;
    const endValue = moment(
      startDate + faker.datatype.number({ min: 2, max: 20 }) * 15 * 60 * 1000,
    ).valueOf();
    const group = faker.datatype.number({ min: 1, max: groups.length });
    items.push({
      id: `${i}`,
      group,
      title: `Room ${group
        .toString()
        .padStart(2, '0')} - ${faker.lorem.sentence()}`,
      surgeon: `Dr. ${faker.name.firstName()} ${faker.name.lastName()}`,
      anesthesiologist: `Dr. ${faker.name.firstName()} ${faker.name.lastName()}`,
      anesthesiologistStatus: personnelStatus[Math.floor(Math.random() * personnelStatus.length)],
      surgeonStatus: personnelStatus[Math.floor(Math.random() * personnelStatus.length)],
      vendor: faker.lorem.words(),
      manufacturer: faker.lorem.word(),
      vendorStatus: personnelStatus[Math.floor(Math.random() * personnelStatus.length)],
      patient: {
        name: `${faker.name.firstName()} ${faker.name.lastName()}`,
        emergencyContact: `${faker.name.firstName()} ${faker.name.lastName()}`,
        age: faker.datatype.number({ min: 1, max: 90 }),
        gender: faker.random.arrayElement(genders),
      },
      selectedRoom: false,
      late: faker.datatype.number({ min: 2, max: 59 }),
      trays: faker.datatype.number({ min: 1, max: 20 }),
      traysStatus: personnelStatus[Math.floor(Math.random() * personnelStatus.length)],
      status: status[Math.floor(Math.random() * status.length)],
      personnel: personnel[Math.floor(Math.random() * personnel.length)],
      start_time: startValue,
      end_time: endValue,
      calendarDetails: getTimeFromDate(startValue, endValue),
      duration: durationAsString(moment.duration(moment(endValue).diff(moment(startValue)))),
      canMove: startValue > new Date().getTime(),
      canResize:
        startValue > new Date().getTime()
          ? endValue > new Date().getTime()
            ? 'both'
            : 'left'
          : endValue > new Date().getTime()
            ? 'right'
            : false,
      className:
        moment(startDate).day() === 6 || moment(startDate).day() === 0
          ? 'item-weekend'
          : '',
      // itemProps: {
      //   'data-tip': faker.hacker.phrase(),
      // },
    });
  }

  items = items.sort((a, b) => b - a);

  return { groups, items };
}
