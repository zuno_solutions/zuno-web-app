/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable camelcase */
import { fetchWrapper } from '../_helpers/fetch-wrapper';

const baseUrl = `${process.env.REACT_APP_ZUNO_API_URL}`;

export const suggestionsLenght = 7;
function getSuggestedSurgeons({
  valueSearched,
  offset = 1,
  limit = suggestionsLenght,
}: { offset?: number; limit?: number; valueSearched?: string } = {}) {
  return fetchWrapper
    .get(
      `${baseUrl}/surgeons/offset/${offset}/limit/${limit}?surgeon=${valueSearched}`,
    )
    .then((response) => response.data);
}

export const surgeonService = {
  getSuggestedSurgeons,
};
