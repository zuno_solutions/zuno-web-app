import { HospitalFormValues } from '../components/_forms/HospitalData';
import { PersonalFormValues } from '../components/_forms/PersonalData';
import { IRoomSetting } from '../components/_onboardingORD/ORDRoomsNaming';
import { ArrivalTimesValues } from '../components/_onboardingORD/TimesForm';
import { fetchWrapper } from '../_helpers/fetch-wrapper';
import { Role } from '../_helpers/role';
import { hospitalSubject } from './account.service';

export interface IUser {
  jwtToken: string | null;
  role: Role;
  state: number;
}

export type RoomsPerFloor = {
  floor: number;
  rooms: number[];
};

export interface FloorsPerHospital extends Array<RoomsPerFloor> {}

const baseUrl = `${process.env.REACT_APP_ZUNO_API_URL}`;

function hospitals(values: HospitalFormValues) {
  return fetchWrapper
    .post(`${baseUrl}/hospitals`, {
      facility_legal_name: values.facilityLegalName,
      facility_name: values.facilityName,
      phone: values.mainPhone,
      physical_address: values.phisicalAddress,
      operating_room_floors: Number(values.orNumber),
    })
    .then((response) => {
      // publish user to subscribers and start timer to refresh token
      localStorage.setItem(
        'hospital',
        JSON.stringify(response.data.hospital || {}),
      );
      // startRefreshTokenTimer();
      return response;
    });
}

function personalData(values: PersonalFormValues) {
  return fetchWrapper
    .put(`${baseUrl}/users/personal-info`, {
      first_name: values.firstName,
      last_name: values.lastName,
      title: values.jobTitle,
      phone: values.mainPhone,
      mobile: values.mobilePhone,
    })
    .then((response) => {
      // publish user to subscribers and start timer to refresh token
      localStorage.setItem('user', JSON.stringify(response.data.user));
      localStorage.setItem(
        'hospital',
        JSON.stringify(response.data.hospital || {}),
      );
      // startRefreshTokenTimer();
      return response;
    });
}

function addRooms(roomsArray: number[]) {
  const body = roomsArray.map((roomsNumber, index) => ({
    floor_no: index + 1,
    rooms: Array.from({ length: roomsNumber || 0 }, (_, i) => i + 1),
  }));

  return fetchWrapper
    .post(`${baseUrl}/rooms/add-rooms`, body)
    .then((response) => {
      // publish user to subscribers and start timer to refresh token
      hospitalSubject.next(response.data.hospital);
      localStorage.setItem(
        'hospital',
        JSON.stringify(response.data.hospital || {}),
      );

      // startRefreshTokenTimer();
      return response;
    });
}

// eslint-disable-next-line camelcase
function addRoomsInfo(body: { floor_no: number; rooms: IRoomSetting[] }[]) {
  return fetchWrapper
    .put(`${baseUrl}/rooms/update-room-info`, body)
    .then((response) => response);
}

function createTimeline(values: ArrivalTimesValues) {
  const body = {
    vendor: {
      desired_arrival_time: values.vendorBeforeSurgery,
      critical_arrival_time: values.vendorGracePeriod,
      grace_period: values.vendorStartTime,
    },
    tray_vendor: {
      desired_arrival_time: values.trayDeliveryTime,
      grace_period: values.vendorTrayGracePeriod,
    },
    surgeon: {
      desired_arrival_time: values.surgeonBeforeSurgery,
      critical_arrival_time: values.surgeonStartTime,
      grace_period: values.surgeonGracePeriod,
    },
    anesthesiologist: {
      desired_arrival_time: values.anesthesiologistBeforeSurgery,
      critical_arrival_time: values.anesthesiologistStartTime,
      grace_period: values.anesthesiologistGracePeriod,
    },
  };

  return fetchWrapper.post(`${baseUrl}/timelines`, body);
}

function getRoomsAtFloor(floorId: number) {
  return fetchWrapper
    .get(`${baseUrl}/rooms/floor/${floorId}`)
    .then((response) => {
      // publish user to subscribers and start timer to refresh token
      hospitalSubject.next(response.data.hospital);
      localStorage.setItem(
        'hospital',
        JSON.stringify(response.data.hospital || {}),
      );

      // startRefreshTokenTimer();
      return response;
    });
}

export const onboardingService = {
  hospitals,
  personalData,
  addRooms,
  addRoomsInfo,
  getRoomsAtFloor,
  createTimeline,
};
