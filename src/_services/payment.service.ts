import { fetchWrapper } from '../_helpers/fetch-wrapper';
import { Role } from '../_helpers/role';

export interface IUser {
  jwtToken: string | null;
  role: Role;
  state: number;
}

const baseUrl = `${process.env.REACT_APP_ZUNO_API_URL}`;

function createPlanSubscriptionsDB(values: {
  cardToken: string;
  subscriptionId: string;
}) {
  return fetchWrapper.post(`${baseUrl}/payment-sources`, {
    payment_source_type: 'card',
    payment_source_token: values.cardToken,
    subscription_id: values.subscriptionId,
  });
}

export const paymentService = {
  createPlanSubscriptionsDB,
};
