import { BehaviorSubject } from 'rxjs';

import { fetchWrapper } from '../_helpers/fetch-wrapper';
import { history } from '../_helpers/history';
import { Role } from '../_helpers/role';

export enum OnboardingTypes {
  HospitalData = 'hospital-data',
  PersonalData = 'personal-data',
  PaymentData = 'payment-data',
  ORDData = 'ord-data',
  ITData = 'it-data',
}

export interface IUser {
  jwtToken: string | null;
  role: Role;
  state: number;
  // eslint-disable-next-line camelcase
  first_name: string;
  // eslint-disable-next-line camelcase
  last_name: string
}

export interface IHospital {
  id: number;
  state: number;
  // eslint-disable-next-line camelcase
  operating_room_floors: number;
}

export const roomsSubject = new BehaviorSubject<any>(null);
export const userSubject = new BehaviorSubject<IUser | null>(null);
export const emailSubject = new BehaviorSubject<string | null>(null);
export const hospitalSubject = new BehaviorSubject<IHospital | null>(null);
export const baseUrl = `${process.env.REACT_APP_ZUNO_API_URL}`;

function register({ email }: { email: string }) {
  emailSubject.next(email);
  return fetchWrapper.post(`${baseUrl}/check-email-availability`, { email });
}

// eslint-disable-next-line camelcase
function verifyEmail(confirmation_token: string | string[] | null) {
  return fetchWrapper
    .post(`${baseUrl}/verify-email`, { confirmation_token })
    .then((user) => {
      // publish user to subscribers and start timer to refresh token
      emailSubject.next(user.data.email);
    });
}

function signup({
  email,
  password,
}: {
  email: string | null;
  password: string;
}) {
  return fetchWrapper.post(`${baseUrl}/sign-up`, { email, password });
}

function login(email: string, password: string) {
  emailSubject.next(email);
  return fetchWrapper.post(`${baseUrl}/login`, { email, password });
}

function resendOTP() {
  return fetchWrapper
    .post(`${baseUrl}/resend-otp`, {
      email: emailSubject.value,
    })
    .then((response) => response);
}

function verifyOTP(otp: string | null) {
  return fetchWrapper
    .post(`${baseUrl}/verify-otp`, {
      email: emailSubject.value,
      otp: Number(otp),
    })
    .then((response) => {
      // publish user to subscribers and start timer to refresh token
      userSubject.next(response.data.user);
      hospitalSubject.next(response.data.hospital);
      localStorage.setItem('user', JSON.stringify(response.data.user));
      localStorage.setItem(
        'hospital',
        JSON.stringify(response.data.hospital || {}),
      );
      emailSubject.next(null);
      // startRefreshTokenTimer();
      return response;
    });
}

function forgotPassword({ email }: { email: string }) {
  return fetchWrapper.post(`${baseUrl}/forget-password`, { email });
}

function validateResetToken(token: string) {
  return fetchWrapper.post(`${baseUrl}/validate-reset-token`, { token });
}

function resetPassword({
  token,
  password,
}: {
  token: string;
  password: string;
}) {
  return fetchWrapper.put(`${baseUrl}/reset-password`, {
    forget_password_token: token,
    password,
  });
}

function getAll() {
  return fetchWrapper.get(baseUrl);
}

function getById(id: number) {
  return fetchWrapper.get(`${baseUrl}/${id}`);
}

function getUserData() {
  return fetchWrapper.get(`${baseUrl}/users/me`).then((response) => {
    // publish user to subscribers and start timer to refresh token
    userSubject.next(response.data.user);
    hospitalSubject.next(response.data.hospital);
    localStorage.setItem('user', JSON.stringify(response.data.user));
    localStorage.setItem(
      'hospital',
      JSON.stringify(response.data.hospital || {}),
    );
    // startRefreshTokenTimer();
    return response;
  });
}

function getRoomsData() {
  return fetchWrapper.get(`${baseUrl}/rooms/count`).then((response) => {
    // publish user to subscribers and start timer to refresh token
    roomsSubject.next(response.data);
    // startRefreshTokenTimer();
    return response.data;
  });
}

function logout() {
  // revoke token, stop refresh timer, publish null to user subscribers and redirect to login page
  fetchWrapper.post(`${baseUrl}/users/sign-out`, {});
  localStorage.removeItem('user');
  localStorage.removeItem('hospital');
  //   stopRefreshTokenTimer();
  userSubject.next(null);
  hospitalSubject.next(null);
  history.push('/account/login');
}

function refreshToken() {
  return fetchWrapper.post(`${baseUrl}/refresh-token`, {}).then((user) => {
    // publish user to subscribers and start timer to refresh token
    userSubject.next(user);
    // parse json object from base64 encoded jwt token
    const jwtToken = JSON.parse(
      atob(userSubject!.value!.jwtToken!.split('.')[1]),
    );

    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - 60 * 1000;
    setTimeout(refreshToken, timeout);
    return user;
  });
}

// eslint-disable-next-line camelcase
function acceptInvite(invitation_token: string | string[] | null) {
  return fetchWrapper
    .post(`${baseUrl}/invites/accept-invite`, { invitation_token })
    .then((response) => {
      // publish user to subscribers and start timer to refresh token
      emailSubject.next(response.data.user.email);
    });
}

function verifyEpicToken(code: string | string[] | null) {
  return fetchWrapper.post(`${baseUrl}/epic`, { code }).then((response) => {
    // publish user to subscribers and start timer to refresh token
    hospitalSubject.next(response.data.hospital);
    localStorage.setItem(
      'hospital',
      JSON.stringify(response.data.hospital || {}),
    );
  });
}

// function create(params) {
//     return fetchWrapper.post(baseUrl, params);
// }

// function update(id, params) {
//     return fetchWrapper.put(`${baseUrl}/${id}`, params)
//         .then(user => {
//             // update stored user if the logged in user updated their own record
//             if (user.id === userSubject.value.id) {
//                 // publish updated user to subscribers
//                 user = { ...userSubject.value, ...user };
//                 userSubject.next(user);
//             }
//             return user;
//         });
// }

// // prefixed with underscore because 'delete' is a reserved word in javascript
// function _delete(id) {
//     return fetchWrapper.delete(`${baseUrl}/${id}`)
//         .then(x => {
//             // auto logout if the logged in user deleted their own record
//             if (id === userSubject.value.id) {
//                 logout();
//             }
//             return x;
//         });
// }

// helper functions

// let refreshTokenTimeout: NodeJS.Timeout;

// function startRefreshTokenTimer() {
//   // parse json object from base64 encoded jwt token
//   const jwtToken = JSON.parse(
//     atob(userSubject!.value!.jwtToken!.split('.')[1]),
//   );

//   // set a timeout to refresh the token a minute before it expires
//   const expires = new Date(jwtToken.exp * 1000);
//   const timeout = expires.getTime() - Date.now() - 60 * 1000;
//   refreshTokenTimeout = setTimeout(refreshToken, timeout);
// }

// function stopRefreshTokenTimer() {
//   clearTimeout(refreshTokenTimeout);
// }

export const accountService = {
  register,
  verifyEmail,
  signup,
  login,
  verifyOTP,
  resendOTP,
  acceptInvite,
  verifyEpicToken,
  forgotPassword,
  validateResetToken,
  resetPassword,
  logout,
  refreshToken,
  getAll,
  getById,
  getUserData,
  getRoomsData,

  // create,
  // update,
  // delete: _delete,
  user: userSubject.asObservable(),
  get userValue() {
    return userSubject.value;
  },
  rooms: roomsSubject.asObservable(),
  get roomsValue() {
    return roomsSubject.value;
  },
  hospital: hospitalSubject.asObservable(),
  get hospitalValue() {
    return hospitalSubject.value;
  },
  email: emailSubject.asObservable(),
  get emailValue() {
    return emailSubject.value;
  },
};
