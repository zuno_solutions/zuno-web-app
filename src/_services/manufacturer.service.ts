/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable camelcase */
import { fetchWrapper } from '../_helpers/fetch-wrapper';
import { accountService } from './account.service';

const baseUrl = `${process.env.REACT_APP_ZUNO_API_URL}`;

const { hospitalValue } = accountService;

export const suggestionsLenght = 7;
function getSuggestedManufacturers({
  valueSearched,
  offset = 1,
  limit = suggestionsLenght,
}: { offset?: number; limit?: number; valueSearched?: string } = {}) {
  return fetchWrapper
    .get(
      `${baseUrl}/manufacturers/offset/${offset}/limit/${limit}?manufacturer=${valueSearched}`,
    )
    .then((response) => response.data);
}

function getSuggestedSurgeons({
  valueSearched,
  offset = 1,
  limit = suggestionsLenght,
}: { offset?: number; limit?: number; valueSearched?: string } = {}) {
  return fetchWrapper
    .get(
      `${baseUrl}/personnel/role/6/offset/${offset}/limit/${limit}?search=${valueSearched}`,
    )
    .then((response) => response.data);
}

export const manufacturerService = {
  getSuggestedManufacturers,
  getSuggestedSurgeons,
};
